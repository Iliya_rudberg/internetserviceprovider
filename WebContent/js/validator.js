﻿
$(document).ready(function () {
    $('#multiform').bootstrapValidator({
        feedbackIcons: {
            required: 'fa fa-asterisk',
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          tariffTraffic: {
                validators: {
                    regexp: {
                        regexp: /^[0-9\.]+$/,
                        message: 'The price can only consist of double value'
                    }
                }
            },
            tariffCost: {
                validators: {
                	 notEmpty: {
                         message: 'The cost is required and cannot be empty'
                     },
                    regexp: {
                        regexp: /^[0-9\.]+$/,
                        message: 'The cost can only consist of double value'
                    }
                }
            },
        downloadSpeed: {
                validators: {
                    notEmpty: {
                        message: 'The download speed is required and cannot be empty'
                    }
                }
            },
            uploadSpeed: {
                validators: {
                    notEmpty: {
                        message: 'The upload speed is required and cannot be empty'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'The upload speed can contains only by digits'
                    }
                }
            },
        tariffType: {
            validators: {
                notEmpty: {
                    message: 'The tariff type is required and cannot be empty'
                }
			}
        },
        tariffName: {
            validators: {
                notEmpty: {
                    message: 'The tariff name is required and cannot be empty'
                },
                  regexp: {
                        regexp: /^[A-Za-zА-Яа-я]+$/,
                        message: 'The tariff name can contain only by letters'
                    }
            }
        },
        contractClientID: {
            validators: {
                notEmpty: {
                    message: 'The client id is required and cannot be empty'
                },
                  regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'The client id is can contain only by digits'
                    }
            }
        },
        contractTariffId: {
            validators: {
                notEmpty: {
                    message: 'The tariff id is required and cannot be empty'
                },
                  regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'The client id is can contain only by digits'
                    }
            }
        },
        contractStatus: {
            validators: {
                notEmpty: {
                    message: 'The status is required and cannot be empty'
                }
            }
        },
        contractBalance: {
            validators: {
                notEmpty: {
                    message: 'The balance is required and cannot be empty'
                },
                regexp: {
                    regexp: /^[0-9\.]+$/,
                    message: 'The balance can only consist of double value'
                }
            }
        },
        date: {
            validators: {
           	 notEmpty: {
                 message: 'The date is required and cannot be empty'
             },
                date: {
                    format: 'YYYY-MM-DD',
                    message: 'The value is not a valid date'
                }
            }
        },
        contractID: {
            validators: {
                notEmpty: {
                    message: 'The contract id is required and cannot be empty'
                },
                  regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'The contract id is can contain only by digits'
                    }
            }
        },
        transactionSum: {
            validators: {
                notEmpty: {
                    message: 'The sum is required and cannot be empty'
                },
                regexp: {
                    regexp: /^[0-9\.]+$/,
                    message: 'The sum can only consist of double value'
                }
            }
        },
        transactionType: {
            validators: {
                notEmpty: {
                    message: 'The type is required and cannot be empty'
                },
            }
        },
        phone: {
            validators: {
                notEmpty: {
                    message: 'The price is required and cannot be empty'
                },
                integer: {
                    message: 'You can only enter an integer'
                },
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'The price can only consist of numbers'
                }
            }
        },
         email: {
        validators: {
            notEmpty: {
                message: 'The email is required and cannot be empty'
            },
             regexp: {
                    regexp: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    message: 'This is no valid email format'
                }
        }
    },
     password: {
            validators: {
                notEmpty: {
                    message: 'The password is required and cannot be empty'
                },
                regexp: {
                    regexp: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{5,20}$/,
                    message: 'At least 6 characters, at least one letter in each register 													and at least one digit'
                }
            }
        },
           confirmPassword: {
        validators: {
            identical: {
                field: 'password',
                message: 'The password and its confirm are not the same'
            }
        }
    },
     address: {
        validators: {
            notEmpty: {
                message: 'The address is required and cannot be empty'
            }
        }
    },
         surname: {
        validators: {
            notEmpty: {
                message: 'The surname is required and cannot be empty'
            },
              regexp: {
                    regexp: /^[A-Za-zА-Яа-я]+$/,
                    message: 'The surname can contain only by letters'
                }
        }
    },
       name: {
        validators: {
            notEmpty: {
                message: 'The name is required and cannot be empty'
            },
              regexp: {
                    regexp: /^[A-Za-zА-Яа-я]+$/,
                    message: 'The name can contain only by letters'
                }
        }
    },
	username: {
            validators: {
                notEmpty: {
                    message: 'The username is required and cannot be empty'
                },
                regexp: {
                    regexp: /^[a-zA-Z][a-zA-Z0-9_]{4,10}$/,
                    message: 'Latin letters, numbers,underscores, the first character - a Latin letter, the number of characters at least 5'
                }
            }
        },		
        passport: {
            validators: {
                notEmpty: {
                    message: 'The passport is required and cannot be empty'
                }
            }
        },	
        position: {
            validators: {
                notEmpty: {
                    message: 'The position is required and cannot be empty'
                },
                  regexp: {
                        regexp: /^[A-Za-zА-Яа-я]+$/,
                        message: 'The position can contain only by letters'
                    }
            }
        },
        }
    });

    $(document).on("click", ".btn-danger", function () {
         var bootstrapValidator = $('#multiform').data('bootstrapValidator');
         bootstrapValidator.enableFieldValidators('name', false);
    });

});