<%@ include file="/WEB-INF/jspf/directive/page.jspf"%> 
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%> 
<!doctype html> 
<html > 
<c:set var="title" scope="request" value="InterSpeed"/> 
<%@ include file="/WEB-INF/jspf/head.jspf"%> 
<body> 
 
<!--==============================Header=============================--> 
<%@ include file="/WEB-INF/jspf/header.jspf"%> 
  <!--==============================Navarea=============================--> 
  <div class="nav-area"> 
    <nav id="main-nav" class="navbar navbar-default"> 
      <div class="container"> 
        <div class="navbar-header"> 
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button> 
          <a class="navbar-brand" href="#"><img src="images/logo.png" alt=""></a> </div> 
        <div class="navbar-collapse collapse"> 
          <ul class="nav navbar-nav"> 
           <li class="dropdown active"><a href="#choose"><fmt:message key="main.menu.choose"/></a></li> 
           <li><a href="#features"><fmt:message key="main.menu.features"/></a></li> 
           <li><a href="#services"><fmt:message key="main.menu.services"/></a></li> 
   <li><a href="#offer"><fmt:message key="main.menu.offers"/></a></li> 
   <li><a href="#web-hosting"><fmt:message key="main.menu.pricing"/></a></li> 
           <li><a href="#clients"><fmt:message key="main.menu.clients"/></a></li> 
           <li><a href="#contact"><fmt:message key="main.menu.connect"/></a></li> 
          </ul> 
        </div> 
        <!--/.nav-collapse -->  
      </div> 
    </nav> 
  </div> 
 
<!--=============================Banner=============================--> 
 
<div id="myCarousel" class="carousel slide" data-ride="carousel"> 
  <div id="lp-pom-block-10"> 
    <div class="banner-dot"> 
      <ol class="carousel-indicators"> 
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li> 
        <li data-target="#myCarousel" data-slide-to="1"></li> 
        <li data-target="#myCarousel" data-slide-to="2"></li> 
      </ol> 
      <div class="baaner-container"> 
        <div class="carousel-inner"> 
          <div class="item active"> 
            <div class="container"> 
              <div class="carousel-caption"> 
                <p class="main-image"><img src="images/icon1.png" alt="" /></p> 
                <div class="letter-container"> 
                  <h1><a href="#"><fmt:message key="main.banner.h1"/></a></h1> 
                </div> 
                <h2><fmt:message key="main.banner.h2"/></h2> 
                <p class="banner-text"> 
              <fmt:message key="main.banner.text"/> 
</p> 
<a class="blue-button wobble-horizontal" href="#contact" role="button"><fmt:message key="main.menu.connect"/></a> 
              </div> 
            </div> 
          </div> 
          <div class="item"> 
            <div class="container"> 
              <div class="carousel-caption"> 
                <div class="banner-inner-container"> 
                  <div class="letter-container head-space"> 
                    <h1><a href="#"><fmt:message key="main.banner2.h1"/></a></h1> 
                  </div> 
                  <h2><fmt:message key="main.banner2.h2"/></h2> 
                  <p class="banner-text1"> 
 <fmt:message key="main.banner2.text"/> 
  </p> 
 
                  <div class="row featurette"> 
                    <div class="col-md-4 banner-icon hidden-xs hidden-sm"> <img src="images/icon2.png" alt="" /> </div> 
                    <div class="col-md-8"> 
                      <div class="banner-nav"> 
                        <ul> 
                          <li><fmt:message key="main.banner2.li.1"/></li> 
                          <li><fmt:message key="main.banner2.li.2"/></li> 
                          <li><fmt:message key="main.banner2.li.3"/></li> 
                          <li><fmt:message key="main.banner2.li.4"/></li> 
                        </ul> 
                      </div> 
                    </div> 
                  </div> 
<a class="blue-button wobble-horizontal" href="#contact" role="button"><fmt:message key="main.menu.connect"/></a> 
                </div> 
              </div> 
            </div> 
          </div> 
          <div class="item"> 
            <div class="container"> 
              <div class="carousel-caption"> 
                <div class="banner-inner-container"> 
                  <div class="letter-container head-space"> 
                    <h1><a href="#"><fmt:message key="main.banner3.h1"/></a></h1> 
                  </div> 
                  <h2><fmt:message key="main.banner3.h2"/></h2> 
                  <p class="banner-text1"> 
 <fmt:message key="main.banner3.text"/> 
  </p> 
   
                  <div class="row featurette"> 
                    <div class="col-md-4 banner-icon hidden-xs hidden-sm"> <img src="images/icon3.png" alt="" /> </div> 
                    <div class="col-md-8"> 
                      <div class="banner-nav"> 
                        <ul> 
                          <li><fmt:message key="main.banner3.li.1"/></li> 
                          <li><fmt:message key="main.banner3.li.2"/></li> 
                          <li><fmt:message key="main.banner3.li.3"/></li> 
                          <li><fmt:message key="main.banner3.li.4"/></li> 
                        </ul> 
                      </div> 
                    </div> 
                  </div> 
<a class="blue-button wobble-horizontal" href="#contact" role="button"><fmt:message key="main.menu.connect"/></a> 
                </div> 
              </div> 
            </div> 
          </div> 
        </div> 
      </div> 
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a> 
      <div class="banner-arrow"> 
        <p class="pulse"><a href="#choose"><img src="images/arrow-bottom.png" alt=""/></a></p> 
      </div> 
    </div> 
  </div> 
</div> 
 
 
<!--============================Home=============================--> 
<div id="choose" class="choose-container" > 
  <div class="container"> 
    <div class="row main-head"> 
      <div class="col-md-6 col-md-offset-3" > 
        <h1><fmt:message key="main.choose.h1"/></h1> 
        <h2><fmt:message key="main.choose.h2"/></h2> 
      </div> 
    </div> 
    <div class="row choose"> 
      <div class="col-sm-6 col-md-3" data-anijs="if: scroll, on: window, do: bounceInLeft animated, before: scrollReveal"> 
        <div class="choose-item choose-img-1"> 
          <div class="choose-info-wrap"> 
            <div class="choose-info"> 
              <div class="choose-info-front choose-img-1"></div> 
              <div class="choose-info-back"> 
                <h3><fmt:message key="main.choose.support.super"/></h3> 
                <p><a href="#"><fmt:message key="main.choose.support.support"/></a></p> 
              </div> 
            </div> 
          </div> 
        </div> 
        <h2><fmt:message key="main.choose.support"/></h2> 
        <p class="space-text"> 
<fmt:message key="main.choose.support.text"/> 
</p> 
      </div> 
      <div class="col-sm-6 col-md-3" data-anijs="if: scroll, on: window, do: bounceInUp animated, before: scrollReveal"> 
        <div class="choose-item choose-img-2"> 
          <div class="choose-info-wrap"> 
            <div class="choose-info"> 
              <div class="choose-info-front choose-img-2"></div> 
              <div class="choose-info-back"> 
                <h3><fmt:message key="main.choose.secure"/></h3> 
                <p><a href="#"><fmt:message key="main.choose.reliable"/></a></p> 
              </div> 
            </div> 
          </div> 
        </div> 
        <h2><fmt:message key="main.choose.security"/></h2> 
        <p class="space-text"> 
<fmt:message key="main.choose.security.text"/> 
</p> 
      </div> 
      <div class="col-sm-6 col-md-3" data-anijs="if: scroll, on: window, do: bounceInUp animated, before: scrollReveal"> 
        <div class="choose-item choose-img-3"> 
          <div class="choose-info-wrap"> 
            <div class="choose-info"> 
              <div class="choose-info-front choose-img-3"></div> 
              <div class="choose-info-back"> 
                <h3><fmt:message key="main.choose.solution"/></h3> 
                <p><a href="#"><fmt:message key="main.choose.solution.great"/></a></p> 
              </div> 
            </div> 
          </div> 
        </div> 
        <h2><fmt:message key="main.choose.solution"/></h2> 
        <p> 
<fmt:message key="main.choose.solution.text"/> 
</p> 
      </div> 
      <div class="col-sm-6 col-md-3" data-anijs="if: scroll, on: window, do: fadeInRightBig animated, before: scrollReveal"> 
        <div class="choose-item choose-img-4"> 
          <div class="choose-info-wrap"> 
            <div class="choose-info"> 
              <div class="choose-info-front choose-img-4"></div> 
              <div class="choose-info-back"> 
                <h3><fmt:message key="main.choose.install"/></h3> 
                <p><a href="#"><fmt:message key="main.choose.install.quick"/></a></p> 
              </div> 
            </div> 
          </div> 
        </div> 
        <h2><fmt:message key="main.choose.install"/> <fmt:message key="main.choose.install.quick"/></h2> 
        <p class="choose1">  
<fmt:message key="main.choose.install.text"/> 
</p> 
      </div> 
    </div> 
  </div> 
</div> 
 
 
 
<!--=============================Features=============================--> 
<div id="features" class="features-container"> 
  <div class="container"> 
    <div class="row main-head"> 
      <div class="col-md-12"> 
        <h1><fmt:message key="main.features.h1"/></h1> 
        <h2><fmt:message key="main.features.h2"/></h2> 
        <p><fmt:message key="main.features.text"/> </p> 
      </div> 
    </div> 
    <div class="row"> 
      <div class="col-sm-6 col-md-6" data-anijs="if: scroll, on: window, do: bounceInLeft animated, before: scrollReveal"> 
        <div class="features"> <span class="featuresIcon1"></span> 
          <h2><fmt:message key="main.features.tariff"/></h2> 
          <p> 
 <fmt:message key="main.features.tariff.text"/> 
  </p> 
        </div> 
      </div> 
      <div class="col-sm-6 col-md-6" data-anijs="if: scroll, on: window, do: fadeInRightBig animated, before: scrollReveal"> 
        <div class="features"> <span class="featuresIcon2"></span> 
          <h2><fmt:message key="main.features.tv"/></h2> 
          <p> 
<fmt:message key="main.features.tv.text"/> 
  </p> 
        </div> 
      </div> 
      <div class="col-sm-6 col-md-6" data-anijs="if: scroll, on: window, do: bounceInLeft animated, before: scrollReveal" > 
        <div class="features"> <span class="featuresIcon3"></span> 
          <h2><fmt:message key="main.features.network"/></h2> 
          <p> 
<fmt:message key="main.features.network.text"/> 
  </p> 
        </div> 
      </div> 
      <div class="col-sm-6 col-md-6" data-anijs="if: scroll, on: window, do: fadeInRightBig animated, before: scrollReveal"> 
        <div class="features"> <span class="featuresIcon4"></span> 
          <h2><fmt:message key="main.features.guarantee"/> </h2> 
          <p> 
<fmt:message key="main.features.quarantee.text"/> 
  </p> 
        </div> 
      </div> 
      <div class="col-sm-6 col-md-6" data-anijs="if: scroll, on: window, do: bounceInLeft animated, before: scrollReveal"> 
        <div class="features"> <span class="featuresIcon5"></span> 
          <h2><fmt:message key="main.features.connect"/></h2> 
          <p> 
<fmt:message key="main.features.connect.text"/> 
  </p> 
        </div> 
      </div> 
      <div class="col-sm-6 col-md-6" data-anijs="if: scroll, on: window, do: fadeInRightBig animated, before: scrollReveal"> 
        <div class="features"> <span class="featuresIcon6"></span> 
          <h2><fmt:message key="main.features.solutions"/></h2> 
          <p> 
<fmt:message key="main.features.soultions.text"/> 
  </p> 
        </div> 
      </div> 
    </div> 
  </div> 
</div> 
 
<!--==============================Services=============================--> 
 
<div id="services" class="service-container"> 
  <div class="container"> 
    <div class="row main-head"> 
      <div class="col-md-12"> 
        <h1><fmt:message key="main.service.h1"/></h1> 
        <h2><fmt:message key="main.service.h2"/></h2> 
        <p><fmt:message  key="main.service.text"/></p> 
      </div> 
    </div> 
    <div class="row" data-anijs="if: scroll, on: window, do: flipInY animated, before: scrollReveal"> 
      <div class="col-sm-12 col-md-6 " > 
        <div class="service1"> <img src="images/service-image.png" alt=""> </div> 
      </div> 
      <div class="col-sm-12 col-md-6 service-text-area" > 
        <div class="service"> <span class="serviceIcon"></span> 
          <h2><fmt:message key="main.service.resources"/></h2> 
          <p> 
<fmt:message key="main.service.resources.text"/> 
  </p> 
        </div> 
        <div class="service"> <span class="serviceIcon"></span> 
          <h2><fmt:message key="main.service.tv"/></h2> 
          <p> 
<fmt:message key="main.service.tv.text"/> 
  </p> 
        </div> 
        <div class="service"> <span class="serviceIcon"></span> 
          <h2><fmt:message key="main.service.network"/></h2> 
          <p> 
           <fmt:message key="main.service.network.text"/> </p> 
        </div> 
      </div> 
    </div> 
  </div> 
</div> 
 
<!--==============================Offer==============================--> 
 
<div id="offer" > 
  <div id="lp-pom-block-11"> 
    <div class="container"> 
      <div class="row offer-head"> 
        <div class="col-md-12"> 
          <h1><fmt:message key="main.offer.h1"/></h1> 
          <h2><fmt:message key="main.offer.h2"/></h2> 
        </div> 
      </div> 
      <div class="row"> 
        <div class="col-sm-6 col-md-6" data-anijs="if: scroll, on: window, do: rotateIn animated, before: scrollReveal"> 
          <div class="offer-box1"> 
            <div class="offer-text"> 
              <ul> 
                <li><fmt:message key="main.offer.buisness.li.1"/></li> 
                <li><fmt:message key="main.offer.buisness.li.2"/></li> 
                <li><fmt:message key="main.offer.buisness.li.3"/></li> 
                <li><fmt:message key="main.offer.buisness.li.4"/></li> 
                <li><fmt:message key="main.offer.buisness.li.5"/></li> 
              </ul> 
            </div> 
            <div class="offer-image wobble-horizontal"> 
              <div class="offer-image-box "><span>$33</span>.50 
                <p><fmt:message key="main.offer.month"/></p> 
              </div> 
            </div> 
          </div> 
          <div class="offer-boxhead"><fmt:message key="main.offer.button.buisness"/><a href="#contact"><img src="images/more-arrow.png" alt=""/> </a></div> 
        </div> 
        <div class="col-sm-6 col-md-6" data-anijs="if: scroll, on: window, do: rotateIn animated, before: scrollReveal"> 
          <div class="offer-box1"> 
            <div class="offer-text"> 
              <ul> 
                <li><fmt:message key="main.offer.home.li.1"/></li> 
                <li><fmt:message key="main.offer.home.li.2"/></li> 
                <li><fmt:message key="main.offer.home.li.3"/></li> 
                <li><fmt:message key="main.offer.home.li.4"/></li> 
                <li><fmt:message key="main.offer.home.li.5"/></li> 
              </ul> 
            </div> 
            <div class="offer-image1 wobble-horizontal"> 
              <div class="offer-image-box"><span>$23</span>.50 
                <p><fmt:message key="main.offer.month"/></p> 
              </div> 
            </div> 
          </div> 
          <div class="offer-boxhead"><fmt:message key="main.offer.button.home"/><a href="#contact"><img src="images/more-arrow.png" alt=""/> </a> </div> 
        </div> 
      </div> 
    </div> 
  </div> 
</div> 
 
<!--==============================Pricing tables=============================--> 
 
<div id="web-hosting" class="price-container"> 
  <div class="container"> 
    <div class="row main-head"> 
      <div class="col-md-12"> 
        <h1><fmt:message key="main.pricing.h1"/></h1> 
        <h2><fmt:message key="main.pricing.h2"/></h2> 
      </div> 
    </div> 
    <div class="row" id="columns"> 
      <div class="col-sm-6 col-md-3 fade"  data-anijs="if: scroll, on: window, do: bounceInLeft animated, before: scrollReveal"> 
        <div class="main-table"> 
          <div class="price-table-top"> <a class="price-table-topbg "> 
            <h3><fmt:message key="main.pricing.table1.h1"/></h3> 
            </a> <a class="price-circle"> 
            <p class="price">$15</p> 
            <p class="month"><fmt:message key="main.pricing.period"/></p> 
            </a> </div> 
          <ul class="price-plans"> 
<li><fmt:message key="main.pricing.table1.li.1"/></li> 
<li><fmt:message key="main.pricing.table1.li.2"/></li> 
                <li><fmt:message key="main.pricing.table1.li.3"/></li> 
                <li><fmt:message key="main.pricing.table1.li.4"/></li> 
                <li><fmt:message key="main.pricing.table1.li.5"/></li> 
                <li><fmt:message key="main.pricing.table1.li.6"/></li> 
          </ul> 
          <a href="#contact" class="price-buy-now wobble-horizontal"><fmt:message key="main.pricing.button"/></a> </div> 
      </div> 
      <div class="col-sm-6 col-md-3 fade" data-anijs="if: scroll, on: window, do: bounceInLeft animated, before: scrollReveal"> 
        <div class="main-table"> 
          <div class="price-table-top"> <a class="price-table-topbg "> 
            <h3><fmt:message key="main.pricing.table2.h1"/></h3> 
            </a> <a class="price-circle"> 
            <p class="price">$30</p> 
             <p class="month"><fmt:message key="main.pricing.period"/></p> 
            </a> </div> 
        <ul class="price-plans"> 
<li><fmt:message key="main.pricing.table2.li.1"/></li> 
<li><fmt:message key="main.pricing.table2.li.2"/></li> 
                <li><fmt:message key="main.pricing.table2.li.3"/></li> 
                <li><fmt:message key="main.pricing.table2.li.4"/></li> 
                <li><fmt:message key="main.pricing.table2.li.5"/></li> 
                <li><fmt:message key="main.pricing.table2.li.6"/></li> 
          </ul> 
          <a href="#contact" class="price-buy-now wobble-horizontal"><fmt:message key="main.pricing.button"/></a> </div> 
      </div> 
      <div class="col-sm-6 col-md-3 fade" data-anijs="if: scroll, on: window, do: fadeInRightBig animated, before: scrollReveal"> 
        <div class="main-table"> 
          <div class="price-table-top"> <a class="price-table-topbg "> 
            <h3><fmt:message key="main.pricing.table3.h1"/></h3> 
            </a> <a class="price-circle"> 
            <p class="price">$40</p> 
            <p class="month"><fmt:message key="main.pricing.period"/></p> 
            </a> </div> 
            <ul class="price-plans"> 
<li><fmt:message key="main.pricing.table3.li.1"/></li> 
<li><fmt:message key="main.pricing.table3.li.2"/></li> 
                <li><fmt:message key="main.pricing.table3.li.3"/></li> 
                <li><fmt:message key="main.pricing.table3.li.4"/></li> 
                <li><fmt:message key="main.pricing.table3.li.5"/></li> 
                <li><fmt:message key="main.pricing.table3.li.6"/></li> 
          </ul> 
          <a href="#contact" class="price-buy-now wobble-horizontal"><fmt:message key="main.pricing.button"/></a> </div> 
      </div> 
      <div class="col-sm-6 col-md-3 fade" data-anijs="if: scroll, on: window, do: fadeInRightBig animated, before: scrollReveal"> 
        <div class="main-table"> 
          <div class="price-table-top"> <a class="price-table-topbg "> 
            <h3 class=""><fmt:message key="main.pricing.table4.h1"/></h3> 
            </a> <a class="price-circle "> 
            <p class="price">$75</p> 
             <p class="month"><fmt:message key="main.pricing.period"/></p> 
            </a> </div> 
           <ul class="price-plans"> 
<li><fmt:message key="main.pricing.table4.li.1"/></li> 
<li><fmt:message key="main.pricing.table4.li.2"/></li> 
                <li><fmt:message key="main.pricing.table4.li.3"/></li> 
                <li><fmt:message key="main.pricing.table4.li.4"/></li> 
                <li><fmt:message key="main.pricing.table4.li.5"/></li> 
                <li><fmt:message key="main.pricing.table4.li.6"/></li> 
          </ul> 
          <a href="#contact" class="price-buy-now wobble-horizontal"><fmt:message key="main.pricing.button"/></a> </div> 
      </div> 
    </div> 
  </div> 
</div> 
<!--==============================Our clients============================--> 
 
<div id="clients" class="client-container"> 
  <div class="container"> 
    <div class="row main-head"> 
      <div class="col-md-12"> 
        <h1><fmt:message key="main.clients.h1"/></h1> 
        <h2><fmt:message key="main.clients.h2"/></h2> 
      </div> 
    </div> 
    <div class="row"> 
      <div class="col-md-8 col-md-offset-2" data-anijs="if: scroll, on: window, do: rubberBand animated, before: scrollReveal"> 
        <div class="client"> 
          <div id="owl-sponsors" class="owl-carousel"> 
            <div class="item"> <a target="_blank" href="#"><img src="images/client-logo1.png" alt="Themeforest" width="260" height="80"></a> </div> 
            <div class="item"> <a target="_blank" href="#"><img src="images/client-logo2.png" alt="Photodune" width="260" height="80"></a> </div> 
            <div class="item"> <a target="_blank" href="#"><img src="images/client-logo3.png" alt="Themeforest" width="260" height="80"></a> </div> 
            <div class="item"> <a target="_blank" href="#"><img src="images/client-logo4.png" alt="AudioJungle" width="260" height="80"></a> </div> 
            <div class="item"> <a target="_blank" href="#"><img src="images/client-logo5.png" alt="Graphicriver" width="260" height="80"></a> </div> 
            <div class="item"> <a target="_blank" href="#"><img src="images/client-logo2.png" alt="Photodune" width="260" height="80"></a> </div> 
            <div class="item"> <a target="_blank" href="#"><img src="images/client-logo3.png" alt="Themeforest" width="260" height="80"></a> </div> 
            <div class="item"> <a target="_blank" href="#"><img src="images/client-logo4.png" alt="AudioJungle" width="260" height="80"></a> </div> 
          </div> 
          <div class="sponsor-title-nav"> 
            <div class="owl-Navigation"> <a class="btn prev"><img src="images/prev-arrow.png" alt=""/></a> <a class="btn next"><img src="images/next-arrow.png" alt=""/></a> </div> 
          </div> 
        </div> 
      </div> 
    </div> 
  </div> 
</div> 
 
 
<!--==============================Connect me=============================--> 
 
<div id="contact" class="service-container"> 
<fmt:message key="main.connect.fullName" var="name"/> 
 
  <div class="container"> 
    <div class="row main-head"> 
      <div class="col-md-12"> 
        <h1><fmt:message key="main.connect.h1"/></h1> 
        <h2><fmt:message key="main.connect.h2"/></h2> 
      </div> 
    </div> 
    <div class="row"> 
      <div class="col-md-10 col-md-offset-1" data-anijs="if: scroll, on:window, do: rollIn animated, before: scrollReveal, after: removeAnim"> 
        <div class="contact-form" id="contact-us"> 
          <form id="multiform" action="controller" method="POST" > 
          <input type="hidden" name=url value="${requestScope['javax.servlet.forward.query_string']}">
          <input type="hidden" name ="command" value="contactMe"> 
            <fieldset> 
              <div class="col-xs-12 col-sm-6"  > 
                <div class="form-group"> 
                  <input type="text" class="form-control" name="name"  id="name"  placeholder=<fmt:message key="main.connect.fullName"/>> 
                </div> 
                <div class="form-group"> 
                  <input type="email" class="form-control" name="email" id="email" placeholder=<fmt:message key="main.connect.email"/>> 
                </div> 
                <div class="form-group"> 
                  <input type="text" class="form-control" name="mobile" id="mobile" placeholder=<fmt:message key="main.connect.phone"/>> 
                </div> 
              </div> 
              <div class="col-xs-12 col-sm-6" > 
                <div class="form-group"> 
                  <textarea class="form-control textarea" rows="5" name="message" id="msg" placeholder=<fmt:message key="main.connect.message"/>></textarea> 
                </div> 
              </div> 
              <div class="relative fullwidth col-xs-12"> 
                <button type="submit" class="form-btn wobble-horizontal"><fmt:message key="main.connect.button"/></button> 
                <span class="loading"></span> </div> 
              <div class="clear"></div> 
            </fieldset> 
          </form> 
        </div> 
      </div> 
    </div> 
  </div> 
</div> 
 <div id="modalMessage" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/errorMessage.jspf"%>
		</div>
<!--===========================Goto top============================-->  
<a href="#" id="go-top"></a>  
 
<!--==============================Footer============================--> 
<%@ include file="/WEB-INF/jspf/footer.jspf"%> 
 
 
<!--===============================Footer-bottom============================--> 
 
<script type="text/javascript"> 
/*=================== 
Send message allert 
 ===================*/ 
 foo(); 
 function foo() { 
     var value = "${message}"; 
     if(value != ""){ 
     alert(value); 
     } 
 } 
  /*=================== 
   OWL Carousel 
    ===================*/ 
 
jQuery(document).ready(function() { 
var owl = jQuery("#owl-sponsors"); 
 
owl.owlCarousel({ 
stopOnHover : true, 
items : 5, 
itemsCustom : false, 
itemsDesktop : [1200,5], 
itemsDesktopSmall : [1240,5], 
itemsTablet: [767,2], 
itemsTabletSmall: false, 
itemsMobile : [480,1], 
singleItem : false, 
pagination: false, 
itemsScaleUp : false, 
}); 
 
jQuery(".next").click(function(){ 
owl.trigger('owl.next'); 
}) 
 
jQuery(".prev").click(function(){ 
owl.trigger('owl.prev'); 
}) 
}); 
 
 
</script>  
<%@ include file="/WEB-INF/jspf/scripts.jspf"%> 
<!--============================Main.js===========================-->  
 
<script src="js/main.js" type="text/javascript"></script>  
</body> 
</html> 