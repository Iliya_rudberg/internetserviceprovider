<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<!-- Head -->
<fmt:message key="title.error" var="title"/>
<c:set var="title" scope="request" value="${title}"/>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
<body class="security-app">
<!--==============================Header=============================-->
<%@ include file="/WEB-INF/jspf/header.jspf"%>
<div class="lc-block">
 <!--==============================Navarea=============================-->
 <c:if test="${userRole.name == 'admin'}">
 <div class="nav-area">
   <%@ include file="/WEB-INF/jspf/adminMenu.jspf"%>
  </div>
 </c:if>
 <c:if test="${userRole.name == 'user'}">
 <div class="nav-area">
   <%@ include file="/WEB-INF/jspf/userMenu.jspf"%>
  </div>
  </c:if>
  <div class="table-wrapper">
	<div class="lc-block">
		<h3 style="text-align:center"><fmt:message key="error_page.title"/></h3>
		
		<c:set var="code"
				value="${requestScope['javax.servlet.error.status_code']}" />
			<c:set var="message"
				value="${requestScope['javax.servlet.error.message']}" />

			<c:if test="${not empty code}">
				<h3>
					Error code:
					<c:out value="${code}" />
				</h3>
			</c:if>

			<c:if test="${not empty message}">
				<h3>
					<c:out value="${message}" />
				</h3>
			</c:if>

			<%-- if get this page using forward --%>
			<c:if test="${not empty errorMessage}">
				<p>
					<c:out value="${errorMessage}" />
				</p>
			</c:if>
			<img style="float:center"src="images/homer.gif" alt="">
	</div>
	</div>
	<div id="modalAdd" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/change.jspf"%>
		</div>
		 <div id="modalEdit" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/editClient.jspf"%>
		</div>
		<div id="modalChange" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/change.jspf"%>
		</div>
		</div>
	<!--==============================Footer============================-->
<%@ include file="/WEB-INF/jspf/footer.jspf"%>
<%@ include file="/WEB-INF/jspf/scripts.jspf"%>

</body>
</html>