<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ taglib uri="/WEB-INF/clientCustomTag.tld" prefix="mytag"%> 

<html>
<fmt:message key="title.accountState" var="title"/>
<c:set var="title" scope="request" value="${title }"/>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
<body class="security-app">


	<!-- Header  -->
	<%@ include file="/WEB-INF/jspf/header.jspf"%>
	<div class="lc-block">

		 <!--==============================Navarea=============================-->
 <div class="nav-area">
   <%@ include file="/WEB-INF/jspf/userMenu.jspf"%>
  </div>
  <div class="table-wrapper">
  
		<!-- Client table -->
	   <h2 style="text-align:center;"><fmt:message key="header.account"/></h2> 
	   <p style="text-align:center;"><fmt:message key="account.description"/></p>
	   <div class="table-responsive"> 
		<table id="table-info" class="table table-hover " >
		<tbody>
			 <tr>
			 	 <tr>
			    <th  class="table-content account-content"><fmt:message key="account.id"/></th>
			    <td><c:out value="${account.id}" /></td>
			    </tr>
			     <tr>
 				<th  class="table-content"><fmt:message key="account.tariffName"/></th>
 				 <td><c:out value="${account.tariff.name}" /></td></tr>
 				 <tr>
  				<th class="table-content"><fmt:message key="account.status"/></th>
  				 <td><c:out value="${account.status}" /></td></tr>
  				 <tr>
  				<th class="table-content"><fmt:message key="account.date"/></th>
  				 <td><c:out value="${account.date}" /></td>
 		</tr>
 		</tbody>
		</table>
		</div>
		<p ><b><fmt:message key="account.currentBalance"/>:<c:out value="${account.balance}" /></b></p>
		</div>
	</div>
	 <div id="modalMessage" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/errorMessage.jspf"%>
		</div>
	 <div id="modalEdit" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/editClient.jspf"%>
		</div>
		<div id="modalChange" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/change.jspf"%>
		</div>
	
		<!--==============================Footer============================-->
<%@ include file="/WEB-INF/jspf/footer.jspf"%>
<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
</body>

</html>