<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ taglib uri="/WEB-INF/employeeCustomTag.tld" prefix="employeeTable"%>
<!doctype html>
<html lang="en">
<fmt:message key="title.main" var="title"/>
<c:set var="title" scope="request" value="${title }"/>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
<!--==============================Header=============================-->

<%@ include file="/WEB-INF/jspf/header.jspf"%>

 <!--==============================Navarea=============================-->
<c:if test="${userRole.name == 'admin'}">
 <div class="nav-area">
   <%@ include file="/WEB-INF/jspf/adminMenu.jspf"%>
  </div>
 </c:if>
 <c:if test="${userRole.name == 'user'}">
 <div class="nav-area">
   <%@ include file="/WEB-INF/jspf/userMenu.jspf"%>
  </div>
 </c:if>
<!--=============================User=============================-->

<div class="info-wrapper">
<%@ include file="/WEB-INF/jspf/userInfo.jspf"%>
		
</div>
 <div id="modalMessage" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/errorMessage.jspf"%>
		</div>
 <div id="modalChange" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/change.jspf"%>
		</div>
		 <div id="modalEdit" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/editClient.jspf"%>
		</div>
<!--==============================Footer============================-->
<%@ include file="/WEB-INF/jspf/footer.jspf"%>
<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
</body>
</html>
