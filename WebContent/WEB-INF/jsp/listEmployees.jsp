<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ taglib uri="/WEB-INF/employeeCustomTag.tld" prefix="mytag"%> 
<html>
<fmt:message key="title.employee" var="title"/>
<c:set var="title" scope="request" value="${title}"/>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
<body class="security-app">

	<!-- Header  -->
	<%@ include file="/WEB-INF/jspf/header.jspf"%>
	<div class="lc-block">
 <!--==============================Navarea=============================-->
 <div class="nav-area">
   <%@ include file="/WEB-INF/jspf/adminMenu.jspf"%>
  </div>
		<div class="table-wrapper">
		<!-- Employee table -->
	   
		<table id="table-info" class="table table-hover " >
		
		<thead>
		
			 <tr>
			 	
				<th class="table-content th-hide"><fmt:message key="entity.id"/></th>
 				<th class="table-content"><fmt:message key="entity.name"/></th>
  				<th class="table-content"><fmt:message key="entity.surname"/></th>
  				<th class="table-content th-hide"><fmt:message key="entity.address"/></th>
  				<th class="table-content"><fmt:message key="entity.phone"/></th>
  				<th class="table-content th-hide"><fmt:message key="employee.position"/></th>
  				<th class="table-content th-hide"><fmt:message key="entity.email"/></th>
				<th > 
				<a href="#" class="add-btn btn btn-default glyphicon glyphicon-plus"></a></th>
 		</tr>
 		
		</thead>
		<tbody>
		
			 <c:forEach var="employee" items="${employee}">
			 <tr>
				 <mytag:employeeTable employee="${employee}"/>
				  <td>
				           	<form action="controller" method="POST" id="table_form">
							<input type="hidden" name="command" value="deleteEmployee" /> 
							<input name="employeeID" value="${employee.id}" hidden="true">
							 <button id="save" type="submit" class="btn btn-default control">
          					<span class="glyphicon glyphicon-remove"></span> 
        					</button>
 							</form>
 				  			<a href="#" class="btn btn-default glyphicon glyphicon-pencil edit"></a>
 				</td>
 				</tr>
 					
 			</c:forEach>
			</tbody>
		</table>
		</div>
	</div>
	
		 <div id="modalAdd" class="modal">
		 <div class="registration modal-add" id="add-form" >
			<span class="close" id="closeModal">&times;</span>
			<div class="modal-title">
			<h1 id="modal-title"></h1>
			<br>
			</div>
		<form class="form-horizontal" id="multiform" action="controller" method="POST">
		<input type="hidden" id="id" name="id" />
		<input type="hidden" name="command" id="command"  />
			<%@ include file="/WEB-INF/jspf/modal/addUser.jspf"%>
		</form>
		</div>
		</div>
		 <div id="modalMessage" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/errorMessage.jspf"%>
		</div>
		<div id="modalChange" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/change.jspf"%>
		</div>
		<!--==============================Footer============================-->
<%@ include file="/WEB-INF/jspf/footer.jspf"%>
<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
<%@ include file="/WEB-INF/jspf/dataTable.jspf"%>
<script>
//Get the modal
$(document).ready(function () {
	$(".edit").click(function() {
	    var columnEmployees= ["id","name","surname","address","phone","positionInput","email"]
	    var columnValues = $(this).parent().siblings().map(function() {
	              return $(this).text();
	    }).get();
	    document.getElementById("command").value ="updateEmployees";
	    	$.each(columnEmployees, function(i, columnHeader) {
	    	    document.getElementById(columnHeader).value =columnValues[i];
	    	    $("#password").hide();
	    		$("#login").hide();
	    		$("#position").show();
	    		
	    	});
	document.getElementById("modal-title").innerHTML="<fmt:message key="edit.employee"/>";
		$("#modalAdd").show();
	});

	var title = document.title;
    $('.add-btn').on('click', function () {
    	document.getElementById("command").value ="addEmployees";
     		document.getElementById("modal-title").innerHTML="<fmt:message key="modal.header.employee"/>";
    		$("#password").show();
    		$("#position").show();
    		$("#login").show();
    	$("#modalAdd").show();
     
    });
   
  
});
</script>
</body>

</html>