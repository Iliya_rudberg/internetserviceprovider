
<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ taglib uri="/WEB-INF/taskCustomTag.tld" prefix="mytag"%> 

<html>
<fmt:message key="title.tasks" var="title"/>
<c:set var="title" scope="request" value="${title }"/>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
<body class="security-app">

	<!-- Header  -->
	<%@ include file="/WEB-INF/jspf/header.jspf"%>
	<div class="lc-block">

		 <!--==============================Navarea=============================-->
 <div class="nav-area">
   <%@ include file="/WEB-INF/jspf/adminMenu.jspf"%>
  </div>
  <div class="table-wrapper">
		<!-- Task table -->
	  
		<table id="table-info" class="table table-hover" >
		
		<thead >
		
			 <tr>
			 	
			<th  class="table-content th-hide"><fmt:message key="entity.id"/></th>
 				<th  class="table-content"><fmt:message key="entity.name"/></th>
  				<th class="table-content"><fmt:message key="entity.phone"/></th>
  				<th class="table-content th-hide"><fmt:message key="entity.email"/></th>
  				<th class="table-content"><fmt:message key="task.message"/></th>
  				<th class="table-content th-hide"><fmt:message key="task.date"/></th>
				<th > 
 		</tr>
 		
		</thead>
		<tbody>
			<c:if test="${not empty task}">
				 <c:forEach var="task" items="${task}">
			 <tr>
						<mytag:taskTable task="${task}"/>
					 <td>
				           	<form action="controller" method="POST" id="table_form">
							<input type="hidden" name="command" value="completeTask" /> 
							<input name="taskID" value="${task.id}" hidden="true">
							 <button id="save" type="submit" class="btn btn-default control">
          					<span class="glyphicon glyphicon-ok"></span> 
        					</button>
 							</form>
 				</td>
				</tr>
				
 			</c:forEach>
 				</c:if>
 				<c:if test="${empty task }">
 				<tr><td>Task list is empty</td></tr>
		</c:if>
			</tbody>
		</table>
		 <div id="modalMessage" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/errorMessage.jspf"%>
		</div>
		</div>
		</div>
	
		 <div id="modalChange" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/change.jspf"%>
		</div>
		<!--==============================Footer============================-->
<%@ include file="/WEB-INF/jspf/footer.jspf"%>
<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
<%@ include file="/WEB-INF/jspf/dataTable.jspf"%>
</body>

</html>