<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ taglib uri="/WEB-INF/clientCustomTag.tld" prefix="mytag"%> 

<html>
<fmt:message key="title.payments" var="title"/>
<c:set var="title" scope="request" value="${title }"/>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
<body class="security-app">


	<!-- Header  -->
	<%@ include file="/WEB-INF/jspf/header.jspf"%>
	<div class="lc-block">

		 <!--==============================Navarea=============================-->
 <div class="nav-area">
   <%@ include file="/WEB-INF/jspf/userMenu.jspf"%>
  </div>
  <div class="table-wrapper">
  
	
	   <h2 style="text-align:center;"><fmt:message key="header.payments"/></h2> 
	   <p style="text-align:center;"><fmt:message key="payments.description"/></p>
	     <div class="table-responsive" style="overflow-x:auto;">
		<table id="table-info" class="table table-hover transaction-table" >
 
      <thead>
			 <tr>
			    <th  class="table-content"><fmt:message key="transaction.date"/></th>
 				<th  class="table-content"><fmt:message key="transaction.sum"/></th>
  				<th class="table-content"><fmt:message key="transaction.type"/></th>
 		</tr>
 		</thead>
 		<tbody>
 		   <c:if test="${not empty transaction }">
 		<c:forEach var="transaction" items="${transaction}">
 		<tr>
 		   <td><c:out value="${transaction.date}" /></td>
 		   <td><c:out value="${transaction.sum}" /></td>
 		   <td><c:out value="${transaction.type}" /></td>
 		</tr>
 		</c:forEach>
 		</c:if>
 		 <c:if test="${empty transaction }">
        <tr><td><c:out value="${errorMessage }"/></td></tr>
    </c:if>  
 		</tbody>
      
   
	</table>
	</div>
		</div>
	</div>
	 <div id="modalMessage" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/errorMessage.jspf"%>
		</div>
	 <div id="modalEdit" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/editClient.jspf"%>
		</div>
		<div id="modalChange" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/change.jspf"%>
		</div>
		<!--==============================Footer============================-->
<%@ include file="/WEB-INF/jspf/footer.jspf"%>
<%@ include file="/WEB-INF/jspf/scripts.jspf"%> 
<%@ include file="/WEB-INF/jspf/dataTable.jspf"%>
</body>

</html>