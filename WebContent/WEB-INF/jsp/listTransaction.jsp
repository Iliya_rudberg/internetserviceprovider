<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ taglib uri="/WEB-INF/transactionCustomTag.tld" prefix="mytag"%> 
<html>
<fmt:message key="title.transaction" var="title"/>
<c:set var="title" scope="request" value="${title}"/>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
<body class="security-app">

	<!-- Header  -->
	<%@ include file="/WEB-INF/jspf/header.jspf"%>
	<div class="lc-block">
 <!--==============================Navarea=============================-->
 <div class="nav-area">
   <%@ include file="/WEB-INF/jspf/adminMenu.jspf"%>
  </div>
		<div class="table-wrapper">
		<!-- Transaction table -->
		 
		<table id="table-info" class="table table-hover" >
		
		<thead>
		
			 <tr>
			 	
				<th class="table-content"><fmt:message key="transaction.id"/></th>
 				<th class="table-content"><fmt:message key="transaction.contract.id"/></th>
 				<th class="table-content th-hide"><fmt:message key="transaction.type"/></th>
  				<th class="table-content"><fmt:message key="transaction.sum"/></th>
  				<th class="table-content th-hide"><fmt:message key="transaction.employee.id"/></th>
  				<th class="table-content"><fmt:message key="transaction.date"/></th>
				<th ><a href="#" class="add-btn btn btn-default glyphicon glyphicon-plus"></a></th>
 		    </tr>
 		
		</thead>
		<tbody>
			 <c:forEach var="transaction" items="${transaction}">
			 <tr>
			<mytag:transactionTable transaction="${transaction}"/>	
			          <td>
			 		<form action="controller" method="POST" id="table_form">
							<input type="hidden" name="command" value="deleteTransaction" /> 
							<input name="transactionID" value="${transaction.id}" hidden="true">
							 <button id="save" type="submit" class="btn btn-default control">
          					<span class="glyphicon glyphicon-remove"></span> 
        					</button>
 							</form>
 				  			<a href="#" class="btn btn-default glyphicon glyphicon-pencil edit" data-toggle="modal" data-target="#myModal"></a>
 				</td>	
 				</tr>
 			</c:forEach>
			</tbody>
		</table>
		</div>
	</div>
	 <div id="modalMessage" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/errorMessage.jspf"%>
		</div>
		 <div id="modalAdd" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/addTransaction.jspf"%>
		</div>
		<div id="modalChange" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/change.jspf"%>
		</div>
		<!--==============================Footer============================-->
<%@ include file="/WEB-INF/jspf/footer.jspf"%>
<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
<%@ include file="/WEB-INF/jspf/dataTable.jspf"%>
<script>
	$('.form_date').datetimepicker({
		minView: 2,
	 format: 'yyyy-mm-dd'	,
    });
</script>
<script>
//Get the modal
$(document).ready(function () {
	var title = document.title;
    $('.add-btn').on('click', function () {
    	document.getElementById("command").value ="addTransactions";
    		document.getElementById("modal-title").innerHTML="<fmt:message key="modal.header.transaction"/>";
    	 $("#employee-group").hide();
     	   $("#date-group").hide();
    	$("#modalAdd").show();
    });

    $(".edit").click(function() {
        var columnTransactions= ["id","contractID","transactionType","transactionSum","employeeId", "date"]
        var columnValues = $(this).parent().siblings().map(function() {
                  return $(this).text();
        }).get();
        document.getElementById("command").value ="updateTransactions";
       	   $.each(columnTransactions, function(i, columnHeader) {
       	        document.getElementById(columnHeader).value =columnValues[i];
       	     $("#employee-group").show();
       	   $("#date-group").show();
       	  $("#status-group").show();
       	      document.getElementById("modal-title").innerHTML="<fmt:message key="edit.transaction"/>";
       	   });
    	
    	$("#modalAdd").show();
    });
});
</script>
<script>

</script>
</body>

</html>