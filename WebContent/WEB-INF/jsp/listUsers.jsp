<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ taglib uri="/WEB-INF/userCustomTag.tld" prefix="mytag"%> 
<html>
<!-- head  -->
<fmt:message key="title.users" var="title"/>
<c:set var="title" scope="request" value="${title }"/>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body class="security-app">

	<!-- Header  -->
	<%@ include file="/WEB-INF/jspf/header.jspf"%>

	<div class="lc-block">
		
		 <!--==============================Navarea=============================-->
 <div class="nav-area">
   <%@ include file="/WEB-INF/jspf/adminMenu.jspf"%>
  </div>
		<div class="table-wrapper">
		<!-- Users table -->
		<table id="table-info" class="table table-hover">
		<thead>
			 <tr>
				<th><fmt:message key="user.id"/></th>
 				<th><fmt:message key="user.login"/></th>
  				<th><fmt:message key="user.role"/></th>
 		</tr>
		</thead>
		<tbody>
			 <c:forEach var="users" items="${users}">
			 <tr>
				<mytag:userTable user="${users}"/>
				</tr>
 			</c:forEach>
			</tbody>
		</table>
	</div>
	</div>
	 <div id="modalMessage" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/errorMessage.jspf"%>
		</div>
	 <div id="modalChange" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/change.jspf"%>
		</div>
	<!--==============================Footer============================-->
<%@ include file="/WEB-INF/jspf/footer.jspf"%>
<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
<%@ include file="/WEB-INF/jspf/dataTable.jspf"%>
</body>

</html>