<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ taglib uri="/WEB-INF/tariffCustomTag.tld" prefix="mytag"%> 

<html>
<fmt:message key="title.accountState" var="title"/>
<c:set var="title" scope="request" value="${title }"/>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<fmt:message key="menu.action.change.password" var="changePassword"/>
<body>
<body class="security-app">


	<!-- Header  -->
	<%@ include file="/WEB-INF/jspf/header.jspf"%>
	<div class="lc-block">

		 <!--==============================Navarea=============================-->
 <div class="nav-area">
   <%@ include file="/WEB-INF/jspf/userMenu.jspf"%>
  </div>
  <div class="table-wrapper">
  <div class="row">
    <div class="col-xs-12">
     <h2 style="text-align:center;"><fmt:message key="header.tariffs_services"/></h2> 
	   <p style="text-align:center;"><fmt:message key="service.description"/></p>
	   <h2 style="text-align:center;"></h2> 
      <div class="table-responsive" style="overflow-x:auto;">
	  
		
		<table class="table table-hover " >
		 <caption class="text-center"><fmt:message key="service.order"/></caption>
		<thead>
			 <tr align="center"> 
			    <th class="table-content" colspan="5"><fmt:message key="service.contract"/> - <c:out value="${account.id}" /></th>
			    </tr>
			    <tr align="center">
 				<th class="table-content" style="width:5%;"><fmt:message key="service.service"/></th>			
  				<th class="table-content"><fmt:message key="service.period"/></th>  			
  				<th class="table-content"><fmt:message key="service.cost"/></th>
  				<th class="table-content"><fmt:message key="service.date"/></th>
  				<th class="table-content"><fmt:message key="service.action"/></th>	
 		</tr>
 		</thead>
 		<tbody>
 		<tr>
 		<td><fmt:message key="service.service.text"/> </td>
 		<td><fmt:message key="service.period.month"/> </td>
 		<td><c:out value="${account.tariff.cost}" /> </td>
 		<td><c:out value="${account.date}" /> </td>
 		<td><input class="changePassword btn btn-default"  value="${changePassword }"></td>
 		</tr>
 		</tbody>
		</table >
		</div>
		</div>
		</div>
		<h2 style="text-align:center;"><fmt:message key="service.change"/></h2> 
		<p style="text-align:center;"><fmt:message key="service.current"/> "<c:out value="${account.tariff.name}" />"</p>
		<form class="form-horizontal" id="multiform" action="controller" method="POST">
		<input type="hidden" id="command" name="command"  value="changeTariff"/>
		<input type="hidden" name=url value="${requestScope['javax.servlet.forward.query_string']}">
		<table id="table-info" class="table table-hover "  >
		
		<thead>
		
			 <tr>
				 <th colspan="1" rowspan ="2" class="table-content"><fmt:message key="tariff.choose"/></th>
 				<th colspan="1" rowspan ="2" class="table-content"><fmt:message key="tariff.name"/></th>
  				<th colspan="1" rowspan ="2" class="table-content"><fmt:message key="tariff.cost"/></th>
  				<th colspan="1" rowspan ="2" class="table-content th-hide"><fmt:message key="tariff.type"/></th>
  				<th colspan="1" rowspan ="2" class="table-content th-hide"><fmt:message key="tariff.traffic"/></th>
  				<th colspan="1" rowspan ="2" class="table-content"><fmt:message key="tariff.description"/></th>
				<th colspan="2" style="text-align: center" class="th-hide" ><fmt:message key="tariff.speed"/></th>
 		</tr>
 		 <tr>
		 <th class="table-content th-hide"><fmt:message key="tariff.speed.upload"/></th>
				<th class="table-content th-hide"><fmt:message key="tariff.speed.download"/></th>
 				
 		</tr>
		</thead>
		
		<tbody>
		
			 <c:forEach var="tariff" items="${tariff}">
			 
			 <tr>
				<td style="width:20px;">
				
		 		
				<input type="radio" value="${tariff.id}" name="tariffID">
				
				
				</td>
				<td><c:out value="${tariff.name}" /> </td>
				<td><c:out value="${tariff.cost}" /> </td>
				<td class="th-hide"><c:out value="${tariff.type}" /> </td>
				<td class="th-hide"><c:out value="${tariff.traffic}" /> </td>
				<td ><c:out value="${tariff.description}" /> </td>
				<td class="th-hide"><c:out value="${tariff.upload}" /> </td>
				<td class="th-hide"><c:out value="${tariff.download}" /> </td>
			 
 				</tr>
 			</c:forEach>
 			
			</tbody>
		</table>
		<center>
	 	<input id="save"  type="submit" class="btn btn-primary" value=<fmt:message key="button.change.tariff"/>>
		</center>
		</form>
		
		 <div id="modalMessage" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/errorMessage.jspf"%>
		</div>
 		<div id="modalChange" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/change.jspf"%>
		</div>
		</div>
	</div>
	 <div id="modalEdit" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/editClient.jspf"%>
		</div>
		<!--==============================Footer============================-->
<%@ include file="/WEB-INF/jspf/footer.jspf"%>
<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
<script>
$('#save').click(function(){$('.multiform').submit();})
</script>

</body>

</html>