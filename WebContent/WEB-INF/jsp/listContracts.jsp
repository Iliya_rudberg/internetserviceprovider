<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ taglib uri="/WEB-INF/contractCustomTag.tld" prefix="mytag"%> 

<html>
<fmt:message key="title.contract" var="title"/>
<c:set var="title" scope="request" value="${title }"/>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
<body class="security-app">

	<!-- Header  -->
	<%@ include file="/WEB-INF/jspf/header.jspf"%>
 <!--==============================Navarea=============================-->
 <div class="nav-area">
   <%@ include file="/WEB-INF/jspf/adminMenu.jspf"%>
  </div>
		<div class="table-wrapper">
		<!-- Tariff table -->
		 
		<table id="table-info" class="table table-hover">
		
		<thead>
		
			 <tr>
			 	
				<th class="table-content"><fmt:message key="contract.id"/></th>
 				<th class="table-content"><fmt:message key="contract.balance"/></th>
 				<th class="table-content th-hide"><fmt:message key="contract.tariff.id"/></th>
  				<th class="table-content th-hide" ><fmt:message key="contract.date"/></th>
  				<th class="table-content th-hide"><fmt:message key="contract.employee.id"/></th>
  				<th class="table-content"><fmt:message key="contract.client.id"/></th>
  				<th class="table-content"><fmt:message key="contract.status"/></th>
				<th class="table-content"><a href="#" class="add-btn btn btn-default glyphicon glyphicon-plus"></a></th>
 		</tr>
 		
		</thead>
		<tbody>
			 <c:forEach var="contract" items="${contract}">
			 <tr>
					<mytag:contractTable contract="${contract}"/>
					<td>
			 		<form action="controller" method="POST" id="table_form">
							<input type="hidden" name="command" value="deleteContract" /> 
							<input name="contractID" value="${contract.id}" hidden="true">
							 <button id="save" type="submit" class="btn btn-default control">
          					<span class="glyphicon glyphicon-remove"></span> 
        					</button>
 							</form>
 				  			<a href="#" class="btn btn-default glyphicon glyphicon-pencil edit"></a>
 				</td>
				</tr>
 			</c:forEach>
			</tbody>
		</table>
		
	</div>
	 <div id="modalMessage" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/errorMessage.jspf"%>
		</div>
		 <div id="modalAdd" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/addContract.jspf"%>
		</div>
		<div id="modalChange" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/change.jspf"%>
		</div>
		<!--==============================Footer============================-->
<%@ include file="/WEB-INF/jspf/footer.jspf"%>
<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
<script>

	$('.form_date').datetimepicker({
		minView: 2,
		format: 'yyyy-mm-dd'	,
    });
</script>
<%@ include file="/WEB-INF/jspf/dataTable.jspf"%>
<script >
//Get the modal
$(document).ready(function () {
	$(".edit").click(function() {
	    var columnContracts= ["id","balance","tariffId","date","employeeId","clientId","status"]
	    var columnValues = $(this).parent().siblings().map(function() {
	              return $(this).text();
	    }).get();
	    document.getElementById("command").value ="updateContracts";
	 	   $.each(columnContracts, function(i, columnHeader) {
	 	        document.getElementById(columnHeader).value =columnValues[i];
	 	       document.getElementById("modal-title").innerHTML="<fmt:message key="edit.contract"/>";
	 	   });
		$("#modalAdd").show();
	});

	var title = document.title;
    $('.add-btn').on('click', function () {
    	document.getElementById("command").value ="addContracts";
    		document.getElementById("modal-title").innerHTML="<fmt:message key="modal.header.contract"/>";
    	$("#modalAdd").show();
     
    });
 
});
</script>
</body>
</html>