<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ taglib uri="/WEB-INF/tariffCustomTag.tld" prefix="mytag"%> 
<html>
<fmt:message key="title.tariff" var="title"/>
<c:set var="title" scope="request" value="${title }"/>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
<body class="security-app">

	<!-- Header  -->
	<%@ include file="/WEB-INF/jspf/header.jspf"%>
	<div class="lc-block">
 <!--==============================Navarea=============================-->
 <div class="nav-area">
   <%@ include file="/WEB-INF/jspf/adminMenu.jspf"%>
  </div>
		<div class="table-wrapper">
		<!-- Tariff table -->
		<table id="table-info" class="table table-hover " >
		
		<thead>
		
			 <tr>
			 	
				<th class="table-content"><fmt:message key="tariff.id"/></th>
 				<th class="table-content"><fmt:message key="tariff.name"/></th>
  				<th class="table-content"><fmt:message key="tariff.cost"/></th>
  				<th class="table-content th-hide"><fmt:message key="tariff.type"/></th>
  				<th class="table-content th-hide"><fmt:message key="tariff.traffic"/></th>
  				<th class="table-content"><fmt:message key="tariff.description"/></th>
				<th class="table-content th-hide"><fmt:message key="tariff.speed.upload"/></th>
				<th class="table-content th-hide"><fmt:message key="tariff.speed.download"/></th>
				<th > 
				<a href="#" class="add-btn btn btn-default glyphicon glyphicon-plus"></a></th>
 		</tr>
 		
		</thead>
		<tbody>
	
			 <c:forEach var="tariff" items="${tariff}">
			 <tr>
			<mytag:tariffTable tariff="${tariff}"/>
			  <td>
			 		<form action="controller" method="POST" id="table_form">
							<input type="hidden" name="command" value="deleteTariff" /> 
							<input name="tariffID" value="${tariff.id}" hidden="true">
							 <button id="save" type="submit" class="btn btn-default control">
          					<span class="glyphicon glyphicon-remove"></span> 
        					</button>
 							</form>
 				  			<a href="#" class="btn btn-default glyphicon glyphicon-pencil edit" data-toggle="modal" data-target="#myModal"></a>
 				</td>
 				</tr>
 			</c:forEach>
			</tbody>
		</table>
		</div>
		</div>
	 <div id="modalMessage" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/errorMessage.jspf"%>
		</div>
		 <div id="modalAdd" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/addTariff.jspf"%>
		</div>
		<div id="modalChange" class="modal">
		<%@ include file="/WEB-INF/jspf/modal/change.jspf"%>
		</div>
		<!--==============================Footer============================-->
<%@ include file="/WEB-INF/jspf/footer.jspf"%>
<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
<%@ include file="/WEB-INF/jspf/dataTable.jspf"%>
<script>
//Get the modal
$(document).ready(function () {
	var title = document.title;
    $('.add-btn').on('click', function () {
    	document.getElementById("command").value ="addTariffs";
    		document.getElementById("modal-title").innerHTML="<fmt:message key="modal.header.tariff"/>";
    	$("#modalAdd").show();
     
    });
   
    $(".edit").click(function() {
        var columnTariffs= ["id","name","cost","type","traffic","description","download","upload"]
        var columnValues = $(this).parent().siblings().map(function() {
                  return $(this).text();
        }).get();
        document.getElementById("command").value ="updateTariffs";
      	   $.each(columnTariffs, function(i, columnHeader) {
      	        document.getElementById(columnHeader).value =columnValues[i];
      	      document.getElementById("modal-title").innerHTML="<fmt:message key="edit.tariff"/>";
      	   });
    	
    	$("#modalAdd").show();
    });
});
</script>
</body>

</html>