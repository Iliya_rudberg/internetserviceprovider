package test;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.epam.isp.core.client.Client;
import com.epam.isp.core.client.ClientDao;
import com.epam.isp.core.user.User;
import com.epam.isp.core.user.UserDao;
import com.epam.isp.dao.ConnectionPool;
import com.epam.isp.dao.impl.ClientDaoImpl;
import com.epam.isp.dao.impl.UserDaoImpl;
import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;

public class ClientDaoTest {
	private static final Logger logger = Logger.getLogger(ClientDaoTest.class);
	private static ClientDao clientDao;
	private static int id;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		try{
			clientDao = new ClientDaoImpl();
			Class.forName("com.mysql.jdbc.Driver");
			MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
			dataSource.setURL("jdbc:mysql://localhost:3306/internetdb?useEncoding=true&amp;characterEncoding=UTF-8"); 
			dataSource.setUser("root"); 
			dataSource.setPassword("iliya1993"); 
			new ConnectionPool(dataSource);
		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
			logger.error("Cannot get a DataSource without JNDI");
		}
		UserDao dao = new UserDaoImpl();
		User user = new User("TestUser","GreaPassword11",1);
		dao.addUser(user);
		Client client = new Client("Test","Test","HB098765", "Minsk",298769878,"iliya.rudberg@gmail,com",UserDaoTest.getMaxId());
		clientDao.addClient(client);
		ClientDaoTest.id = getMaxId();
	}

	@Test
	public void testGetClients() {
		Assert.assertNotNull(clientDao.getClients());
	}

	@Test
	public void testGetClientByContractId() {
		Assert.assertNotNull(clientDao.getClientByContractId(10));
	}

	@Test
	public void testUpdateClientById() {
		Client client = new Client(1,"Iliya","Rudberg","HB098765", "Minsk",298769878,"iliya.rudberg@gmail,com");
		Assert.assertTrue(clientDao.updateClientById(client));
	}

	@Test
	public void testDeleteClientByID() {
		Assert.assertTrue(clientDao.deleteClientByID(ClientDaoTest.id));
	}

	@Test
	public void testGetClientByUserId() {
		Assert.assertNotNull(clientDao.getClientByUserId(1));
	}

	@Test
	public void testChangePassword() {
		Assert.assertNotNull(clientDao.changePassword(1, "password"));
	}
	public static int getMaxId(){
		int id;
		Connection connection = ConnectionPool.getConnection();
		try (PreparedStatement ps = connection.prepareStatement("select MAX(id) AS id FROM clients ")) {
			ps.execute();
			ResultSet resultSet = ps.getResultSet();
			resultSet.next();
			id = resultSet.getInt("id");
			resultSet.close();
			return id;
		} catch (SQLException ex) {
			logger.error("Cannot find the max id", ex);
			return 0;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					logger.error("Can not close connection");
				}
			}
		}
	}
}
