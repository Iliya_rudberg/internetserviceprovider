package test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.epam.isp.core.tariff.Tariff;
import com.epam.isp.core.tariff.TariffDao;
import com.epam.isp.core.tariff.TariffType;
import com.epam.isp.dao.ConnectionPool;
import com.epam.isp.dao.impl.TariffDaoImpl;
import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;

public class TariffDaoTest {
	private static  TariffDao tariffDao;
	private static final Logger logger = Logger.getLogger(TariffDaoTest.class);
	private static int id;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		try{
			tariffDao = new TariffDaoImpl();
			Class.forName("com.mysql.jdbc.Driver");
			MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
			dataSource.setURL("jdbc:mysql://localhost:3306/internetdb?useEncoding=true&amp;characterEncoding=UTF-8"); 
			dataSource.setUser("root"); 
			dataSource.setPassword("iliya1993"); 
			new ConnectionPool(dataSource);
		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
			logger.error("Cannot get a DataSource without JNDI");
		}
		Tariff tariff = new Tariff("test",0.3,TariffType.LIMITED,10,"TestDesc",25,25);
		tariffDao.addTariff(tariff);
		id = getMaxId();
	}

	@Test
	public void testGetTariffs() {
		Assert.assertNotNull(tariffDao.getTariffs());
	}

	@Test
	public void testGetTariffById() {
		Assert.assertNotNull(tariffDao.getTariffById(14));
	}


	@Test
	public void testDeleteTariffByID() {
		Assert.assertTrue(tariffDao.deleteTariffByID(TariffDaoTest.id));
	}

	@Test
	public void testUpdateTariffById() {
		Tariff tariff = new Tariff (14,"SocialUnlim",15,TariffType.LIMITED,10,"Good opportunity",25,25);
		Assert.assertTrue(tariffDao.updateTariffById(tariff));
	}
	public static int getMaxId(){
		int id;
		Connection connection = ConnectionPool.getConnection();
		try (PreparedStatement ps = connection.prepareStatement("select MAX(id) AS id FROM tariff ")) {
			ps.execute();
			ResultSet resultSet = ps.getResultSet();
			resultSet.next();
			id = resultSet.getInt("id");
			resultSet.close();
			return id;
		} catch (SQLException ex) {
			logger.error("Cannot find the max id", ex);
			return 0;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					logger.error("Can not close connection");
				}
			}
		}
	}

}
