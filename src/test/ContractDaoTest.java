package test;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import com.epam.isp.core.contract.Contract;
import com.epam.isp.core.contract.ContractDao;
import com.epam.isp.core.contract.Status;
import com.epam.isp.dao.ConnectionPool;
import com.epam.isp.dao.impl.ContractDaoImpl;
import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;

public class ContractDaoTest {
	private static final Logger logger = Logger.getLogger(ContractDaoTest.class);
	private static ContractDao contractDao;
	private static int id;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		try{
			contractDao = new ContractDaoImpl();
			Class.forName("com.mysql.jdbc.Driver");
			MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
			dataSource.setURL("jdbc:mysql://localhost:3306/internetdb?useEncoding=true&amp;characterEncoding=UTF-8"); 
			dataSource.setUser("root"); 
			dataSource.setPassword("iliya1993"); 
			new ConnectionPool(dataSource);
		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
			logger.error("Cannot get a DataSource without JNDI");
		}
		
		Contract contract = new Contract(10,31,1, Date.valueOf("2017-04-09"),Status.WORKING,14);
		contractDao.addContract(contract);
		ContractDaoTest.id = getMaxId();
	}


	@Test
	public void testGetContractByClientId() {
		Assert.assertNotNull(contractDao.getContractByClientId(1));
	}

	@Test
	public void testGetContractsByEmployeeId() {
		Assert.assertNotNull(contractDao.getContractsByEmployeeId(31));
	}

	@Test
	public void testChangeStatusOfContract() {
		Assert.assertTrue(contractDao.changeStatusOfContract(new Contract(10,Status.WORKING)));
	}


	@Test
	public void testGetContracts() {
		Assert.assertNotNull(contractDao.getContracts());
	}

	@Test
	public void testDeleteContractByID() {
		Assert.assertTrue(contractDao.deleteContractByID(ContractDaoTest.id));
	}

	@Test
	public void testUpdateContractById() {
		Contract contract = new Contract(10,10,31,1, Date.valueOf("2017-04-09"),Status.WORKING,15);
		Assert.assertTrue(contractDao.updateContractById(contract));
	}

	@Test
	public void testGetContractById() {
		Assert.assertNotNull(contractDao.getContractById(10));
	}

	@Test
	public void testChangeTariffOfContract() {
		Contract contract = new Contract(10,15);
		Assert.assertTrue(contractDao.changeTariffOfContract(contract));
	}
	public static int getMaxId(){
		int id;
		Connection connection = ConnectionPool.getConnection();
		try (PreparedStatement ps = connection.prepareStatement("select MAX(id) AS id FROM contracts ")) {
			ps.execute();
			ResultSet resultSet = ps.getResultSet();
			resultSet.next();
			id = resultSet.getInt("id");
			resultSet.close();
			return id;
		} catch (SQLException ex) {
			logger.error("Cannot find the max id", ex);
			return 0;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					logger.error("Can not close connection");
				}
			}
		}
	}
}
