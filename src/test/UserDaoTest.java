package test;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import com.epam.isp.core.user.User;
import com.epam.isp.core.user.UserDao;
import com.epam.isp.dao.ConnectionPool;
import com.epam.isp.dao.impl.UserDaoImpl;
import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;

public class UserDaoTest {
	private static final Logger logger = Logger.getLogger(UserDaoTest.class);
	private static UserDao userDao = new UserDaoImpl();
	private static int id;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		try{
			Class.forName("com.mysql.jdbc.Driver");
			MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
			dataSource.setURL("jdbc:mysql://localhost:3306/internetdb?useEncoding=true&amp;characterEncoding=UTF-8"); 
			dataSource.setUser("root"); 
			dataSource.setPassword("iliya1993"); 
			new ConnectionPool(dataSource);
		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
			logger.error("Cannot get a DataSource without JNDI");
		}
		User user = new User ("UserTest","GreatPassword1111",2);
		userDao.addUser(user);
		UserDaoTest.id = getMaxId();
		
	}

	@Test
	public void testGetUsers() {
		Assert.assertNotNull(userDao.getUsers());
	}

	@Test
	public void testDeleteUserByID() {
	
		Assert.assertTrue(userDao.deleteUserByID(UserDaoTest.id));
	}


	@Test
	public void testGetUserByLogin() {
		Assert.assertNotNull(userDao.getUserByLogin("admin"));
	}

	@Test
	public void testGetUserById() {
		Assert.assertNotNull(userDao.getUserById(1));
	}

	@Test
	public void testGetRoles() {
		Assert.assertNotNull(userDao.getRoles());
	}
	public static int getMaxId(){
		int id;
		Connection connection = ConnectionPool.getConnection();
		try (PreparedStatement ps = connection.prepareStatement("select MAX(id) AS id FROM users ")) {
			ps.execute();
			ResultSet resultSet = ps.getResultSet();
			resultSet.next();
			id = resultSet.getInt("id");
			resultSet.close();
			return id;
		} catch (SQLException ex) {
			logger.error("Cannot find the max id", ex);
			return 0;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					logger.error("Can not close connection");
				}
			}
		}
	}

}
