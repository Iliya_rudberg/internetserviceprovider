package test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.epam.isp.core.employee.Employee;
import com.epam.isp.core.employee.EmployeeDao;
import com.epam.isp.core.user.User;
import com.epam.isp.core.user.UserDao;
import com.epam.isp.dao.ConnectionPool;
import com.epam.isp.dao.impl.EmployeeDaoImpl;
import com.epam.isp.dao.impl.UserDaoImpl;
import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;

public class EmployeeDaoTest {
	private static final Logger logger = Logger.getLogger(EmployeeDaoTest.class);
	private static EmployeeDao employeeDao;
	private static int id;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		try{
			employeeDao = new EmployeeDaoImpl();
			Class.forName("com.mysql.jdbc.Driver");
			MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
			dataSource.setURL("jdbc:mysql://localhost:3306/internetdb?useEncoding=true&amp;characterEncoding=UTF-8"); 
			dataSource.setUser("root"); 
			dataSource.setPassword("iliya1993"); 
			new ConnectionPool(dataSource);
		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
			logger.error("Cannot get a DataSource without JNDI");
		}
		UserDao dao = new UserDaoImpl();
		User user = new User("TestUser","GreaPassword11",1);
		dao.addUser(user);
		Employee employee = new Employee("Test","Test","Address", 29102102,"Boss","iliya.rudberg@gmail,com",UserDaoTest.getMaxId());
		employeeDao.addEmployee(employee);
		EmployeeDaoTest.id = getMaxId();
	}

	@Test
	public void testGetEmployee() {
		Assert.assertNotNull(employeeDao.getEmployee());
	}

	

	@Test
	public void testGetEmployeeById() {
		Assert.assertNotNull(employeeDao.getEmployeeById(31));
	}

	@Test
	public void testGetEmployeeByUserId() {
		Assert.assertNotNull(employeeDao.getEmployeeByUserId(1));
	}

	@Test
	public void testDeleteEmployeeByID() {
		Assert.assertTrue(employeeDao.deleteEmployeeByID(id));
	}

	@Test
	public void testUpdateEmployeeById() {
		Employee employee = new Employee("Iliya","Rudberg","Minsk", 259078236,"Boss","iliya.rudberg@gmail.com",1);
		Assert.assertTrue(employeeDao.updateEmployeeById(employee));
	}
	public static int getMaxId(){
		int id;
		Connection connection = ConnectionPool.getConnection();
		try (PreparedStatement ps = connection.prepareStatement("select MAX(id) AS id FROM employees ")) {
			ps.execute();
			ResultSet resultSet = ps.getResultSet();
			resultSet.next();
			id = resultSet.getInt("id");
			resultSet.close();
			return id;
		} catch (SQLException ex) {
			logger.error("Cannot find the max id", ex);
			return 0;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					logger.error("Can not close connection");
				}
			}
		}
	}

}
