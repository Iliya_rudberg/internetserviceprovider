package test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.epam.isp.core.tasks.Task;
import com.epam.isp.core.tasks.TaskDao;
import com.epam.isp.dao.ConnectionPool;
import com.epam.isp.dao.impl.TaskDaoImpl;
import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;

public class TaskDaoTest {
	private static TaskDao taskDao;
	private static final Logger logger = Logger.getLogger(TaskDaoTest.class);
	private static int id;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		try{
			taskDao = new TaskDaoImpl();
			Class.forName("com.mysql.jdbc.Driver");
			MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
			dataSource.setURL("jdbc:mysql://localhost:3306/internetdb?useEncoding=true&amp;characterEncoding=UTF-8"); 
			dataSource.setUser("root"); 
			dataSource.setPassword("iliya1993"); 
			new ConnectionPool(dataSource);
		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
			logger.error("Cannot get a DataSource without JNDI");
		}
		Task task = new Task("Test",29292929,"iliya.rudberg@gmail.com", "This is a test message");
		taskDao.addTask(task);
		TaskDaoTest.id = getMaxId();
	
	
	}

	@Test
	public void testListTasks() {
		Assert.assertNotNull(taskDao.listTasks());
	}

	@Test
	public void testCompleteTask() {
		Assert.assertTrue(taskDao.completeTask(id));
	}
	public static int getMaxId(){
		int id;
		Connection connection = ConnectionPool.getConnection();
		try (PreparedStatement ps = connection.prepareStatement("select MAX(id) AS id FROM contact ")) {
			ps.execute();
			ResultSet resultSet = ps.getResultSet();
			resultSet.next();
			id = resultSet.getInt("id");
			resultSet.close();
			return id;
		} catch (SQLException ex) {
			logger.error("Cannot find the max id", ex);
			return 0;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					logger.error("Can not close connection");
				}
			}
		}
	}

}
