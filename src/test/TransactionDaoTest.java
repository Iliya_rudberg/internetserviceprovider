package test;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.epam.isp.core.transaction.Transaction;
import com.epam.isp.core.transaction.TransactionDao;
import com.epam.isp.core.transaction.TransactionType;
import com.epam.isp.dao.ConnectionPool;
import com.epam.isp.dao.impl.TransactionDaoImpl;
import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;

public class TransactionDaoTest {
	private static final Logger logger = Logger.getLogger(TransactionDaoTest.class);
	private static TransactionDao transactionDao;
	private static int id;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		try{
			transactionDao = new TransactionDaoImpl();
			Class.forName("com.mysql.jdbc.Driver");
			MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
			dataSource.setURL("jdbc:mysql://localhost:3306/internetdb?useEncoding=true&amp;characterEncoding=UTF-8"); 
			dataSource.setUser("root"); 
			dataSource.setPassword("iliya1993"); 
			new ConnectionPool(dataSource);
		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
			logger.error("Cannot get a DataSource without JNDI");
		}
		Transaction transaction = new Transaction (1.00,TransactionType.BANK,10,31);
		transactionDao.addTransaction(transaction);
		TransactionDaoTest.id = getMaxId();
		
	}

	@Test
	public void testGetTransactions() {
		Assert.assertNotNull(transactionDao.getTransactions());
	}

	@Test
	public void testGetTransactionsByEmployeeId() {
		Assert.assertNotNull(transactionDao.getTransactionsByEmployeeId(31));
	}

	@Test
	public void testGetTransactionById() {
		Assert.assertNotNull(transactionDao.getTransactionById(1));
	}

	@Test
	public void testUpdateTransactionById() {
		Transaction transaction = new Transaction (1,10,TransactionType.BANK,31,31.98,Date.valueOf("2017-4-06"));
		Assert.assertTrue(transactionDao.updateTransactionById(transaction));
	}

	@Test
	public void testDeleteTransactionByID() {
		Assert.assertTrue(transactionDao.deleteTransactionByID(TransactionDaoTest.id));
	}

	@Test
	public void testGetTransactionByContractId() {
		Assert.assertNotNull(transactionDao.getTransactionByContractId(10));
	}
	public static int getMaxId(){
		int id;
		Connection connection = ConnectionPool.getConnection();
		try (PreparedStatement ps = connection.prepareStatement("select MAX(id) AS id FROM transactions ")) {
			ps.execute();
			ResultSet resultSet = ps.getResultSet();
			resultSet.next();
			id = resultSet.getInt("id");
			resultSet.close();
			return id;
		} catch (SQLException ex) {
			logger.error("Cannot find the max id", ex);
			return 0;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					logger.error("Can not close connection");
				}
			}
		}
	}

}
