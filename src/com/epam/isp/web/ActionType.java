package com.epam.isp.web;

/**
 * Constants, which main purpose is to tell whether user want's to read or write
 * some data.
 *
 * @author Iliya Rudberg
 *
 */
public enum ActionType {
	GET, POST;
}
