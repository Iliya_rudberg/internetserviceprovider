package com.epam.isp.web.command;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

import com.epam.isp.core.user.Role;
import com.epam.isp.core.user.User;
import com.epam.isp.core.user.UserManager;
import com.epam.isp.core.user.UserManagerImpl;
import com.epam.isp.Path;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.utils.Utils;

/**
 * Invoked when user logins in the system.
 *
 * @author Iliya Rudberg.
 *
 */
public class LoginCommand extends Command {
	private static final long serialVersionUID = -3071536593627692473L;

	private static final Logger logger = Logger.getLogger(LoginCommand.class);
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.info("Start executing Command");

		String result = null;

		if (actionType == ActionType.POST) {
			result = doPost(request, response);
		} else {
			result = null;
		}

		logger.info("End executing command");
		return result;
	}

	/**
	 * Logins user in system. As first page displays depends on the user role.
	 *
	 * @return path to the page.
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response) {
		String forward = null;

		HttpSession session = request.getSession();

		String login = request.getParameter("login");
		String password = request.getParameter("password");
		password = Utils.generateHash(password);
		UserManager manager = new UserManagerImpl();
		User user = manager.getUserByLogin(login);
		logger.info("User found: " + user);

		if (user == null || !password.equals(user.getPassword())) {
			String lang = (String) request.getSession().getAttribute("lang");
			String errorMessage = "";
			if (lang == null || lang.equals("en")) {
				errorMessage = "Cannot find user with such login/password";
			} else if (lang.equals("ru")) {
				errorMessage = "Не удалось найти пользователя с подобным догином/паролем";
			}
			request.getSession().setAttribute("message", errorMessage);
			logger.error("errorMessage: Cannot find user with such login/password");
		} else {
			Role userRole = Role.getRole(user);
			logger.trace("userRole --> " + userRole);

			if (userRole == Role.ADMIN) {
				forward = Path.REDIRECT_TO_GET_EMPLOYEE;
			}

			if (userRole == Role.USER) {
				forward = Path.REDIRECT_TO_GET_CLIENT;
			}

			session.setAttribute("user", user);
			logger.trace("Set the session attribute: user --> " + user);

			session.setAttribute("userRole", userRole);
			logger.info("Set the session attribute: userRole --> " + userRole);

			logger.info("User " + user + " logged as " + userRole.toString().toLowerCase());
		}
		return forward;
	}

}