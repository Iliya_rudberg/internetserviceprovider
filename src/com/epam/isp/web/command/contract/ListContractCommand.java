package com.epam.isp.web.command.contract;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.epam.isp.Path;
import com.epam.isp.core.contract.Contract;
import com.epam.isp.core.contract.ContractManager;
import com.epam.isp.core.contract.ContractManagerImpl;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
/**
 * Invokes when user want to list contracts.
 * 
 * @author Iliya Rudberg.
 *
 */
public class ListContractCommand extends Command{
	private static final long serialVersionUID = 7723206619340362128L;

	private static final Logger logger = Logger.getLogger(ListContractCommand .class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");

		String result = null;

		if (ActionType.GET == actionType) {
			result = doGet(request, response);
		}

		logger.debug("Finished executing Command");
		return result;
	}

	/**
	 * Forward user to page of all contracts.
	 *
	 * @return path to view of all contracts page.
	 */
	private String doGet(HttpServletRequest request, HttpServletResponse response) {
		ContractManager manager = new ContractManagerImpl();

		Collection<Contract> contract = manager.getContracts();
		logger.trace("Contract found: " + contract);


		request.setAttribute("contract", contract);
		
		return Path.FORWARD_VIEW_ALL_CONTRACTS;
	}
}
