package com.epam.isp.web.command.contract;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.isp.Path;
import com.epam.isp.core.contract.Contract;
import com.epam.isp.core.contract.Status;
import com.epam.isp.core.contract.ContractManager;
import com.epam.isp.core.contract.ContractManagerImpl;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.utils.validation.ContractInputValidator;
/**
 * Invokes when user want to add contract.
 * 
 * @author Iliya Rudberg.
 *
 */
public class AddContractCommand extends Command {

	private static final long serialVersionUID = 3315917092461024179L;

	private static final Logger logger = Logger.getLogger(AddContractCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");

		String result = null;
		if (ActionType.POST == actionType) {
			result = doPost(request, response);
		}

		logger.debug("Finished executing Command");

		return result;
	}

	/**
	 * Redirects to view all contracts after submitting add contract form.
	 *
	 * @return path to the view of added contract if fields properly filled,
	 *         otherwise redisplays add contract page.
	 * @throws IOException
	 * @throws ServletException
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		double balance = 0.0;
		String lang = (String)request.getSession().getAttribute("language");
		String message = null;
		boolean complete= false;
		// validation
		
		boolean valid = ContractInputValidator.validateContractParametrs(request.getParameter("contractBalance"),
					   request.getParameter("contractClientID"),
					   request.getParameter("contractStatus"),
					   request.getParameter("contractTariffId"),
					   request.getParameter("date"));
		logger.trace("Validation: " + valid);
		if (valid) {
		String availableDate = request.getParameter("date");
		balance = Double.parseDouble(request.getParameter("contractBalance"));
		Status status = Status.valueOf(request.getParameter("contractStatus").toUpperCase());
		int  clientId = Integer.parseInt(request.getParameter("contractClientID"));
		int  tariffId = Integer.parseInt(request.getParameter("contractTariffId"));
		int employeesId = Integer.parseInt((request.getSession().getAttribute("employeeId")).toString());
		Date date = java.sql.Date.valueOf(availableDate);
		logger.trace("The fields got: "  + balance + " " + status.toString() + " " + clientId +   " "
				 + " " + balance + " " + employeesId + " "+ tariffId);
		// create contract entity
		Contract contract = new Contract(balance, employeesId, clientId, date,status,tariffId);
		logger.trace("Contract was created: " + contract);
		// add contract to database
			ContractManager manager = new ContractManagerImpl();
			complete = manager.addContract(contract);
		logger.trace("Contract was added to database");
		response.reset();
		}
		if (complete == true){
			if (lang == null || lang.equals("en")) { 
				message = "Contract was successfully added to database"; 
				} else if (lang.equals("ru")) { 
				message = "Контракт был успешно добавлен в базу данных"; 
				}else if (lang.equals("by")) { 
				message = "Кантракт быў паспяхова дададзены ў базу дадзеных"; 
				} }
			else{if (lang == null || lang.equals("en")) { 
					message = "Cannot add a contract"; 
					} else if (lang.equals("ru")) { 
					message = "Не удалось добавить контракт"; 
					}else if (lang.equals("by")) { 
					message = "Не ўдалося дабавiць  кантракт"; 
					} }
		request.getSession().setAttribute("message", message);
		return Path.REDIRECT_TO_VIEW_ALL_CONTRACTS;
	}

}
