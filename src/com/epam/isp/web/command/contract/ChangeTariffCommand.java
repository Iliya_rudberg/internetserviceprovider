package com.epam.isp.web.command.contract;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

import com.epam.isp.core.client.Client;
import com.epam.isp.core.client.ClientManager;
import com.epam.isp.core.client.ClientManagerImpl;
import com.epam.isp.core.contract.Contract;
import com.epam.isp.core.contract.ContractManager;
import com.epam.isp.core.contract.ContractManagerImpl;
import com.epam.isp.core.tariff.Tariff;
import com.epam.isp.core.tariff.TariffManager;
import com.epam.isp.core.tariff.TariffManagerImpl;
import com.epam.isp.core.user.User;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.utils.validation.Validation;
/**
 * Invokes when user want to change tariff in contract.
 * 
 * @author Iliya Rudberg.
 *
 */
public class ChangeTariffCommand extends Command {

	private static final long serialVersionUID = 3315917092461024179L;

	private static final Logger logger = Logger.getLogger(ChangeTariffCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");

		String result = null;
		if (ActionType.POST == actionType) {
			result = doPost(request, response);
		}

		logger.debug("Finished executing Command");

		return result;
	}

	/**
	 * Redirects to view all tariffs after submitting change contract form.
	 *
	 * @return path to the view of all tariffs if fields properly filled,
	 *         otherwise redisplays change tariff page.
	 * @throws IOException
	 * @throws ServletException
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String lang = (String)request.getSession().getAttribute("language");
		String message = null;
		boolean complete= false;
		// action
			final String action = "controller?";
			String url = request.getParameter("url");
			ContractManager manager = new ContractManagerImpl();
			ClientManager manager1 = new ClientManagerImpl();
	User user = (User)		request.getSession().getAttribute("user");
	int clientId = 0;
	int tariffId = 0;
	Contract contract;
	boolean valid = false;
	if(user.getRoleId() == 2){
		
	if (Validation.isInt(request.getParameter("tariffID")))
	{valid = true;
	}else {valid = false;}
	
	logger.trace("Validation: " + valid);

	if (valid){ 
		TariffManager tariffManager = new TariffManagerImpl();
	Client client = manager1.getClientByUserId(user.getId());
	tariffId = Integer.parseInt(request.getParameter("tariffID"));
	clientId = client.getId();
	Contract contr =(Contract) request.getSession().getAttribute("account");
	Tariff tariff = tariffManager.getTariffById(tariffId);
	contr.setTariff(tariff);
	//update tariff name at the page
	request.getSession().setAttribute("account", contr);
	}
	}
	else{
	if (	Validation.isInt(request.getParameter("contractID"))&&
			Validation.isInt(request.getParameter("tariffID")))
		{valid = true;}else {valid = false;}
		logger.trace("Validation: " + valid);
		if (valid){
		tariffId = Integer.parseInt(request.getParameter("tariffID"));
		clientId =  Integer.parseInt(request.getParameter("contractID"));
}}
	if (clientId !=0 && tariffId != 0) {
		
	logger.trace("The fields got: "  + clientId + " " + tariffId + " " );
	
	int contractId = manager.getContractByClientId(clientId).getId();
	// create contract entity
	contract = new Contract(contractId, tariffId);
	logger.trace("Contract was changed: " + contract);
	// update contract at database
	complete = manager.changeTariffOfContract(contract);
	
	
	logger.trace("Tariff of contract was changed");
	response.reset();
	}if (complete == true){
		if (lang == null || lang.equals("en")) { 
			message = "Tariff of contract was changed"; 
			} else if (lang.equals("ru")) { 
			message = "Тариф контракта был изменен"; 
			}else if (lang.equals("by")) { 
			message = "Тарыф кантракта быў зменены"; 
			} }
		else{if (lang == null || lang.equals("en")) { 
				message = "Cannot change tariff of contract"; 
				} else if (lang.equals("ru")) { 
				message = "Не удалось изменить тариф контракта"; 
				}else if (lang.equals("by")) { 
				message = "Не ўдалося змянiць тарыф кантракта"; 
				} }
	request.getSession().setAttribute("message", message);
	return action + url;
}
}