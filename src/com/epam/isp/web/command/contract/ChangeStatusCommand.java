package com.epam.isp.web.command.contract;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

import com.epam.isp.core.contract.Contract;
import com.epam.isp.core.contract.Status;
import com.epam.isp.core.contract.ContractManager;
import com.epam.isp.core.contract.ContractManagerImpl;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.utils.validation.Validation;
/**
 * Invokes when user want to change status of contract.
 * 
 * @author Iliya Rudberg.
 *
 */
public class ChangeStatusCommand extends Command {

	private static final long serialVersionUID = 3315917092461024179L;

	private static final Logger logger = Logger.getLogger(ChangeStatusCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");

		String result = null;
		if (ActionType.POST == actionType) {
			result = doPost(request, response);
		}

		logger.debug("Finished executing Command");

		return result;
	}

	/**
	 * Redirects to view all contracts after submitting change contract status form.
	 *
	 * @return path to the view of changed contract if fields properly filled,
	 *         otherwise redisplays change contract page.
	 * @throws IOException
	 * @throws ServletException
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String lang = (String)request.getSession().getAttribute("language");
		String message = null;
		boolean complete= false;
		// action
				final String action = "controller?";
				String url = request.getParameter("url");
		// validation
		boolean valid = false;
		if (Validation.isInt(request.getParameter("contractID"))&& 
			Validation.contains(request.getParameter("contractStatus").toUpperCase(), Status.class))
		{valid = true;}else {valid = false;}
		logger.trace("Validation: " + valid);
		if (valid) {
		Status status  = Status.valueOf(request.getParameter("contractStatus").toUpperCase());
		int  contractId = Integer.parseInt(request.getParameter("contractID"));
		logger.trace("The fields got: "  + contractId + " " + status.toString() + " " );
		// create contract entity
		Contract contract = new Contract(contractId, status);
		logger.trace("Contract was created: " + contract);
		// change status of contract at database
			ContractManager manager = new ContractManagerImpl();
			complete = manager.changeStatusOfContract(contract);
		logger.trace("Status of contract was changed");
		response.reset();
		}
		if (complete == true){
			if (lang == null || lang.equals("en")) { 
				message = "The status of contract was changed"; 
				} else if (lang.equals("ru")) { 
				message = "Статус контракта был изменен"; 
				}else if (lang.equals("by")) { 
				message = "Статус кантракта быў зменены"; 
				} }
			else{if (lang == null || lang.equals("en")) { 
					message = "Cannot change status of contract"; 
					} else if (lang.equals("ru")) { 
					message = "Не удалось изменить статус контракта"; 
					}else if (lang.equals("by")) { 
					message = "Не ўдалося змянiць статус кантракта"; 
					} }
		request.getSession().setAttribute("message", message);
		return action + url;
	}

}
