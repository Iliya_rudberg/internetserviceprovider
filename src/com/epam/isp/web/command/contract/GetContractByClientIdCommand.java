package com.epam.isp.web.command.contract;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.epam.isp.Path;
import com.epam.isp.core.client.Client;
import com.epam.isp.core.contract.Contract;
import com.epam.isp.core.contract.ContractManager;
import com.epam.isp.core.contract.ContractManagerImpl;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
/**
 * Invokes when user want to get contract by client id.
 * 
 * @author Iliya Rudberg.
 *
 */
public class GetContractByClientIdCommand extends Command {
	private static final long serialVersionUID = -405445733126752744L;

	private static final Logger logger = Logger.getLogger(GetContractByClientIdCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");
		String result = null;

		if (ActionType.GET == actionType) {
			result = doGet(request, response);
		}

		logger.debug("Command finished");
		return result;
	}

	/**
	 * Forward to view an account state.
	 * 
	 * @return path to account state page.
	 */
	private String doGet(HttpServletRequest request, HttpServletResponse response) {

		try {
			processRequest(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			logger.trace("Servlet exception");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.trace("IOException");
		}

		return Path.FORWARD_VIEW_ACCOUNT_STATE;
	}
	public static void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get client entity
		Client client = (Client) request.getSession().getAttribute("entity");
		int clientId = client.getId();
		logger.trace("Client user id: " + clientId);

		// find contract by client id
		ContractManager manager = new ContractManagerImpl();
		Contract contract = manager.getContractByClientId(clientId);
		logger.trace("Client were found by user id: " + client);
		request.getSession().setAttribute("account", contract);
	}
}
