package com.epam.isp.web.command.contract;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.epam.isp.Path;
import com.epam.isp.core.contract.ContractManager;
import com.epam.isp.core.contract.ContractManagerImpl;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.utils.validation.Validation;
/**
 * Invokes when user want to delete contract.
 * 
 * @author Iliya Rudberg.
 *
 */
public class DeletContractCommand extends Command {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(DeletContractCommand.class);
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");

		String result = null;
		if (ActionType.POST == actionType) {
			result = doPost(request, response);
		}

		logger.debug("Finished executing Command");

		return result;
	}
	/**
	 * Redirects to view all contracts after submitting table form.
	 *
	 * @return path to the view of all contract if fields properly filled, exception if error was occurred.
	 * @throws IOException
	 * @throws ServletException
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String lang = (String)request.getSession().getAttribute("language");
		String message = null;
		boolean complete= false;
		ContractManager manager = new ContractManagerImpl();
		// validation
		Validation.isInt(request.getParameter("contractID"));
		int contractID = Integer.parseInt(request.getParameter("contractID"));
		complete = manager.deleteContractByID(contractID);
		logger.trace("Contract id: " + contractID +" was deleted.");
		if (complete == true){
			if (lang == null || lang.equals("en")) { 
				message = "Contract was deleted"; 
				} else if (lang.equals("ru")) { 
				message = "Контракт был удален"; 
				}else if (lang.equals("by")) { 
				message = "Кантракт быў выдалены"; 
				} }
			else{if (lang == null || lang.equals("en")) { 
					message = "Cannot delete a contract"; 
					} else if (lang.equals("ru")) { 
					message = "Не удалось удалить контракт"; 
					}else if (lang.equals("by")) { 
					message = "Не ўдалося выдалиць кантракт"; 
					} }
		request.getSession().setAttribute("message", message);
		return Path.REDIRECT_TO_VIEW_ALL_CONTRACTS;
	}

}
