package com.epam.isp.web.command.contract;

import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.epam.isp.Path;
import com.epam.isp.core.contract.Contract;
import com.epam.isp.core.contract.Status;
import com.epam.isp.core.contract.ContractManager;
import com.epam.isp.core.contract.ContractManagerImpl;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.utils.validation.ContractInputValidator;
/**
 * Invokes when user want to update contract.
 * 
 * @author Iliya Rudberg.
 *
 */
public class UpdateContractCommand extends Command{
	private static final long serialVersionUID = 3315917092461024179L;

	private static final Logger logger = Logger.getLogger(UpdateContractCommand.class);
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		logger.debug("Start executing Command");

		String result = null;
		if (ActionType.POST == actionType) {
			result = doPost(request, response);
		}

		logger.debug("Finished executing Command");

		return result;
	}
	/**
	 * Redirects to view all contracts after submitting update contract form.
	 *
	 * @return path to the view of updated contract if fields properly filled,
	 *         otherwise redisplays update contract page.
	 * @throws IOException
	 * @throws ServletException
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String lang = (String)request.getSession().getAttribute("language");
		String message = null;
		boolean complete= false;
		double balance = 0.0;
		// validation
		boolean valid = ContractInputValidator.validateContractParametrs(request.getParameter("contractBalance"),
				   request.getParameter("contractClientID"),
				   request.getParameter("contractStatus"),
				   request.getParameter("contractTariffId"),
				   request.getParameter("date"));
	
		if (valid) {
			String[] formatStrings = {"MM/dd/yyyy", "yyyy-MM-dd"};
			int id = Integer.parseInt(request.getParameter("id"));
			String availableDate = request.getParameter("date");
			DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
			balance = Double.parseDouble(request.getParameter("contractBalance"));
			Status status = Status.valueOf(request.getParameter("contractStatus").toUpperCase());
			int  clientId = Integer.parseInt(request.getParameter("contractClientID"));
			int  tariffId = Integer.parseInt(request.getParameter("contractTariffId"));
			int  employeeId = Integer.parseInt(request.getParameter("contractEmployeeId"));
			Date sqlDate = null;
			java.util.Date date =  null;
			for (String formatString : formatStrings)
		    {
		        try
		        {
		        	 date = new SimpleDateFormat(formatString).parse(availableDate);
		        }
		        catch (ParseException e) {}
		    }
				sqlDate = Date.valueOf(outputFormat.format(date));
			logger.trace("The fields got: "  + balance + " " + status.toString() + " " + clientId +   " "
					 + " " + balance + " " + employeeId + " "+ tariffId);
		// create contract entity
			Contract contract = new Contract(id, balance, employeeId, clientId, sqlDate,status,tariffId);
		logger.trace("Contract was created: " + contract);
		// update contract at database
			ContractManager manager = new ContractManagerImpl();
			complete = manager.updateContractById(contract);
		logger.trace("Contract was updated at database");
		response.reset();
		}
		if (complete == true){
			if (lang == null || lang.equals("en")) { 
				message = "Contract was updated"; 
				} else if (lang.equals("ru")) { 
				message = "Контракт был обновлен"; 
				}else if (lang.equals("by")) { 
				message = "Кантракт быў абноўлен"; 
				} }
			else{if (lang == null || lang.equals("en")) { 
					message = "Cannot update a contract"; 
					} else if (lang.equals("ru")) { 
					message = "Не удалось обновить контракт"; 
					}else if (lang.equals("by")) { 
					message = "Не ўдалося абнавіць кантракт"; 
					} }
		request.getSession().setAttribute("message", message);
		return Path.REDIRECT_TO_VIEW_ALL_CONTRACTS;
	}

}
