package com.epam.isp.web.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.isp.Path;
import com.epam.isp.web.ActionType;

/**
 * Invoked when command was not found for client request.
 *
 * @author Iliya Rudberg
 *
 */
public class NoCommand extends Command {
	private static final long serialVersionUID = -2785976616686657267L;

	private static final Logger logger = Logger.getLogger(NoCommand.class);

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Command execution starts");

		String errorMessage = "No such command";
		request.setAttribute("errorMessage", errorMessage);
		logger.error("Set the request attribute: 'errorMessage' = " + errorMessage);

		logger.debug("Command execution finished");
		return Path.ERROR_PAGE;
	}

}