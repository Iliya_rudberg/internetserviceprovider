package com.epam.isp.web.command.task; 
 
import java.io.IOException; 
import javax.servlet.ServletException; 
import javax.servlet.http.HttpServletRequest; 
import javax.servlet.http.HttpServletResponse; 
import org.apache.log4j.Logger; 
import com.epam.isp.core.tasks.Task;
import com.epam.isp.core.tasks.TaskManager;
import com.epam.isp.core.tasks.TaskManagerImpl;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.utils.validation.Validation; 
 
 
/** 
 * Invoked when user want to send message. 
 * 
 * @author Iliya Rudberg. 
 * 
 */ 
public class AddTaskCommand extends Command { 
private static final long serialVersionUID = -3071536593627692473L; 
 
private static final Logger logger = Logger.getLogger(AddTaskCommand.class); 
 
@Override 
public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType) 
throws IOException, ServletException { 
logger.debug("Start executing Command"); 
 
String result = null; 
 
if (actionType == ActionType.POST) { 
result = doPost(request, response); 
} else { 
result = null; 
} 
 
logger.debug("End executing command"); 
return result; 
} 
 
/** 
 * Send contact request to the admin page. 
 * 
 * @return path to the page. 
 */ 
private String doPost(HttpServletRequest request, HttpServletResponse response) { 
	final String action = "controller?";
	final String url = request.getParameter("url");
	boolean complete = false;
//validation 
	boolean valid = Validation.isFilled(request.getParameter("name"), 
	request.getParameter("mobile"), 
	request.getParameter("email"), 
	request.getParameter("message")); 
	if(valid){ 
		String name = request.getParameter("name");
		int phone = Integer.parseInt(request.getParameter("mobile"));
		String email = request.getParameter("email");
		String message = request.getParameter("message");
		Task task = new Task(name,phone,email,message);
		TaskManager manager = new TaskManagerImpl();
		complete = manager.addTask(task);
	} 
	String lang = (String) request.getSession().getAttribute("language"); 
	String message = ""; 
	if (complete == true){
	if (lang == null || lang.equals("en")) { 
		message = "Your message has succefully sended"; 
		} else if (lang.equals("ru")) { 
		message = "Ваше сообщение успешно отправлено"; 
		}else if (lang.equals("by")) { 
		message = "Ваша паведамленне паспяхова дасланы"; 
		} }
	else{if (lang == null || lang.equals("en")) { 
			message = "Cannot send a message"; 
			} else if (lang.equals("ru")) { 
			message = "Ваше сообщение не отправлено"; 
			}else if (lang.equals("by")) { 
			message = "Ваша паведамленне не даслано"; 
			} }
			
		request.getSession().setAttribute("message", message); 
		return action + url; 
	} 
	 
}