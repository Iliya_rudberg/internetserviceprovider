package com.epam.isp.web.command.task;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.epam.isp.Path;
import com.epam.isp.core.tasks.TaskManager;
import com.epam.isp.core.tasks.TaskManagerImpl;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.utils.validation.Validation;
/**
 * Complete task command.
 * 
 * @author Iliya Rudberg
 *
 */
public class CompleteTaskCommand extends Command{
	private static final long serialVersionUID = 3315917092461024179L;

	private static final Logger logger = Logger.getLogger(CompleteTaskCommand.class);
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		logger.debug("Start executing Command");

		String result = null;
		if (ActionType.POST == actionType) {
			result = doPost(request, response);
		}

		logger.debug("Finished executing Command");

		return result;
	}
	/**
	 * Complete a created task.
	 *
	 * @return path to the view all tasks.
	 * @throws IOException
	 * @throws ServletException
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		boolean complete = false;
		boolean valid = Validation.isFilled(request.getParameterValues("taskID"));
	
		if(valid){
			int taskID = Integer.parseInt(request.getParameter("taskID"));
			TaskManager manager = new TaskManagerImpl();
			complete = manager.completeTask(taskID);
		}
		String lang = (String)request.getSession().getAttribute("language");
		String message = null;
		if (complete == true){
			if (lang == null || lang.equals("en")) { 
				message = "Task has succefull completed"; 
				} else if (lang.equals("ru")) { 
				message = "Задание было успешно заврешено"; 
				}else if (lang.equals("by")) { 
				message = "Заданне было паспяхова заврешено"; 
				} }
			else{if (lang == null || lang.equals("en")) { 
					message = "Cannot complet the task"; 
					} else if (lang.equals("ru")) { 
					message = "Не удалось завершить задание"; 
					}else if (lang.equals("by")) { 
					message = "Не ўдалося завяршыць заданне"; 
					} }
					
				request.getSession().setAttribute("message", message); 
		return Path.REDIRECT_TO_GET_TASKS;
	}

}
