package com.epam.isp.web.command.task;

import java.io.IOException;
import java.util.Collection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.epam.isp.core.tasks.Task;
import com.epam.isp.core.tasks.TaskManager;
import com.epam.isp.core.tasks.TaskManagerImpl;
import com.epam.isp.Path;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
/**
 * Get all tasks command.
 * 
 * @author Iliya Rudberg
 *
 */
public class ListTaskCommand  extends Command {
	private static final long serialVersionUID = 7723206619340362128L;

	private static final Logger logger = Logger.getLogger(ListTaskCommand .class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");

		String result = null;

		if (ActionType.GET == actionType) {
			result = doGet(request, response);
		}

		logger.debug("Finished executing Command");
		return result;
	}

	/**
	 * Forward user to page of all tariff.
	 * @return path to view of all tariff page.
	 */
	private String doGet(HttpServletRequest request, HttpServletResponse response) {
		TaskManager manager = new TaskManagerImpl();
		Collection<Task> task = manager.listTasks();
		logger.trace("Task found: " + task);
		request.setAttribute("task", task);
	    return Path.FORWARD_VIEW_ALL_TASKS;
	}
}
