package com.epam.isp.web.command.entity.user;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.isp.core.user.User;
import com.epam.isp.core.user.UserManager;
import com.epam.isp.core.user.UserManagerImpl;
import com.epam.isp.Path;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;

/**
 * List of users command.
 * 
 * @author Iliya Rudberg
 *
 */
public class ListUsersCommand extends Command {
	private static final long serialVersionUID = 7723206619340362128L;

	private static final Logger logger = Logger.getLogger(ListUsersCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");

		String result = null;

		if (ActionType.GET == actionType) {
			result = doGet(request, response);
		}

		logger.debug("Finished executing Command");
		return result;
	}

	/**
	 * Forward user to page of all users.
	 *
	 * @return path to view of all users page.
	 */
	private String doGet(HttpServletRequest request, HttpServletResponse response) {
		UserManager manager = new UserManagerImpl();

		Collection<User> users = manager.getUsers();
		logger.trace("Users found: " + users);
		
		request.setAttribute("users", users);

		return Path.FORWARD_VIEW_ALL_USERS;
	}

}
