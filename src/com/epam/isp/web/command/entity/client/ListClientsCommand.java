package com.epam.isp.web.command.entity.client;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.isp.Path;
import com.epam.isp.core.client.Client;
import com.epam.isp.core.client.ClientManager;
import com.epam.isp.core.client.ClientManagerImpl;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
/**
 * Invokes when user want to get a list of clients.
 * 
 * @author Iliya Rudberg.
 *
 */
public class ListClientsCommand extends Command {
	private static final long serialVersionUID = 7723206619340362128L;

	private static final Logger logger = Logger.getLogger(ListClientsCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");

		String result = null;

		if (ActionType.GET == actionType) {
			result = doGet(request, response);
		}

		logger.debug("Finished executing Command");
		return result;
	}

	/**
	 * Forward user to page of all clients.
	 *
	 * @return path to view of all clients page.
	 */
	private String doGet(HttpServletRequest request, HttpServletResponse response) {
		ClientManager manager = new ClientManagerImpl();

		Collection<Client> client = manager.getClients();
		logger.trace("Client found: " + client);


		request.setAttribute("client", client);

		return Path.FORWARD_VIEW_ALL_CLIENTS;
	}

}
