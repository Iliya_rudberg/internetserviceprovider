package com.epam.isp.web.command.entity.client;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.epam.isp.core.client.Client;
import com.epam.isp.core.client.ClientManager;
import com.epam.isp.core.client.ClientManagerImpl;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.utils.validation.Validation;
/**
 * Invokes when user want to update client.
 * 
 * @author Iliya Rudberg.
 *
 */
public class UpdateClientCommand extends Command{
	private static final long serialVersionUID = 3315917092461024179L;

	private static final Logger logger = Logger.getLogger(UpdateClientCommand.class);
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		logger.debug("Start executing Command");

		String result = null;
		if (ActionType.POST == actionType) {
			result = doPost(request, response);
		}

		logger.debug("Finished executing Command");

		return result;
	}
	/**
	 * Redirects to view all users after submitting update client form.
	 *
	 * @return path to the view of updated client if fields properly filled,
	 *         otherwise display exception in client page.
	 * @throws IOException
	 * @throws ServletException
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		boolean complete = false;
		String lang = (String)request.getSession().getAttribute("language");
		String message = null;
		final String action = "controller?";
		final String url = request.getParameter("url");
		// validation
		boolean valid = Validation.isFilled(request.getParameter("name"),
																	   request.getParameter("surname"),
																	   request.getParameter("address"),
																	   request.getParameter("phone"));
					
		
		logger.trace("Validation: " + valid);
		
		if (valid) {
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String address = request.getParameter("address");
		int phone = Integer.parseInt(request.getParameter("phone"));
		String passport = request.getParameter("passport");
		String email = request.getParameter("email");
		logger.trace("The fields got: "+ id+ " " + name + " " + surname + " " + address + " " + phone + " " + passport+" "+ email);
		// create client entity
		Client client = new Client(id, name, surname, passport, address, phone, email);
		logger.trace("Client was created: " + client);
		// update client at database
			ClientManager manager = new ClientManagerImpl();
			complete = manager.updateClientById(client);
		logger.trace("Client was updated at database");
		response.reset();
		}
		if (complete == true){
			if (lang == null || lang.equals("en")) { 
				message = "Client was updated"; 
				} else if (lang.equals("ru")) { 
				message = "Клиент был обновлен"; 
				}else if (lang.equals("by")) { 
				message = "Кліент быў абноўлен"; 
				} }
			else{if (lang == null || lang.equals("en")) { 
					message = "Cannot update a client"; 
					} else if (lang.equals("ru")) { 
					message = "Не удалось обновить клиента"; 
					}else if (lang.equals("by")) { 
					message = "Не ўдалося абнавіць клiента"; 
					} }
		request.getSession().setAttribute("message", message);
		return action + url;
	}

}
