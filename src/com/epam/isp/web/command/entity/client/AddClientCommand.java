package com.epam.isp.web.command.entity.client;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.isp.LoginDuplicateException;
import com.epam.isp.Path;
import com.epam.isp.core.client.Client;
import com.epam.isp.core.client.ClientManager;
import com.epam.isp.core.client.ClientManagerImpl;
import com.epam.isp.core.user.User;
import com.epam.isp.core.user.UserManager;
import com.epam.isp.core.user.UserManagerImpl;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.utils.MailUtils;
import com.epam.isp.web.utils.Utils;
import com.epam.isp.web.utils.validation.UserInputValidator;
import com.epam.isp.web.utils.validation.Validation;
/**
 * Invokes when user want to add client.
 * 
 * @author Iliya Rudberg.
 *
 */
public class AddClientCommand extends Command {

	private static final long serialVersionUID = 3315917092461024179L;

	private static final Logger logger = Logger.getLogger(AddClientCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");

		String result = null;
		if (ActionType.POST == actionType) {
			result = doPost(request, response);
		}

		logger.debug("Finished executing Command");

		return result;
	}

	/**
	 * Redirects to view all clients after submitting add client form.
	 *
	 * @return path to the view of added client if fields properly filled,
	 *         otherwise redisplays add client page.
	 * @throws IOException
	 * @throws ServletException
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		boolean complete =false;
		String login =  null;
		String email = null;
		// validation
		boolean valid = Validation.isFilled(request.getParameter("username"),
																	   request.getParameter("name"),
																	   request.getParameter("surname"),
																	   request.getParameter("address"),
																	   request.getParameter("phone"));
		if (valid){
			valid = UserInputValidator.validateUserParametrs(request.getParameter("username"),
														 request.getParameter("password"),
														 request.getParameter("email"));
		}
		logger.trace("Validation: " + valid);
		UserManager userManager = new UserManagerImpl();
		if (valid) {
		login = request.getParameter("username");
		String password = Utils.generateHash(request.getParameter("password"));
		User user = new User (login, password, 2);
		try {
			userManager.addUser(user);
		} catch (LoginDuplicateException e) {
			// TODO Auto-generated catch block
			valid = false;
			logger.trace("Login" + login + " is allready exist in database");
		}
		}
		User user = userManager.getUserByLogin(login);
		if (valid && user != null){
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String address = request.getParameter("address");
		int  phone = Integer.parseInt(request.getParameter("phone"));
		String passport = request.getParameter("passport");
		 email = request.getParameter("email");
		int usersId = user.getId();
		logger.trace("The fields got: " + name + " " + surname + " " + address + " " + phone + " "
				 + " " + passport + " " + email  );
		// create client entity
		Client client = new Client( name, surname, passport,address,phone,email,usersId);
		logger.trace("Client was created: " + client);
		// add client to database
			ClientManager manager = new ClientManagerImpl();
			complete = manager.addClient(client);
		logger.trace("Client was added to database");
		response.reset();
		}
		
		String lang = (String)request.getSession().getAttribute("language");
		String message = null;
		if (complete == true){
			if (lang == null || lang.equals("en")) { 
				message = "Client was successfully added to database"; 
				} else if (lang.equals("ru")) { 
				message = "Клиент был успешно добавлен в базу данных"; 
				}else if (lang.equals("by")) { 
				message = "Кліент быў паспяхова дададзены ў базу дадзеных"; 
				} }
			else{if (lang == null || lang.equals("en")) { 
					message = "Cannot add a client"; 
					} else if (lang.equals("ru")) { 
					message = "Не удалось добавить клиента"; 
					}else if (lang.equals("by")) { 
					message = "Не ўдалося дабавiць  клiента"; 
					} }
		request.getSession().setAttribute("message", message);
		// Send email
		if (!email.equals("")) {
			if (UserInputValidator.validateEmail(email)) {
				
				MailUtils.sendConfirmationEmail(new User(login, request.getParameter("password")), email);
				logger.trace("Email was sent");
			} else {
				logger.error("Wrong email");
				return Path.REDIRECT_TO_VIEW_ALL_CLIENTS + "&error=wrongEmail";
			}
		}
		return Path.REDIRECT_TO_VIEW_ALL_CLIENTS;
	}

}
