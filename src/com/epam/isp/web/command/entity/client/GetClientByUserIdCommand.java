package com.epam.isp.web.command.entity.client;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.isp.Path;
import com.epam.isp.core.client.Client;
import com.epam.isp.core.client.ClientManager;
import com.epam.isp.core.client.ClientManagerImpl;
import com.epam.isp.core.user.User;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
/**
 * Invokes when user want to get client by users id.
 * 
 * @author Iliya Rudberg.
 *
 */
public class GetClientByUserIdCommand extends Command {
	private static final long serialVersionUID = -405445733126752744L;

	private static final Logger LOG = Logger.getLogger(GetClientByUserIdCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		LOG.debug("Start executing Command");
		String result = null;

		if (ActionType.GET == actionType) {
			result = doGet(request, response);
		}

		LOG.debug("Command finished");
		return result;
	}

	/**
	 * Forward to admin page.
	 * 
	 * @return path to admin page.
	 */
	private String doGet(HttpServletRequest request, HttpServletResponse response) {

		// get user id
		User user = (User) request.getSession().getAttribute("user");
		int userId = user.getId();
		LOG.trace("Client user id: " + userId);

		// find client by user id
		ClientManager manager = new ClientManagerImpl();
		Client client = manager.getClientByUserId(userId);
		
		LOG.trace("Client were found by user id: " + client);
		request.getSession().setAttribute("entity", client);
			return Path.FORWARD_VIEW_ADMIN_PAGE;
		}
	}

