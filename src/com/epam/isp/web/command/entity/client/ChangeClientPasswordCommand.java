package com.epam.isp.web.command.entity.client;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import com.epam.isp.core.client.ClientManager;
import com.epam.isp.core.client.ClientManagerImpl;
import com.epam.isp.core.user.User;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.utils.Utils;
import com.epam.isp.web.utils.validation.Validation;
/**
 * Invokes when user want to change client password.
 * 
 * @author Iliya Rudberg.
 *
 */
public class ChangeClientPasswordCommand extends Command{
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(ChangeClientPasswordCommand.class);
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");

		String result = null;
		if (ActionType.POST == actionType) {
			result = doPost(request, response);
		}

		logger.debug("Finished executing Command");

		return result;
	}
	/**
	 * Redirects to a page, from which password was changed,after submitting change password form.
	 *
	 * @return path to a page, from which password was changed, if fields properly filled,
	 *         otherwise redisplays an exception.
	 * @throws IOException
	 * @throws ServletException
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		boolean complete = false;
		String lang = (String)request.getSession().getAttribute("language");
		String message = null;
		final String action = "controller?";
		ClientManager manager = new ClientManagerImpl();
		int clientID = 0;
		String oldPassword = null;
		String password = null;
		// get parameters
		String url = request.getParameter("url");
		User user = (User)		request.getSession().getAttribute("user");
		// generate password
		password = request.getParameter("password");
		password = Utils.generateHash(password);
		oldPassword = request.getParameter("oldPassword");
		oldPassword = Utils.generateHash(oldPassword);
		//check roles & password and get client id
		
		if(user.getRoleId() == 2){
			if(oldPassword.equals(user.getPassword())){
			clientID = manager.getClientByUserId(user.getId()).getId();
			}
		}else{
			if (Validation.isInt(request.getParameter("contractClientID"))){
				clientID = Integer.parseInt(request.getParameter("contractClientID"));}
		}
		// change password of client
					if (clientID != 0){
				    complete = manager.changePassword(clientID, password);
					logger.trace("Password for client id: " + clientID +" was changed.");
					}	
		if (complete == true){
			if (lang == null || lang.equals("en")) { 
				message = "Client's password was changed"; 
				} else if (lang.equals("ru")) { 
				message = "Пароль клиента был изменен"; 
				}else if (lang.equals("by")) { 
				message = "Пароль кліента быў зменены"; 
				} }
			else{if (lang == null || lang.equals("en")) { 
					message = "Cannot change password of client"; 
					} else if (lang.equals("ru")) { 
					message = "Не удалось изменить пароль клиента"; 
					}else if (lang.equals("by")) { 
					message = "Не ўдалося змянiць пароль клiента"; 
					} }
		request.getSession().setAttribute("message", message);
		return action+url;
	}

}
