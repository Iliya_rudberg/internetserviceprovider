package com.epam.isp.web.command.entity.client;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.epam.isp.Path;
import com.epam.isp.core.client.ClientManager;
import com.epam.isp.core.client.ClientManagerImpl;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.utils.validation.Validation;
/**
 * Invokes when user want to delete client.
 * 
 * @author Iliya Rudberg.
 *
 */
public class DeleteClientCommand extends Command{
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(DeleteClientCommand.class);
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");

		String result = null;
		if (ActionType.POST == actionType) {
			result = doPost(request, response);
		}

		logger.debug("Finished executing Command");

		return result;
	}
	/**
	 * Redirects to view all clients, after submitting delete  form.
	 *
	 * @return path to the view of all clients if fields properly filled,
	 *         otherwise display an exception.
	 * @throws IOException
	 * @throws ServletException
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		boolean complete = false;
		String lang = (String)request.getSession().getAttribute("language");
		String message = null;
		int clientID;
		ClientManager manager = new ClientManagerImpl();
		// get current  client id from page
		if (Validation.isInt(request.getParameter("clientID"))){
		clientID = Integer.parseInt(request.getParameter("clientID"));
	   complete = manager.deleteClientByID(clientID);
		logger.trace("Client id: " + clientID +" was deleted.");
		}
		if (complete == true){
			if (lang == null || lang.equals("en")) { 
				message = "Client was deleted"; 
				} else if (lang.equals("ru")) { 
				message = "Клиент был удален"; 
				}else if (lang.equals("by")) { 
				message = "Кліент быў выдалены"; 
				} }
			else{if (lang == null || lang.equals("en")) { 
					message = "Cannot delete a client"; 
					} else if (lang.equals("ru")) { 
					message = "Не удалось удалить клиента"; 
					}else if (lang.equals("by")) { 
					message = "Не ўдалося выдалиць клiента"; 
					} }
		request.getSession().setAttribute("message", message);
		return Path.REDIRECT_TO_VIEW_ALL_CLIENTS;
	}

}
