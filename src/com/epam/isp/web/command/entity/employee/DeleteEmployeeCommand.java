package com.epam.isp.web.command.entity.employee;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.epam.isp.Path;
import com.epam.isp.core.employee.EmployeeManager;
import com.epam.isp.core.employee.EmployeeManagerImpl;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.utils.validation.Validation;
/**
 * Invokes when user want to delete employee.
 * 
 * @author Iliya Rudberg.
 *
 */
public class DeleteEmployeeCommand extends Command{
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(DeleteEmployeeCommand.class);
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");

		String result = null;
		if (ActionType.POST == actionType) {
			result = doPost(request, response);
		}

		logger.debug("Finished executing Command");

		return result;
	}
	/**
	 * Redirects to view all employees after submitting delete employee form.
	 *
	 * @return path to the view of all employees if fields properly filled,
	 *         otherwise display exception in employee page.
	 * @throws IOException
	 * @throws ServletException
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String lang = (String)request.getSession().getAttribute("language");
		String message = null;
		boolean complete= false;
		int employeeID;
		EmployeeManager manager = new EmployeeManagerImpl();
		// get current  employee id from page
		if (Validation.isInt(request.getParameter("employeeID"))){
		employeeID = Integer.parseInt(request.getParameter("employeeID"));
	    complete = manager.deleteEmployeeByID(employeeID);
		logger.trace("Employee id: " + employeeID +" was deleted.");
		}
		if (complete == true){
			if (lang == null || lang.equals("en")) { 
				message = "Employee was deleted"; 
				} else if (lang.equals("ru")) { 
				message = "Сотрудник был удален"; 
				}else if (lang.equals("by")) { 
				message = "Супрацоўнік быў выдалены"; 
				} }
			else{if (lang == null || lang.equals("en")) { 
					message = "Cannot delete a employee"; 
					} else if (lang.equals("ru")) { 
					message = "Не удалось удалить сотрудника"; 
					}else if (lang.equals("by")) { 
					message = "Не ўдалося выдалиць супрацоўніка"; 
					} }
		request.getSession().setAttribute("message", message);
		return Path.REDIRECT_TO_VIEW_ALL_EMPLOYEES;
	}

}
