package com.epam.isp.web.command.entity.employee;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.isp.Path;
import com.epam.isp.core.employee.Employee;
import com.epam.isp.core.employee.EmployeeManager;
import com.epam.isp.core.employee.EmployeeManagerImpl;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
/**
 * Invokes when user want to get employees list.
 * 
 * @author Iliya Rudberg.
 *
 */
public class ListEmployeesCommand extends Command {
	private static final long serialVersionUID = 7723206619340362128L;

	private static final Logger logger = Logger.getLogger(ListEmployeesCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");

		String result = null;

		if (ActionType.GET == actionType) {
			result = doGet(request, response);
		}

		logger.debug("Finished executing Command");
		return result;
	}

	/**
	 * Forward user to page of all employee.
	 *
	 * @return path to view of all employee page.
	 */
	private String doGet(HttpServletRequest request, HttpServletResponse response) {
		EmployeeManager manager = new EmployeeManagerImpl();

		Collection<Employee> employee = manager.getEmployee();
		logger.trace("Employee found: " + employee);


		request.setAttribute("employee", employee);

		return Path.FORWARD_VIEW_ALL_EMPLOYEES;
	}

}
