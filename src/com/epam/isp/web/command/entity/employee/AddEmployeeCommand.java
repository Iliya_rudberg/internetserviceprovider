package com.epam.isp.web.command.entity.employee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.isp.LoginDuplicateException;
import com.epam.isp.Path;
import com.epam.isp.core.employee.Employee;
import com.epam.isp.core.employee.EmployeeManager;
import com.epam.isp.core.employee.EmployeeManagerImpl;
import com.epam.isp.core.user.User;
import com.epam.isp.core.user.UserManager;
import com.epam.isp.core.user.UserManagerImpl;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.utils.MailUtils;
import com.epam.isp.web.utils.Utils;
import com.epam.isp.web.utils.validation.UserInputValidator;
import com.epam.isp.web.utils.validation.Validation;
/**
 * Invokes when user want to add employee.
 * 
 * @author Iliya Rudberg.
 *
 */
public class AddEmployeeCommand extends Command {

	private static final long serialVersionUID = 3315917092461024179L;

	private static final Logger logger = Logger.getLogger(AddEmployeeCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");

		String result = null;
		if (ActionType.POST == actionType) {
			result = doPost(request, response);
		}

		logger.debug("Finished executing Command");

		return result;
	}

	/**
	 * Redirects to view all employee after submitting add employee form.
	 *
	 * @return path to the view of added employee if fields properly filled,
	 *         otherwise redisplays add employee page.
	 * @throws IOException
	 * @throws ServletException
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String login =  null;
		String email = null;
		String lang = (String)request.getSession().getAttribute("language");
		String message = null;
		boolean complete= false;
		// validation
		boolean valid = Validation.isFilled(request.getParameter("username"),
																	   request.getParameter("name"),
																	   request.getParameter("surname"),
																	   request.getParameter("address"),
																	   request.getParameter("phone"));
		if (valid){
			valid = UserInputValidator.validateUserParametrs(request.getParameter("username"),
														 request.getParameter("password"),
														 request.getParameter("email"));
		}
		logger.trace("Validation: " + valid);
		UserManager userManager = new UserManagerImpl();
		if (valid) {
		login = request.getParameter("username");
		String password = Utils.generateHash(request.getParameter("password"));
		User user = new User (login, password, 1);
		try {
			userManager.addUser(user);
		} catch (LoginDuplicateException e) {
			// TODO Auto-generated catch block
			valid = false;
			logger.trace("Login" + login + " is allready exist in database");
		}
		}
		User user = userManager.getUserByLogin(login);
		if (valid && user != null){
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String address = request.getParameter("address");
		int  phone = Integer.parseInt(request.getParameter("phone"));
		String position = request.getParameter("position");
		email = request.getParameter("email");
		int usersId = user.getId();
		logger.trace("The fields got: " + name + " " + surname + " " + address + " " + phone + " "
				 + " " + position + " " + email  );
		// create employee entity
		Employee employee = new Employee( name, surname, address,phone,position,email,usersId);
		logger.trace("Employee was created: " + employee);
		// add employee to database
			EmployeeManager manager = new EmployeeManagerImpl();
			complete = manager.addEmployee(employee);
		logger.trace("Employee was added to database");
		response.reset();
		}
		if (complete == true){
			if (lang == null || lang.equals("en")) { 
				message = "Employee was successfully added to database"; 
				} else if (lang.equals("ru")) { 
				message = "Сотрудник был успешно добавлен в базу данных"; 
				}else if (lang.equals("by")) { 
				message = "Супрацоўнік быў паспяхова дададзены ў базу дадзеных"; 
				} }
			else{if (lang == null || lang.equals("en")) { 
					message = "Cannot add a employee"; 
					} else if (lang.equals("ru")) { 
					message = "Не удалось добавить сотрудника"; 
					}else if (lang.equals("by")) { 
					message = "Не ўдалося дабавiць  супрацоўніка"; 
					} }
		request.getSession().setAttribute("message", message);
		// Send email
				if (!email.equals("")) {
					if (UserInputValidator.validateEmail(email)) {
						MailUtils.sendConfirmationEmail(new User(login, request.getParameter("password")), email);
						logger.trace("Email was sent");
					} else {
						logger.error("Wrong email");
						return Path.REDIRECT_TO_VIEW_ALL_EMPLOYEES + "&error=wrongEmail";
					}
				}
		return Path.REDIRECT_TO_VIEW_ALL_EMPLOYEES;
	}

}
