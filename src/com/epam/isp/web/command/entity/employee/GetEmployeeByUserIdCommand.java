package com.epam.isp.web.command.entity.employee;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.epam.isp.Path;
import com.epam.isp.core.employee.Employee;
import com.epam.isp.core.employee.EmployeeManager;
import com.epam.isp.core.employee.EmployeeManagerImpl;
import com.epam.isp.core.user.User;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
/**
 * Invokes when user want to get employee by user id.
 * 
 * @author Iliya Rudberg.
 *
 */
public class GetEmployeeByUserIdCommand extends Command {
	private static final long serialVersionUID = -405445733126752744L;

	private static final Logger LOG = Logger.getLogger(GetEmployeeByUserIdCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		LOG.debug("Start executing Command");
		String result = null;

		if (ActionType.GET == actionType) {
			result = doGet(request, response);
		}

		LOG.debug("Command finished");
		return result;
	}

	/**
	 * Forward to view admin page.
	 * 
	 * @return path to admin page.
	 */
	private String doGet(HttpServletRequest request, HttpServletResponse response) {

		// get user id
		User user = (User) request.getSession().getAttribute("user");
		int userId = user.getId();
		LOG.trace("Employee user id: " + userId);

		// find employee by user id
		EmployeeManager manager = new EmployeeManagerImpl();
		Employee employee = manager.getEmployeeByUserId(userId);
		
		LOG.trace("Employee were found by user id: " + employee);
		request.getSession().setAttribute("employeeId", employee.getId());
		request.setAttribute("entity", employee);

		return Path.FORWARD_VIEW_ADMIN_PAGE;
	}
}
