package com.epam.isp.web.command.entity.employee;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.epam.isp.Path;
import com.epam.isp.core.employee.Employee;
import com.epam.isp.core.employee.EmployeeManager;
import com.epam.isp.core.employee.EmployeeManagerImpl;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.utils.validation.Validation;
/**
 * Invokes when user want to update employees.
 * 
 * @author Iliya Rudberg.
 *
 */
public class UpdateEmployeeCommand extends Command{
	private static final long serialVersionUID = 3315917092461024179L;

	private static final Logger logger = Logger.getLogger(UpdateEmployeeCommand.class);
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		logger.debug("Start executing Command");

		String result = null;
		if (ActionType.POST == actionType) {
			result = doPost(request, response);
		}

		logger.debug("Finished executing Command");

		return result;
	}
	/**
	 * Redirects to view all employees after submitting update employee form.
	 *
	 * @return path to the view of updated employee if fields properly filled,
	 *         otherwise redisplays update employee page.
	 * @throws IOException
	 * @throws ServletException
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String lang = (String)request.getSession().getAttribute("language");
		String message = null;
		boolean complete= false;
		// validation
		boolean valid = Validation.isFilled(
				   request.getParameter("name"),
				   request.getParameter("surname"),
				   request.getParameter("address"),
				   request.getParameter("phone"));
		logger.trace("Validation: " + valid);
		if (valid){
			valid = Validation.isCorrectEmail(request.getParameter("email"));
		}
		if (valid) {
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String address = request.getParameter("address");
		int phone = Integer.parseInt(request.getParameter("phone"));
		String position = request.getParameter("position");
		String email = request.getParameter("email");
		logger.trace("The fields got: "+ id+ " " + name + " " + surname + " " + address + " " + phone + " " + position+" "+ email);
		// create employee entity
		Employee employee = new Employee(id, name, surname, address, phone,position, email);
		logger.trace("Employee was created: " + employee);
		// update employee at database
			EmployeeManager manager = new EmployeeManagerImpl();
			complete =manager.updateEmployeeById(employee);
		logger.trace("Employee was updated at database");
		response.reset();
		}
		if (complete == true){
			if (lang == null || lang.equals("en")) { 
				message = "Employee was updated"; 
				} else if (lang.equals("ru")) { 
				message = "Сотрудник был обновлен"; 
				}else if (lang.equals("by")) { 
				message = "Супрацоўнік быў абноўлен"; 
				} }
			else{if (lang == null || lang.equals("en")) { 
					message = "Cannot update a employee"; 
					} else if (lang.equals("ru")) { 
					message = "Не удалось обновить сотрудника"; 
					}else if (lang.equals("by")) { 
					message = "Не ўдалося абнавіць супрацоўніка"; 
					} }
		request.getSession().setAttribute("message", message);
		return Path.REDIRECT_TO_VIEW_ALL_EMPLOYEES;
	}

}
