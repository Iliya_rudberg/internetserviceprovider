package com.epam.isp.web.command.transaction;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.epam.isp.Path;
import com.epam.isp.core.contract.Contract;
import com.epam.isp.core.contract.ContractManager;
import com.epam.isp.core.contract.ContractManagerImpl;
import com.epam.isp.core.transaction.TransactionManager;
import com.epam.isp.core.transaction.TransactionManagerImpl;
import com.epam.isp.core.transaction.Transaction;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.utils.validation.Validation;
/**
 * Delete transaction command.
 * 
 * @author Iliya Rudberg
 *
 */
public class DeleteTransactionCommand extends Command {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(DeleteTransactionCommand.class);
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");

		String result = null;
		if (ActionType.POST == actionType) {
			result = doPost(request, response);
		}

		logger.debug("Finished executing Command");

		return result;
	}
	/**
	 * Redirects to view all transactions after submitting delete transaction form.
	 *
	 * @return path to the view of deleted transaction if fields properly filled,
	 *         otherwise redisplays delete transaction page.
	 * @throws IOException
	 * @throws ServletException
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		boolean complete = false;
		String lang = (String) request.getSession().getAttribute("language"); 
		String message = ""; 
		TransactionManager manager = new TransactionManagerImpl();
		ContractManager contractManager = new ContractManagerImpl();
		// get current  tariff id from page
		if(Validation.isInt(request.getParameter("transactionID"))){
		int transactionID = Integer.parseInt(request.getParameter("transactionID"));
		Transaction transaction = manager.getTransactionById(transactionID);
		Contract contract = contractManager.getContractById(transaction.getContractId());
		Double balance = contract.getBalance() - transaction.getSum();
		contract.setBalance(balance);
		contractManager.updateContractById(contract);
		complete = manager.deleteTransactionByID(transactionID);
		logger.trace("Tariff id: " + transactionID +" was deleted.");}
		if (complete == true){
			if (lang == null || lang.equals("en")) { 
				message = "Transaction was deleted"; 
				} else if (lang.equals("ru")) { 
				message = "Транзакция была удалена"; 
				}else if (lang.equals("by")) { 
				message = "Транзакцыя была выдалена"; 
				} }
			else{if (lang == null || lang.equals("en")) { 
					message = "Cannot delete a transaction"; 
					} else if (lang.equals("ru")) { 
					message = "Не удалось удалить транзакцию"; 
					}else if (lang.equals("by")) { 
					message = "Не ўдалося выдалиць транзакцыю"; 
					} }
		request.getSession().setAttribute("message", message);
		return Path.REDIRECT_TO_VIEW_ALL_TRANSACTIONS;
	}

}
