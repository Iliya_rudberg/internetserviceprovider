package com.epam.isp.web.command.transaction;

import java.io.IOException;
import java.util.Collection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.isp.Path;
import com.epam.isp.core.contract.Contract;
import com.epam.isp.core.transaction.Transaction;
import com.epam.isp.core.transaction.TransactionManager;
import com.epam.isp.core.transaction.TransactionManagerImpl;
import com.epam.isp.core.user.Role;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.command.contract.GetContractByClientIdCommand;
/**
 * Invokes when user want to get transaction by contract id.
 * 
 * @author Iliya Rudberg.
 *
 */
public class GetTransactionByContractIdCommand extends Command {
	private static final long serialVersionUID = -405445733126752744L;

	private static final Logger logger = Logger.getLogger(GetTransactionByContractIdCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");
		String result = null;

		if (ActionType.GET == actionType) {
			result = doGet(request, response);
		}

		logger.debug("Command finished");
		return result;
	}

	/**
	 * Forward to view payments.
	 * 
	 * @return path to payments.
	 */
	private String doGet(HttpServletRequest request, HttpServletResponse response) {
		
		// set contract if null
		if (request.getSession().getAttribute("userRole").equals(Role.USER)){
			if(request.getSession().getAttribute("account") == null){try {
			GetContractByClientIdCommand.processRequest(request,response);
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				logger.trace("Cannot get the contract");
			}}}
		// get contract 
		Contract contract = (Contract) request.getSession().getAttribute("account");
		//get transaction by contract id
		TransactionManager manager = new TransactionManagerImpl();
		Collection<Transaction> transaction= manager.getTransactionByContractId(contract.getId());
		if (!transaction.isEmpty()){
		logger.trace("Transactions were found by contract id: " + transaction);
		request.setAttribute("transaction", transaction);
		}
		else{request.setAttribute("errorMessage", "Can not find any transaction by this contract id");}
	

		return Path.FORWARD_VIEW_PAYMENTS;
	}
}
