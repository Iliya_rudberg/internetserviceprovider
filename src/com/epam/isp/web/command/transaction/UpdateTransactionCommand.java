package com.epam.isp.web.command.transaction;

import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.epam.isp.Path;
import com.epam.isp.core.contract.Contract;
import com.epam.isp.core.contract.ContractManager;
import com.epam.isp.core.contract.ContractManagerImpl;
import com.epam.isp.core.transaction.TransactionManager;
import com.epam.isp.core.transaction.TransactionManagerImpl;
import com.epam.isp.core.transaction.TransactionType;
import com.epam.isp.core.transaction.Transaction;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.utils.validation.TransactionInputValidator;
/**
 * Update transaction command.
 * 
 * @author Iliya Rudberg
 *
 */
public class UpdateTransactionCommand extends Command{
	private static final long serialVersionUID = 3315917092461024179L;

	private static final Logger logger = Logger.getLogger(UpdateTransactionCommand.class);
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		logger.debug("Start executing Command");

		String result = null;
		if (ActionType.POST == actionType) {
			result = doPost(request, response);
		}

		logger.debug("Finished executing Command");

		return result;
	}
	/**
	 * Redirects to view all transactions after submitting update user form.
	 *
	 * @return path to the view of update transaction if fields properly filled,
	 *         otherwise redisplays update transaction page.
	 * @throws IOException
	 * @throws ServletException
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// validation
		boolean complete = false;
		String lang = (String) request.getSession().getAttribute("language"); 
		String message = ""; 
		boolean valid = TransactionInputValidator.validateTransactionParametrs(request.getParameter("contractID"),
				   request.getParameter("transactionSum"),
				   request.getParameter("transactionType"));	
		if (valid) {
			String[] formatStrings = {"MM/dd/yyyy", "yyyy-MM-dd"};
			DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
			int id = Integer.parseInt(request.getParameter("id"));
			int contractId = Integer.parseInt(request.getParameter("contractID"));
			TransactionType type = TransactionType.valueOf(request.getParameter("transactionType").toUpperCase());
			Double sum = Double.parseDouble(request.getParameter("transactionSum"));
			int employeeId = Integer.parseInt(request.getParameter("employeeId"));
			Date sqlDate = null;
			java.util.Date date =  null;
			for (String formatString : formatStrings)
		    {
		        try
		        {
		        	 date = new SimpleDateFormat(formatString).parse(request.getParameter("date"));
		        }
		        catch (ParseException e) {}
		    }
				sqlDate = Date.valueOf(outputFormat.format(date));
			logger.info("The fields got: "  + id + " " + contractId +   " "
					 + " " + type.toString() + " " + employeeId + " "+ sqlDate);
		// create transaction entity
			Transaction transaction = new Transaction(id, contractId, type, employeeId, sum,sqlDate);
		logger.info("Transaction was updated: " + transaction);
		//update transaction at database
			TransactionManager manager = new TransactionManagerImpl();
			ContractManager contractManager = new ContractManagerImpl();
			Transaction tr = manager.getTransactionById(id);
			Contract contract = contractManager.getContractById(contractId);
			manager.updateTransactionById(transaction);
			double balance = sum - tr.getSum() + contract.getBalance();
			contract.setBalance(balance);
			 complete = contractManager.updateContractById(contract);
		logger.trace("Transaction was updated at database");
		response.reset();
		}
		if (complete == true){
			if (lang == null || lang.equals("en")) { 
				message = "Transaction was updated"; 
				} else if (lang.equals("ru")) { 
				message = "Транзакция была обновлена"; 
				}else if (lang.equals("by")) { 
				message = "Транзакцыя была абноўлена"; 
				} }
			else{if (lang == null || lang.equals("en")) { 
					message = "Cannot update a transaction"; 
					} else if (lang.equals("ru")) { 
					message = "Не удалось обновить транзакцию"; 
					}else if (lang.equals("by")) { 
					message = "Не ўдалося абнавіць транзакцыю"; 
					} }
		request.getSession().setAttribute("message", message);
		return Path.REDIRECT_TO_VIEW_ALL_TRANSACTIONS;
	}

}
