package com.epam.isp.web.command.transaction;

import java.io.IOException;
import java.util.Collection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.epam.isp.Path;
import com.epam.isp.core.transaction.TransactionManager;
import com.epam.isp.core.transaction.TransactionManagerImpl;
import com.epam.isp.core.transaction.Transaction;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
/**
 * Get transactions command.
 * 
 * @author Iliya Rudberg
 *
 */
public class ListTransactionsCommand extends Command {
	private static final long serialVersionUID = 7723206619340362128L;

	private static final Logger logger = Logger.getLogger(ListTransactionsCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");

		String result = null;

		if (ActionType.GET == actionType) {
			result = doGet(request, response);
		}

		logger.debug("Finished executing Command");
		return result;
	}
	/**
	 * Redirects to view all transactions after clicking invoking get transaction command.
	 *
	 * @return path to the view of all transactions.
	 * @throws IOException
	 * @throws ServletException
	 */
	private String doGet(HttpServletRequest request, HttpServletResponse response) {
		TransactionManager manager = new TransactionManagerImpl();

		Collection<Transaction> transaction = manager.getTransactions();
		logger.trace("Transaction found: " + transaction);


		request.setAttribute("transaction", transaction);

		return Path.FORWARD_VIEW_ALL_TRANSACTIONS;
	}

}
