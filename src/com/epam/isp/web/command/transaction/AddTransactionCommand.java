package com.epam.isp.web.command.transaction;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.epam.isp.Path;
import com.epam.isp.core.contract.Contract;
import com.epam.isp.core.contract.ContractManager;
import com.epam.isp.core.contract.ContractManagerImpl;
import com.epam.isp.core.transaction.Transaction;
import com.epam.isp.core.transaction.TransactionManager;
import com.epam.isp.core.transaction.TransactionManagerImpl;
import com.epam.isp.core.transaction.TransactionType;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.utils.validation.TransactionInputValidator;
/**
 * Add transaction command.
 * 
 * @author Iliya Rudberg
 *
 */
public class AddTransactionCommand extends Command {

	private static final long serialVersionUID = 3315917092461024179L;

	private static final Logger logger = Logger.getLogger(AddTransactionCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");
		String result = null;
		if (ActionType.POST == actionType) {
			result = doPost(request, response);
		}

		logger.debug("Finished executing Command");

		return result;
	}

	/**
	 * Redirects to view all transactions after submitting add transaction form.
	 *
	 * @return path to the view of added transaction if fields properly filled,
	 *         otherwise redisplays add transaction page.
	 * @throws IOException
	 * @throws ServletException
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		double sum = 0.0;
		String lang = (String) request.getSession().getAttribute("language"); 
		String message = ""; 
		boolean complete = false;
		// validation
		boolean valid = TransactionInputValidator.validateTransactionParametrs(request.getParameter("contractID"),
					   request.getParameter("transactionSum"),
					   request.getParameter("transactionType"));
		logger.trace("Validation: " + valid);
		if (valid) {
		sum = Double.parseDouble(request.getParameter("transactionSum"));
		TransactionType type= TransactionType.valueOf(request.getParameter("transactionType").toUpperCase());
		int  contractId = Integer.parseInt(request.getParameter("contractID"));
		int employeesId = Integer.parseInt((request.getSession().getAttribute("employeeId")).toString());
		logger.trace("The fields got: "  + sum + " " + type+ " " + contractId +   " "
				+ employeesId );
		// create transaction entity
		Transaction transaction = new Transaction(sum, type, contractId, employeesId);
		logger.trace("Transaction was created: " + transaction);
		
		// add transaction to database and update contract balance
			ContractManager manager = new ContractManagerImpl();
			Contract contract = manager.getContractById(contractId);
			TransactionManager transactionManager = new TransactionManagerImpl();
			transactionManager.addTransaction(transaction);
			double balance = sum + contract.getBalance();
			contract.setBalance(balance);
			complete = manager.updateContractById(contract);
		logger.trace("Transaction was added to database");
		response.reset();
		}
		if (complete == true){
			if (lang == null || lang.equals("en")) { 
				message = "Transaction was successfully added to database"; 
				} else if (lang.equals("ru")) { 
				message = "Транзакция была успешно добавлен в базу данных"; 
				}else if (lang.equals("by")) { 
				message = "Транзакцыя была паспяхова дададзена ў базу дадзеных"; 
				} }
			else{if (lang == null || lang.equals("en")) { 
					message = "Cannot add a transaction"; 
					} else if (lang.equals("ru")) { 
					message = "Не удалось добавить транзакцию"; 
					}else if (lang.equals("by")) { 
					message = "Не ўдалося дабавiць  транзакцыю"; 
					} }
		request.getSession().setAttribute("message", message);
		return Path.REDIRECT_TO_VIEW_ALL_TRANSACTIONS;
	}

}
