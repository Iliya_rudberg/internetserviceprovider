package com.epam.isp.web.command;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.epam.isp.web.command.contract.AddContractCommand;
import com.epam.isp.web.command.contract.ChangeStatusCommand;
import com.epam.isp.web.command.contract.ChangeTariffCommand;
import com.epam.isp.web.command.contract.DeletContractCommand;
import com.epam.isp.web.command.contract.GetContractByClientIdCommand;
import com.epam.isp.web.command.contract.ListContractCommand;
import com.epam.isp.web.command.contract.UpdateContractCommand;
import com.epam.isp.web.command.entity.client.AddClientCommand;
import com.epam.isp.web.command.entity.client.ChangeClientPasswordCommand;
import com.epam.isp.web.command.entity.client.DeleteClientCommand;
import com.epam.isp.web.command.entity.client.GetClientByUserIdCommand;
import com.epam.isp.web.command.entity.client.ListClientsCommand;
import com.epam.isp.web.command.entity.client.UpdateClientCommand;
import com.epam.isp.web.command.entity.employee.AddEmployeeCommand;
import com.epam.isp.web.command.entity.employee.DeleteEmployeeCommand;
import com.epam.isp.web.command.entity.employee.GetEmployeeByUserIdCommand;
import com.epam.isp.web.command.entity.employee.ListEmployeesCommand;
import com.epam.isp.web.command.entity.employee.UpdateEmployeeCommand;
import com.epam.isp.web.command.entity.user.ListUsersCommand;
import com.epam.isp.web.command.tariff.AddTariffCommand;
import com.epam.isp.web.command.tariff.DeleteTariffCommand;
import com.epam.isp.web.command.tariff.ListTariffCommand;
import com.epam.isp.web.command.tariff.UpdateTariffCommand;
import com.epam.isp.web.command.task.AddTaskCommand;
import com.epam.isp.web.command.task.CompleteTaskCommand;
import com.epam.isp.web.command.task.ListTaskCommand;
import com.epam.isp.web.command.transaction.AddTransactionCommand;
import com.epam.isp.web.command.transaction.DeleteTransactionCommand;
import com.epam.isp.web.command.transaction.GetTransactionByContractIdCommand;
import com.epam.isp.web.command.transaction.ListTransactionsCommand;
import com.epam.isp.web.command.transaction.UpdateTransactionCommand;

/**
 * Class that manages all commands.
 *
 * @author Iliya Rudberg
 *
 */
public class CommandManager {

	private static final Logger LOG = Logger.getLogger(CommandManager.class);

	private static Map<String, Command> commands = new HashMap<String, Command>();

	// initialization all commands
	static {
		// common commands
		commands.put("login", new LoginCommand());
		commands.put("logout", new LogoutCommand());
		commands.put("language", new LanguageCommand());
		commands.put("noCommand", new NoCommand());
		//tasks
		commands.put("contactMe", new AddTaskCommand());
		commands.put("listTasks", new ListTaskCommand());
		commands.put("completeTask", new CompleteTaskCommand());
		// users commands
		commands.put("listUsers", new ListUsersCommand());
		// employee commands
		commands.put("addEmployees", new AddEmployeeCommand());
		commands.put("listEmployees", new ListEmployeesCommand());
		commands.put("updateEmployees", new UpdateEmployeeCommand());
		commands.put("getEmployeeByUserId", new GetEmployeeByUserIdCommand());
		commands.put("deleteEmployee", new DeleteEmployeeCommand());
		// tariff commands
		commands.put("addTariffs", new AddTariffCommand());
		commands.put("listTariffs", new ListTariffCommand());
		commands.put("deleteTariff", new DeleteTariffCommand());
		commands.put("updateTariffs", new UpdateTariffCommand());
		// contract commands
				commands.put("getContractByClientId", new GetContractByClientIdCommand());
				commands.put("addContracts", new AddContractCommand());
				commands.put("changeStatus", new ChangeStatusCommand());
				commands.put("changeTariff", new ChangeTariffCommand());
				commands.put("listContracts", new ListContractCommand());
				commands.put("deleteContract", new DeletContractCommand());
				commands.put("updateContracts", new UpdateContractCommand());
		// client commands
		commands.put("listClients", new ListClientsCommand());
		commands.put("addClients", new AddClientCommand());
		commands.put("updateClients", new UpdateClientCommand());
		commands.put("changeClientPassword", new ChangeClientPasswordCommand());
		commands.put("getClientByUserId", new GetClientByUserIdCommand());
		commands.put("deleteClient", new DeleteClientCommand());
		// transaction command
		commands.put("listTransactions", new ListTransactionsCommand());
		commands.put("addTransactions", new AddTransactionCommand());
		commands.put("getTransactionByContractId", new GetTransactionByContractIdCommand());
		commands.put("updateTransactions", new UpdateTransactionCommand());
		commands.put("deleteTransaction", new DeleteTransactionCommand());
		LOG.debug("Command container was successfully initialized");
		LOG.trace("Total number of commands equals to " + commands.size());
	}

	/**
	 * Returns command object which execution will give path to the resource.
	 *
	 * @param commandName
	 *            Name of the command.
	 * @return Command object if container contains such command, otherwise
	 *         specific <code>noCommand</code object will be returned.
	 */
	public static Command get(String commandName) {
		if (commandName == null || !commands.containsKey(commandName)) {
			LOG.trace("Command not found with name = " + commandName);
			return commands.get("noCommand");
		}

		return commands.get(commandName);
	}

}