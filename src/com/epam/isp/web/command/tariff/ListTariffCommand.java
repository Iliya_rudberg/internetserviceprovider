package com.epam.isp.web.command.tariff;

import java.io.IOException;
import java.util.Collection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.epam.isp.core.tariff.TariffManager;
import com.epam.isp.core.tariff.TariffManagerImpl;
import com.epam.isp.core.tariff.Tariff;
import com.epam.isp.core.user.Role;
import com.epam.isp.Path;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.command.contract.GetContractByClientIdCommand;
/**
 * Get all tariffs command.
 * 
 * @author Iliya Rudberg
 *
 */
public class ListTariffCommand  extends Command {
	private static final long serialVersionUID = 7723206619340362128L;

	private static final Logger logger = Logger.getLogger(ListTariffCommand .class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");

		String result = null;

		if (ActionType.GET == actionType) {
			result = doGet(request, response);
		}

		logger.debug("Finished executing Command");
		return result;
	}

	/**
	 * Forward user to page of all tariff.
	 * @return path to view of all tariff page.
	 */
	private String doGet(HttpServletRequest request, HttpServletResponse response) {
		TariffManager manager = new TariffManagerImpl();
		Collection<Tariff> tariff = manager.getTariffs();
		logger.trace("Tariff found: " + tariff);

		request.setAttribute("tariff", tariff);
		if (request.getSession().getAttribute("userRole").equals(Role.USER)){
		if(request.getSession().getAttribute("account") == null){try {
		GetContractByClientIdCommand.processRequest(request,response);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			logger.trace("Cannot get the contract");
		}}
		return Path.FORWARD_VIEW_SERVICES;}
		else return Path.FORWARD_VIEW_ALL_TARIFFS;
	}
}
