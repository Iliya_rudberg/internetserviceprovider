package com.epam.isp.web.command.tariff;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.isp.Path;
import com.epam.isp.core.tariff.TariffManager;
import com.epam.isp.core.tariff.TariffManagerImpl;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.utils.validation.Validation;
/**
 * Delete tariff command.
 * 
 * @author Iliya Rudberg
 *
 */
public class DeleteTariffCommand extends Command {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(DeleteTariffCommand.class);
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		logger.debug("Start executing Command");

		String result = null;
		if (ActionType.POST == actionType) {
			result = doPost(request, response);
		}

		logger.debug("Finished executing Command");

		return result;
	}
	/**
	 * Redirects to view all tariffs after submitting delete tariff form.
	 *
	 * @return path to the view of deleted tariff if fields properly filled,
	 *         otherwise redisplays delete tariff page.
	 * @throws IOException
	 * @throws ServletException
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String lang = (String)request.getSession().getAttribute("language");
		String message = null;
		boolean complete= false;
		TariffManager manager = new TariffManagerImpl();
		// get current  tariff id from page
		Validation.isInt(request.getParameter("tariffID"));
		int tariffID = Integer.parseInt(request.getParameter("tariffID"));
		complete = manager.deleteTariffByID(tariffID);
		logger.trace("Tariff id: " + tariffID +" was deleted.");
		if (complete == true){
			if (lang == null || lang.equals("en")) { 
				message = "Tariff was deleted"; 
				} else if (lang.equals("ru")) { 
				message = "Тариф был удален"; 
				}else if (lang.equals("by")) { 
				message = "Тарыф быў выдалены"; 
				} }
			else{if (lang == null || lang.equals("en")) { 
					message = "Cannot delete a tariff"; 
					} else if (lang.equals("ru")) { 
					message = "Не удалось удалить тариф"; 
					}else if (lang.equals("by")) { 
					message = "Не ўдалося выдалиць тарыф"; 
					} }
		request.getSession().setAttribute("message", message);
		return Path.REDIRECT_TO_VIEW_ALL_TARIFFS;
	}

}
