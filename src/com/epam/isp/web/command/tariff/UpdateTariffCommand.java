package com.epam.isp.web.command.tariff;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.epam.isp.Path;
import com.epam.isp.core.tariff.TariffManager;
import com.epam.isp.core.tariff.TariffManagerImpl;
import com.epam.isp.core.tariff.Tariff;
import com.epam.isp.core.tariff.TariffType;
import com.epam.isp.web.ActionType;
import com.epam.isp.web.command.Command;
import com.epam.isp.web.utils.validation.TariffInputValidator;
/**
 * Update tariff command.
 * 
 * @author Iliya Rudberg
 *
 */
public class UpdateTariffCommand extends Command{
	private static final long serialVersionUID = 3315917092461024179L;

	private static final Logger logger = Logger.getLogger(UpdateTariffCommand.class);
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionType actionType)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		logger.debug("Start executing Command");

		String result = null;
		if (ActionType.POST == actionType) {
			result = doPost(request, response);
		}

		logger.debug("Finished executing Command");

		return result;
	}
	/**
	 * Redirects to view all tariffs after submitting update tariff form.
	 *
	 * @return path to the view of updated tariff if fields properly filled,
	 *         otherwise redisplays update tariff page.
	 * @throws IOException
	 * @throws ServletException
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		double traffic = 0.0;
		boolean complete = false;
		// validation
		boolean valid = TariffInputValidator.validateTariffParametrs(request.getParameter("tariffName"),
				   request.getParameter("tariffDescription"),
				   request.getParameter("tariffCost"),
				   request.getParameter("tariffType").toUpperCase(),
				   request.getParameter("downloadSpeed"),
				   request.getParameter("uploadSpeed"));
		logger.trace("Validation: " + valid);
		if (valid) {
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("tariffName");
		double cost = Double.parseDouble(request.getParameter("tariffCost"));
		TariffType type = TariffType.valueOf(request.getParameter("tariffType").toUpperCase());
		int  download = Integer.parseInt(request.getParameter("downloadSpeed"));
		int  upload = Integer.parseInt(request.getParameter("uploadSpeed"));
		if (!request.getParameter("tariffTraffic").isEmpty())
		 traffic = Double.parseDouble(request.getParameter("tariffTraffic"));
		String description = request.getParameter("tariffDescription");
		logger.trace("The fields got: "+ id+ " " + name + " " + cost + " " + type.toString() + " " + download + " " + upload + " "
				 + " " + traffic + " " + description);
		// create tariff entity
		Tariff tariff = new Tariff(id, name, cost, type, traffic,description,download,upload);
		logger.trace("Tariff was created: " + tariff);
		// update tariff at database
			TariffManager manager = new TariffManagerImpl();
			complete = manager.updateTariffById(tariff);
		logger.trace("Tariff was updated at database");
		response.reset();
		}
		String lang = (String)request.getSession().getAttribute("language");
		String message = null;
		if (complete == true){
			if (lang == null || lang.equals("en")) { 
				message = "Tariff was updated"; 
				} else if (lang.equals("ru")) { 
				message = "Тариф был обновлен"; 
				}else if (lang.equals("by")) { 
				message = "Тарыф быў абноўлен"; 
				} }
			else{if (lang == null || lang.equals("en")) { 
					message = "Cannot update a tariff"; 
					} else if (lang.equals("ru")) { 
					message = "Не удалось обновить тариф"; 
					}else if (lang.equals("by")) { 
					message = "Не ўдалося абнавіць тарыф"; 
					} }
		request.getSession().setAttribute("message", message);
		return Path.REDIRECT_TO_VIEW_ALL_TARIFFS;
	}

}
