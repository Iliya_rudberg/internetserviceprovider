package com.epam.isp.web.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import com.epam.isp.core.tasks.Task;


/**
 * Task custom tag. Displays on jsp row of table with task data.
 * 
 * @author Iliya Rudberg.
 *
 */
public class TaskCustomTag extends  SimpleTagSupport {
	private Task task;
	public void setTask (Task task) {
	this.task = task;
	}
	@Override
	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		out.write("<td class='th-hide'>"+ task.getId() +"</td>");
		out.write("<td>"+ task.getName() +"</td>");
		out.write("<td>"+ task.getPhone() +"</td>");
		out.write("<td class='th-hide'>"+ task.getEmail() +"</td>");
		out.write("<td >"+ task.getMessage() +"</td>");
		out.write("<td class='th-hide'>"+ task.getDate() +"</td>");
		
	}
}
