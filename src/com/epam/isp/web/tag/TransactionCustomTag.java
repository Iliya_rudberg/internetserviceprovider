package com.epam.isp.web.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.epam.isp.core.transaction.Transaction;


/**
 * Transaction custom tag. Displays on jsp row of table with transaction data.
 * 
 * @author Iliya Rudberg.
 *
 */
public class TransactionCustomTag extends  SimpleTagSupport {
	private Transaction transaction;
	public void setTransaction (Transaction transaction) {
	this.transaction = transaction;
	}
	@Override
	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		out.write("<td>"+ transaction.getId() +"</td>");
		out.write("<td>"+ transaction.getContractId() +"</td>");
		out.write("<td class='th-hide'>"+ transaction.getType() +"</td>");
		out.write("<td>"+ transaction.getSum()+"</td>");
		out.write("<td class='th-hide'>"+ transaction.getEmployeesId() +"</td>");
		out.write("<td>"+ transaction.getDate() +"</td>");
		
	}
}
