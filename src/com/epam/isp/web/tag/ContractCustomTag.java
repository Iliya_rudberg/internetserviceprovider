package com.epam.isp.web.tag;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.epam.isp.core.contract.Contract;


/**
 * Contract custom tag. Displays on jsp row of table with contract data.
 * 
 * @author Iliya Rudberg.
 *
 */
public class ContractCustomTag extends  SimpleTagSupport {
	private Contract contract;
	public void setContract (Contract contract) {
	this.contract = contract;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		out.write("<td>"+ contract.getId() +"</td>");
		out.write("<td>"+ contract.getBalance() +"</td>");
		out.write("<td class='th-hide'>"+ contract.getTariffId() +"</td>");
		out.write("<td class='th-hide'>"+ contract.getDate() +"</td>");
		out.write("<td class='th-hide'>"+ contract.getEmployeesId()+"</td>");
		out.write("<td>"+ contract.getClientId() +"</td>");
		out.write("<td>"+ contract.getStatus() +"</td>");
		
	}
}
