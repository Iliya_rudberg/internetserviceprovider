package com.epam.isp.web.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.epam.isp.core.tariff.Tariff;


/**
 * Tariff custom tag. Displays on jsp row of table with tariff data.
 * 
 * @author Iliya Rudberg.
 *
 */
public class TariffCustomTag extends  SimpleTagSupport {
	private Tariff tariff;
	public void setTariff (Tariff tariff) {
	this.tariff = tariff;
	}
	@Override
	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		out.println("<td>" + tariff.getId() + "</td>");
		out.println("<td>" + tariff.getName() + "</td>");
		out.println("<td>" + tariff.getCost() + "</td>");
		out.println("<td class='th-hide'>" + tariff.getType() + "</td>");
		out.println("<td class='th-hide'>" + tariff.getTraffic() + "</td>");
		out.println("<td>" + tariff.getDescription() + "</td>");
		out.println("<td class='th-hide'>" + tariff.getUpload() + "</td>");
		out.println("<td class='th-hide'>" + tariff.getDownload() + "</td>");
		
	}
}
