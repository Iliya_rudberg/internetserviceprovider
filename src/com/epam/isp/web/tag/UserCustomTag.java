package com.epam.isp.web.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import com.epam.isp.core.user.User;


/**
 * User custom tag. Displays on jsp row of table with user data.
 * 
 * @author Iliya Rudberg.
 *
 */
public class UserCustomTag extends  SimpleTagSupport {
	private User user;
	public void setUser(User user) {
	this.user = user;
	}
	@Override
	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		out.write("<td>"+ user.getId() +"</td>");
		out.write("<td>"+ user.getLogin() +"</td>");
		out.write("<td>"+ user.getRoleId() +"</td>");
		
	}
}
