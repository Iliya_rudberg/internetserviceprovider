package com.epam.isp.web.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import com.epam.isp.core.employee.Employee;


/**
 * Employee custom tag. Displays on jsp row of table with employee data.
 * 
 * @author Iliya Rudberg.
 *
 */
public class EmployeeCustomTag extends SimpleTagSupport {
	private Employee employee;



	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@Override
	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		out.println("<td class='th-hide'>" + employee.getId() + "</td>");
		out.println("<td>" + employee.getName() + "</td>");
		out.println("<td>" + employee.getSurname() + "</td>");
		out.println("<td class='th-hide'>" + employee.getAddress() + "</td>");
		out.println("<td>" + employee.getPhone() + "</td>");
		out.println("<td class='th-hide'>" + employee.getPosition() + "</td>");
		out.println("<td class='th-hide'>" + employee.getEmail() + "</td>");
		
	}

}
