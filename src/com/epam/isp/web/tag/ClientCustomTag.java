package com.epam.isp.web.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.epam.isp.core.client.Client;


/**
 * Client custom tag. Displays on jsp row of table with client data.
 * 
 * @author Iliya Rudberg.
 *
 */
public class ClientCustomTag extends  SimpleTagSupport {
	private Client client;
	public void setClient (Client client) {
	this.client = client;
	}
	@Override
	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		out.write("<td class='th-hide'>"+ client.getId() +"</td>");
		out.write("<td>"+ client.getName() +"</td>");
		out.write("<td>"+ client.getSurname() +"</td>");
		out.write("<td class='th-hide'>"+ client.getAddress() +"</td>");
		out.write("<td>"+ client.getPhone() +"</td>");
		out.write("<td class='th-hide'>"+ client.getPassportId() +"</td>");
		out.write("<td class='th-hide'>"+ client.getEmail() +"</td>");
		
	}
}
