package com.epam.isp.web.utils;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

import javax.annotation.Resource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

import com.epam.isp.core.user.User;

/**
 * Mail util class designed to send email for user with information.
 * 
 * @author Iliya Rudberg
 *
 */
public class MailUtils {
	private static final Logger logger = Logger.getLogger(MailUtils.class);
	@Resource(name="mail/Session")
	private static final Session SESSION = init();
	private static final String theme = "Registration";
	
	private static Session init() {
		final String username = "interspeedisp@gmail.com";
		final String password = "iliya1993";

		Properties props = new Properties();
		 props.put("mail.smtp.auth", "true");
	      props.put("mail.smtp.starttls.enable", "true");
	      props.put("mail.smtp.host", "smtp.gmail.com");
	      props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });
		return session;
	}

	/**
	 * Send email with information to user.
	 * 
	 * @param user
	 *            recipient.
	 * @param email
	 *            email address.
	 */
	public static void sendConfirmationEmail(User user, String email) {
	
		try {
			Message msg = new MimeMessage(SESSION);
			msg.setFrom(new InternetAddress("interspeedisp@gmail.com"));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(email));

			setContentToConfirmationEmailEng(msg, user);

			msg.setSentDate(new Date());

			Transport.send(msg);
		} catch (AddressException e) {
			logger.error(e);
		} catch (MessagingException e) {
			logger.error(e);
		} catch (UnsupportedEncodingException e) {
			logger.error(e);
		}
	}

	/**
	 * Set content to confirmation email.
	 * 
	 * @param msg
	 *            massage.
	 * @param user
	 *            recipient.
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	private static void setContentToConfirmationEmailEng(Message msg, User user)
			throws MessagingException, UnsupportedEncodingException {
		msg.setSubject(theme);

		Multipart multipart = new MimeMultipart();

		InternetHeaders emailAndPass = new InternetHeaders();
		emailAndPass.addHeader("Content-type", "text/plain; charset=UTF-8");
		String hello = "Hello, " + user.getLogin() + "  !\n"
				+ " You successfuly registered on our web-site!\n\n\n";

		String data = "\nLogin: " + user.getLogin() + "\nPassword: " + user.getPassword() + "\n\n";

		MimeBodyPart greetingAndData = new MimeBodyPart(emailAndPass, (hello + data).getBytes("UTF-8"));

		multipart.addBodyPart(greetingAndData);

		msg.setContent(multipart);
	}

}

