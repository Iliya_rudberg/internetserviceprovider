package com.epam.isp.web.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
/**
 * Generate hash to get or add user login from data base.
 * 
 * @author Iliya Rudberg
 *
 */
public class Utils {

	/**
	 * Generate hash in SHA 256.
	 * 
	 * @param password
	 *            input value.
	 * @return password 
	 * 			  in SHA-256
	 */
	public static String generateHash(String password){
		 MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	        md.update(password.getBytes());

	        byte byteData[] = md.digest();

	        //convert the byte to hex format method 1
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < byteData.length; i++) {
	         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
	        }
		return sb.toString();
		
	}
}
