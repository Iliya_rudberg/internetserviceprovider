package com.epam.isp.web.utils.validation;

/**
 * Validation class contains common methods to validate data.
 * 
 * @author Iliya Rudberg
 *
 */
public class Validation {
	private static final String passwordRegex = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})";
	private static final String loginRegex = "[a-zA-Z][a-zA-Z0-9_]{4,10}$";
	private static final String dateRegex = "[0-9/]{0,5}[0-9/]{0,3}[/0-9]{0,3}$";
	private static final String emailRegex = "^[-\\w.]+@([A-z0-9][-A-z0-9]+\\.)+[A-z]{2,4}$";

	/**
	 * Validate fields.
	 * 
	 * @param values
	 *            specified fields.
	 * @return true if the validate success.
	 */
	public static boolean isFilled(String... values) {

		for (String value : values) {
			if (value.isEmpty()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Validate email input.
	 * 
	 * @param email
	 *            specified email.
	 * @return true if validation success.
	 */
	public static boolean isCorrectEmail(String email) {
		if (!email.matches(emailRegex)) {
			return false;
		}
		return true;
	}
	/**
	 * Validate login input.
	 * 
	 * @param login
	 *            specified login.
	 * @return true if validation success.
	 */
	public static boolean isCorrectLogin(String login) {
		if (!login.matches(loginRegex)) {
			return false;
		}
		return true;
	}
	/**
	 * Check if enum contains.
	 * 
	 * @param value
	 *            specified enum value.
	 * @param enumData
	 *            specified enum class.
	 * @return true if validation success.
	 */
	public static <E extends Enum<E>> boolean contains(String value, Class<E> enumData) {

	    for (Enum<E> c : enumData.getEnumConstants()) {
	        if (c.name().equals(value)) {
	            return true;
	        }
	    }

	    return false;
	}
	/**
	 * Validate int input.
	 * 
	 * @param int
	 *            specified int.
	 * @return true if validation success.
	 */
	 public static boolean isInt(String str) {  
		 try {
	            Integer.parseInt(str);
	            return true;
	        } catch (NumberFormatException e) {
	            return false;
	        }
	    }
		/**
		 * Validate double input.
		 * 
		 * @param double
		 *            specified double.
		 * @return true if validation success.
		 */
	 public static boolean isDouble(String str) {

		 try {
	            Double.parseDouble(str);
	            return true;
	        } catch (NumberFormatException e) {
	            return false;
	        }
	    }
		/**
		 * Validate password input.
		 * 
		 * @param password
		 *            specified password.
		 * @return true if validation success.
		 */
	public static boolean isCorrectPassword(String pas) {
		if (!pas.matches(passwordRegex)) {
			return false;
		}
		
		return true;
	}
	/**
	 * Validate date input.
	 * 
	 * @param date
	 *            specified date.
	 * @return true if validation success.
	 */
	public static boolean isCorrectDate(String date) {
		if (!date.matches(dateRegex)) {
			return false;
		}
		
		return true;
	}
	@Override
	public boolean equals(Object other){
	    if (other == null) return false;
	    if (other == this) return true;
		return false;
	}
}
