package com.epam.isp.web.utils.validation;


/**
 * Validator for add client input.
 * 
 * @author Iliya Rudberg
 *
 */
public class ClientInputValidator {

	/**
	 * Validate client input data.
	 * 
	 * @param firstName
	 *            specified first name.
	 * @param lastName
	 *            specified last name.
	 * @return true if validation success.
	 */
	public static boolean validatePatientParametrs(String firstName, String lastName) {
		return (Validation.isFilled(firstName, lastName) );
	}

	
}
