package com.epam.isp.web.utils.validation;

/**
 * User input validator.
 * 
 * @author Iliya Rudberg
 *
 */
public class UserInputValidator {

	/**
	 * Validate user input data.
	 * 
	 * @param login
	 *            specified login
	 * @param password
	 *            specified password
	 * @param email
	 *            specified email.
	 * @return true if validation success.
	 */
	public static boolean validateUserParametrs( String login, String password,String email) {
		boolean valid = true;
		if (!Validation.isCorrectEmail(email)){
			valid =  false;
		}
		if (!Validation.isCorrectLogin(login)){
			valid =  false;
		}
		if (!Validation.isCorrectPassword(password)){
			valid =  false;
		}
		return valid;
	}
	
	/**
	 * Validate email input.
	 * 
	 * @param email
	 *            specified email.
	 * @return true if validation success.
	 */
	public static boolean validateEmail(String email) {
		return Validation.isCorrectEmail(email);
	}

}
