package com.epam.isp.web.utils.validation;

import com.epam.isp.core.transaction.TransactionType;

/**
 * Validator for transaction input.
 * 
 * @author Iliya Rudberg
 *
 */
public class TransactionInputValidator {

	/**
	 * Validate contract input data.
	 * 
	 * @param contractId
	 *            specified contract id.
	 * @param sum
	 *            specified sum.
	 * @param type
	 *            specified transaction type.
	 * @return true if validation success.
	 */
	public static boolean validateTransactionParametrs(String contractId, String sum,String type
			  ) {
		if (Validation.isFilled(sum, contractId, type)){
			if(Validation.isDouble(sum)){
				if(Validation.contains(type.toUpperCase(), TransactionType.class)){
					if(Validation.isInt(contractId)){
			return true;}}}}
		return false;
}

	
}
