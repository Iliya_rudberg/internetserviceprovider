package com.epam.isp.web.utils.validation;

import com.epam.isp.core.tariff.TariffType;

/**
 * Validator for tariff input.
 * 
 * @author Iliya Rudberg
 *
 */
public class TariffInputValidator {

	/**
	 * Validate tariff input data.
	 * 
	 * @param name
	 *            specified name.
	 * @param description
	 *            specified  description.
	 * @param download
	 *            specified download.
	 * @param type
	 *            specified type.
	 * @param upload
	 *            specified last upload.
	 * @param cost
	 *            specified last cost.
	 * @return true if validation success.
	 */
	public static boolean validateTariffParametrs(String name, String description,String cost, 
			  String type, String download, String upload 
			  ) {
		if (Validation.isFilled(name, cost, type, download, upload)){
			if(Validation.isDouble(cost)){
				if(Validation.contains(type, TariffType.class)){
					if(Validation.isInt(download)){
						if(Validation.isInt(upload)){
							return true;}}}}
		}
		return false;
	}

	
}
