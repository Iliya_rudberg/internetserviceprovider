package com.epam.isp.web.utils.validation;

import com.epam.isp.core.contract.Status;

/**
 * Validator for contract input.
 * 
 * @author Iliya Rudberg
 *
 */
public class ContractInputValidator {

	/**
	 * Validate contract input data.
	 * 
	 * @param balance
	 *            specified balance.
	 * @param clientId
	 *            specified client id.
	 * @param status
	 *            specified contract status.
	 * @param tariffId
	 *            specified tariff id.
	 * @param date
	 *            specified date.
	 * @return true if validation success.
	 */
	public static boolean validateContractParametrs(String balance, String clientId,String status, 
			  String tariffId, String date
			  ) {
		
		if (Validation.isFilled(balance, status, tariffId, clientId, date)){
			if(Validation.isDouble(balance)){
				if(Validation.contains(status.toUpperCase(), Status.class)){
					if(Validation.isInt(tariffId)){
						if(Validation.isInt(clientId)){
			return true;}}}}}
		return false;
}

	
}
