package com.epam.isp;


/**
 * Contains all paths.
 * 
 * @author Iliya Rudberg
 *
 */
public class Path {

	public static final String ERROR_PAGE = "/WEB-INF/jsp/errorPage.jsp";
	public static final String WELCOME_PAGE = "login.jsp";
	
	public static final String FORWARD_VIEW_ALL_TASKS = "/WEB-INF/jsp/listTasks.jsp";
	public static final String FORWARD_VIEW_ADMIN_PAGE = "/WEB-INF/jsp/adminPage.jsp";
	public static final String FORWARD_VIEW_ALL_USERS = "/WEB-INF/jsp/listUsers.jsp";
	public static final String FORWARD_VIEW_ALL_EMPLOYEES= "/WEB-INF/jsp/listEmployees.jsp";
	public static final String FORWARD_VIEW_ALL_TRANSACTIONS= "/WEB-INF/jsp/listTransaction.jsp";
	public static final String FORWARD_VIEW_ALL_CLIENTS= "/WEB-INF/jsp/listClients.jsp";
	public static final String FORWARD_VIEW_SERVICES = "/WEB-INF/jsp/tariffs_services.jsp";
	public static final String FORWARD_VIEW_ALL_TARIFFS = "/WEB-INF/jsp/listTariffs.jsp";
	public static final String FORWARD_VIEW_ALL_CONTRACTS = "/WEB-INF/jsp/listContracts.jsp";
	public static final String FORWARD_VIEW_ACCOUNT_STATE = "/WEB-INF/jsp/accountState.jsp";
	public static final String FORWARD_VIEW_PAYMENTS = "/WEB-INF/jsp/payments.jsp";
	public static final String REDIRECT_TO_GET_TASKS = "controller?command=listTasks";
	public static final String REDIRECT_TO_GET_EMPLOYEE = "controller?command=getEmployeeByUserId";
	public static final String REDIRECT_TO_GET_CLIENT = "controller?command=getClientByUserId";
	public static final String REDIRECT_TO_VIEW_ALL_USERS = "controller?command=listUsers";
	public static final String REDIRECT_TO_VIEW_ALL_TARIFFS = "controller?command=listTariffs";
	public static final String REDIRECT_TO_VIEW_ALL_EMPLOYEES = "controller?command=listEmployees";
	public static final String REDIRECT_TO_VIEW_ALL_CLIENTS = "controller?command=listClients";
	public static final String REDIRECT_TO_VIEW_ALL_CONTRACTS = "controller?command=listContracts";
	public static final String REDIRECT_TO_VIEW_ADD_USER_FORM = "controller?command=addUser";
	public static final String REDIRECT_TO_LOGIN_FORM = "controller?command=login";
	public static final String REDIRECT_TO_VIEW_ALL_TRANSACTIONS = "controller?command=listTransactions";
	

}
