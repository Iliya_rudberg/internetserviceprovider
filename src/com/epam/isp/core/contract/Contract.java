package com.epam.isp.core.contract;

import java.io.Serializable;
import java.sql.Date;
import com.epam.isp.core.tariff.Tariff;

/**
 * Contract entity. Provides entity field's and get/set methods to hims.
 * 
 * @author Iliya Rudberg
 *
 */
public class Contract  implements Serializable {

	private static final long serialVersionUID = -897453822651366267L;
	private int id;
	private double balance;
	private int employeesId;
	private int clientId;
	private Date date;
	private Status status;
	private int tariffId;
	private Tariff tariff;
	
	public Contract(int id,double balance, int employeesId, int clientId, Date date,Status status,int tariffId) {
		this.setId(id);
		this.balance = balance;
		this.employeesId = employeesId;
		this.clientId = clientId;
		this.date = date;
		this.status = status;
		this.tariffId =  tariffId;
	}
	
	public Contract(double balance, int employeesId, int clientId, Date date,Status status,int tariffId) {
		this.balance = balance;
		this.employeesId = employeesId;
		this.clientId = clientId;
		this.date = date;
		this.status = status;
		this.tariffId =  tariffId;
	}
	public Contract(int id, double balance) {
		this.setId(id);
		this.balance = balance;
	}
	public Contract(int id, int tariffId) {
		this.setId(id);
		this.tariffId = tariffId;
	}
	public Contract(int id, Status status, Tariff tariff,Date date, double balance) {
		this.setId(id);
		this.date = date;
		this.status = status;
		this.tariff = tariff;
		this.balance = balance;
	}
	public Contract(int id, Status status) {
		this.setId(id);
		this.status = status;
	}
	public int getId() {
		return id;
	}
	public Tariff getTariff() {
		return tariff;
	}

	public void setTariff(Tariff tariff) {
		this.tariff = tariff;
	}
	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public int getEmployeesId() {
		return employeesId;
	}

	public void setEmployeesId(int employeesId) {
		this.employeesId = employeesId;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public void setId(int id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}

	public void setdate(Date date) {
		this.date = date;
	}
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	public int getTariffId() {
		return tariffId;
	}

	public void setTariffId(int tariffId) {
		this.tariffId =  tariffId;
	}
}
