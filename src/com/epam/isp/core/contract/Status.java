package com.epam.isp.core.contract;
/**
 * Status enum class. Consist by possible contract status.
 * 
 * @author Iliya Rudberg
 *
 */
public enum Status {
BLOCKED,WORKING,DISABLED;
}
