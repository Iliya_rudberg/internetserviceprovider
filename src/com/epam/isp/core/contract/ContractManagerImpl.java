package com.epam.isp.core.contract;

import java.util.List;

import com.epam.isp.dao.impl.ContractDaoImpl;
/**
 * Contract manager implementation. Here the business logic call dao methods.
 * 
 * @author Iliya Rudberg
 *
 */
public class ContractManagerImpl implements ContractManager {
	private ContractDao contractDao = new ContractDaoImpl();

	@Override
	public Contract getContractByClientId(int clientId) {
		// TODO Auto-generated method stub
		return contractDao.getContractByClientId(clientId);
	}
	@Override
	public List<Contract> getContractsByEmployeeId(int employeeId) {
		// TODO Auto-generated method stub
		return contractDao.getContractsByEmployeeId(employeeId);
	}
	@Override
	public boolean addContract(Contract contract) {
		// TODO Auto-generated method stub
		return contractDao.addContract(contract);
	}

	@Override
	public boolean changeStatusOfContract(Contract contract) {
		// TODO Auto-generated method stub
		 return contractDao.changeStatusOfContract(contract);
	}
	@Override
	public List<Contract> getContractsByStatus(Status status) {
		// TODO Auto-generated method stub
		return contractDao.getContractsByStatus(status);
	}
	@Override
	public List<Contract> getContracts() {
		// TODO Auto-generated method stub
		return contractDao.getContracts();
	}

	@Override
	public boolean deleteContractByID(int contractID) {
		// TODO Auto-generated method stub
		 return contractDao.deleteContractByID(contractID);
	}

	@Override
	public boolean updateContractById(Contract contract) {
		// TODO Auto-generated method stub
		 return contractDao.updateContractById(contract);
	}

	@Override
	public Contract getContractById(int id) {
		// TODO Auto-generated method stub
		return contractDao.getContractById(id);
	}

	

	@Override
	public boolean changeTariffOfContract(Contract contract) {
		// TODO Auto-generated method stub
		return contractDao.changeTariffOfContract(contract);
	}

}
