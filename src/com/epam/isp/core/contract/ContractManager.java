package com.epam.isp.core.contract;

import java.util.List;

/**
 * Contract manager.
 * 
 * @author Iliya Rudberg
 *
 */
public interface ContractManager {
	/**
	 * Get  contract by client id.
	 * 
	 * @param clientId
	 *            specified clientId.
	 * @return Contract entity.
	 */
	Contract getContractByClientId(int clientId);
	/**
	 * Get contract by employee id.
	 * 
	 * @param employeeId
	 *            specified employeeId.
	 * @return list of contracts.
	 */
	List<Contract> getContractsByEmployeeId(int employeeId);
	/**
	 * Add contract to database.
	 * 
	 * @param contract
	 *            specified contract.
	 * @return boolean value.
	 */
	boolean addContract(Contract contract);
	/**
	 * Change status of contract.
	 * 
	 * @param status
	 *            specified status.
	 * @return boolean value.
	 */
	boolean changeStatusOfContract(Contract contract);
	
	/**
	 * Get contracts by status.
	 * 
	 * @param status
	 *            specified status.
	 * @return list of contracts.
	 */
	List<Contract> getContractsByStatus(Status status);
	/**
	 * Get all contracts from database.
	 * 
	 * @return list of contracts.
	 */
	List<Contract> getContracts();
	/**
	 * Delete contract by id.
	 * 
	 * @param contractID
	 *            specified contractID.
	 * @return boolean value.
	 */
	boolean deleteContractByID(int contractID);
	/**
	 * Update  contract by id.
	 * 
	 * @param contract
	 *            specified contract.
	 * @return boolean value.
	 */
	boolean updateContractById(Contract contract);
	/**
	 * Get contract by id.
	 * 
	 * @param id
	 *            specified id.
	 * @return Contract entity.
	 */
	Contract getContractById(int id);
	/**
	 * Change tariff of contract.
	 * 
	 * @param contract
	 *            specified Contract.
	 * @return boolean value.
	 */
	boolean changeTariffOfContract(Contract contract);
}
