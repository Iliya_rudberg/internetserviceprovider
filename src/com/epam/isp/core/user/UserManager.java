package com.epam.isp.core.user;

import java.util.List;

import com.epam.isp.LoginDuplicateException;

/**
 * Manager interface for User entity.
 * 
 * @author Iliya Rudberg
 *
 */
public interface UserManager {


	/**
	 * Get all users from database.
	 * 
	 * @return All users from database.
	 */
	List<User> getUsers();
	/**
	 * Delete user by id.
	 * 
	 * @param userID
	 *            specified userID.
	 * @return boolean value
	 */
	boolean deleteUserByID(int userID);
	/**
	 * Add user to database.
	 * 
	 * @param user
	 *            specified user.
	 * @return boolean value
	 */
	boolean addUser(User user) throws LoginDuplicateException;

	/**
	 * Get user by login
	 * 
	 * @param login
	 *            specified login.
	 * @return user who was found in database.
	 */
	User getUserByLogin(String login);

	/**
	 * Get user by id.
	 * 
	 * @param id
	 *            specified id.
	 * @return user entity.
	 */
	User getUserById(int id);

	/**
	 * Get all roles that can be used.
	 * 
	 * @return list of roles.
	 */
	List<Role> getRoles();
}
