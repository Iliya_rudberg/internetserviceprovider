package com.epam.isp.core.user;

import java.io.Serializable;

import com.epam.isp.core.Entity;

/**
 * User entity.
 * 
 * @author Iliya Rudberg
 *
 */
public class User extends Entity implements Serializable {

	private static final long serialVersionUID = 4837649241480654204L;

	private String login; 
	private String password;
	private int roleId;
	private Role role;
	int id;
	public User(int id, String login, String password, int roleId) {
		this.id = id; 
		this.login = login;
		this.password = password;
		this.roleId = roleId;
	}
	public User(String login, String password, int roleId) {
		this.login = login;
		this.password = password;
		this.roleId = roleId;
	}
	public User(String login, String password) {
		this.login = login;
		this.password = password;
	}
	public User(int id,String login, String password, Role role) {
		this.id = id; 
		this.role =  role;
		this.login = login;
		this.password = password;
	}
	public int getId(){
		return this.id;
	}
	public void setId(int id){
		this.id = id;
	}
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	public Role getRole(){
		return role;
	}
	public void setRole(Role role){
		this.role = role;
	}
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
