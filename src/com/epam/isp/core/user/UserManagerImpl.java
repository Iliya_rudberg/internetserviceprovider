package com.epam.isp.core.user;

import java.util.List;

import com.epam.isp.dao.impl.UserDaoImpl;
import com.epam.isp.LoginDuplicateException;

/**
 * UserDao implementation. Here core layer call dao methods.
 * 
 * @author Iliya Rudberg
 *
 */
public class UserManagerImpl implements UserManager {

	UserDao userDao = new UserDaoImpl();

	@Override
	public List<User> getUsers() {
		return userDao.getUsers();
	}


	@Override
	public boolean addUser(User user) throws LoginDuplicateException {
		return userDao.addUser(user);
	}

	@Override
	public User getUserByLogin(String login) {
		return userDao.getUserByLogin(login);
	}

	@Override
	public List<Role> getRoles() {
		return userDao.getRoles();
	}


	@Override
	public User getUserById(int id) {
		return userDao.getUserById(id);
	}
	@Override
	public boolean deleteUserByID(int userID) {
		// TODO Auto-generated method stub
		return userDao.deleteUserByID(userID);
	}

}
