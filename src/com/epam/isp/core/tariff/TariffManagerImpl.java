package com.epam.isp.core.tariff;
import java.util.List;

import com.epam.isp.dao.impl.TariffDaoImpl;

/**
 * Tariff manager implementation. Here the business logic call dao methods.
 * 
 * @author Iliya Rudberg
 *
 */
public class TariffManagerImpl implements TariffManager {
	private TariffDao tariffDao = new TariffDaoImpl();
	@Override
	public List<Tariff> getTariffs() {
		// TODO Auto-generated method stub
		return tariffDao.getTariffs();
	}
	@Override
	public boolean addTariff(Tariff tariff) {
		// TODO Auto-generated method stub
		return tariffDao.addTariff(tariff);
	}

	@Override
	public boolean deleteTariffByID(int tariffID) {
		// TODO Auto-generated method stub
		return tariffDao.deleteTariffByID(tariffID);
	}

	@Override
	public boolean updateTariffById(Tariff tariff) {
		// TODO Auto-generated method stub
		return tariffDao.updateTariffById(tariff);
	}
	@Override
	public Tariff getTariffById(int id) {
		// TODO Auto-generated method stub
		return tariffDao.getTariffById(id);
	}
}
