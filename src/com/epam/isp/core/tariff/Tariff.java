package com.epam.isp.core.tariff;

import java.io.Serializable;

/**
 * Tariff entity. Provides entity field's and get/set methods to hims.
 * 
 * @author Iliya Rudberg.
 *
 */
public class Tariff implements Serializable {

	private static final long serialVersionUID = 8924384621418707436L;
	private String name;
	private double cost;
	private TariffType type;
	private int download;
	private int upload;
	private double traffic;
	private String description;
	private int id;
	public Tariff(String name, double cost,TariffType type,double traffic,String description,int download,int upload) {
		this.name = name;
		this.cost = cost;
		this.type = type;
		this.traffic = traffic;
		this.description =  description;
		this.download =  download;
		this.upload =  upload;
	}
	public Tariff(int id,String name, double cost,TariffType type,double traffic,String description,int download,int upload) {
		this.setId(id);
		this.name = name;
		this.cost = cost;
		this.type = type;
		this.traffic = traffic;
		this.description =  description;
		this.download =  download;
		this.upload =  upload;
	}
	public Tariff(String name, double cost) {
		this.name = name;
		this.cost = cost;
	}
	public String getDescription(){
		return description;
	}
	public void setDecsription(String description){
		this.description = description ;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public double getCost() {
		return cost;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public TariffType getType() {
		return type;
	}

	public void setType(TariffType type) {
		this.type = type;
	}
	public int getDownload() {
		return download;
	}

	public void setDownload(int download) {
		this.download =  download;
	}
	public int getUpload() {
		return upload;
	}

	public void setUpload(int upload) {
		this.upload =  upload;
	}
	public double getTraffic() {
		return traffic;
	}

	public void setTraffic(Double traffic) {
		this.traffic = traffic;
	}


}
