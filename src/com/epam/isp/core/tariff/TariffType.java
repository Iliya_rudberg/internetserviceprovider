package com.epam.isp.core.tariff;
/**
 * Tariff type enum class. Consist by possible types of tariff.
 * 
 * @author Iliya Rudberg
 *
 */
public enum TariffType {
LIMITED,UNLIMITED;
}
