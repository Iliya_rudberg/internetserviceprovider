package com.epam.isp.core.tariff;

import java.util.List;
/**
 * Tariff manager.
 * 
 * @author Iliya Rudberg
 *
 */
public interface TariffManager {
	/**
	 * Get all tariffs from database.
	 * 
	 * @return list of tariffs.
	 */
	List<Tariff> getTariffs();
	/**
	 * Get tariff by id from database.
	 * 
	 * @param id
	 *            specified id.
	 * @return Tariff entity.
	 */
	Tariff getTariffById(int id);
	/**
	 * Add new tariff.
	 * 
	 * @param tariff
	 *            specified tariff.
	 * @return boolean value
	 */
	boolean addTariff(Tariff tariff);
	/**
	 * Delete tariff by id.
	 * 
	 * @param tariffID
	 *            specified tariffID.
	 * @return boolean value
	 */
	
	boolean deleteTariffByID(int tariffID);
	/**
	 * Update tariff by id.
	 * 
	 * @param tariffID
	 *            specified tariffID.
	 * @return boolean value
	 */
	boolean updateTariffById(Tariff tariff);
}
