package com.epam.isp.core.employee;

import java.util.List;
import com.epam.isp.dao.impl.EmployeeDaoImpl;
/**
 * Employee manager implementation. Here the business logic call dao methods.
 * 
 * @author Iliya Rudberg
 *
 */
public class EmployeeManagerImpl implements EmployeeManager {
	private EmployeeDao employeeDao = new EmployeeDaoImpl();
	@Override
	public List<Employee> getEmployee() {
		// TODO Auto-generated method stub
		return employeeDao.getEmployee();
	}

	@Override
	public boolean addEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return employeeDao.addEmployee(employee);
	}

	@Override
	public Employee getEmployeeById(int id) {
		// TODO Auto-generated method stub
		return employeeDao.getEmployeeById(id);
	}
	
	@Override
	public Employee getEmployeeByUserId(int id) {
		// TODO Auto-generated method stub
		return employeeDao.getEmployeeByUserId(id);
	}

	

	@Override
	public boolean deleteEmployeeByID(int employeeID) {
		// TODO Auto-generated method stub
		return employeeDao.deleteEmployeeByID(employeeID);
	}

	@Override
	public boolean  updateEmployeeById(Employee employee) {
		// TODO Auto-generated method stub
		return employeeDao.updateEmployeeById(employee);
	}

}
