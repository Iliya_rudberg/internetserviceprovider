package com.epam.isp.core.employee;

import java.io.Serializable;

import com.epam.isp.core.Entity;

/**
 * Employee entity. Provides entity field's and get/set methods to hims.
 * 
 * @author Iliya Rudberg
 *
 */
public class Employee extends Entity implements Serializable {

	private static final long serialVersionUID = 2613496056932204312L;
	
	private int id;
	private String name;
	private String surname;
	private String address;
	private String email;
	private int phone;
	private String position;
	private int userId;

	public Employee(String name, String surname,String address, int phone, String position,String email,int userId) {
		this.name = name;
		this.surname = surname;
		this.address = address;
		this.phone = phone;
		this.position = position;
		this.email = email;
		this.userId =  userId;
		

	}
	
	public Employee(int id, String name, String surname,String address, int phone, String position, String email) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.address = address;
		this.phone = phone;
		this.position = position;
		this.email = email;
	}
	
	public Employee(String name, String surname) {
		this.name = name;
		this.surname = surname;

	}
	public int getId(){
		return this.id;
	}
	public void setId(int id){
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public void setname(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getEmail() {
		// TODO Auto-generated method stub
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}

