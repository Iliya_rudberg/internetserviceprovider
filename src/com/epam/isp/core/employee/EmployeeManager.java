package com.epam.isp.core.employee;

import java.util.List;
/**
* Employee manager.
* 
* @author Iliya Rudberg
*
*/
public interface EmployeeManager {
	/**
	 * Get all employee from database.
	 * 
	 * @return list of employees.
	 */
	List<Employee> getEmployee();

	/**
	 * Add employee to database.
	 * 
	 * @param employee
	 *            specified employee.
	 * @return boolean value
	 */
	boolean addEmployee(Employee employee);

	/**
	 * Get employee by id.
	 * 
	 * @param id
	 *            specified id.
	 * @return employee entity.
	 */
	Employee getEmployeeById(int id);
	/**
	 * Get employee by user id.
	 * 
	 * @param id
	 *            specified id.
	 * @return employee entity.
	 */
	Employee getEmployeeByUserId(int id);
	/**
	 * Delete employee by id.
	 * 
	 * @param tariffID
	 *            specified tariffID.
	 * @return boolean value
	 */
	boolean deleteEmployeeByID(int employeeID);
	/**
	 * Update employee by id.
	 * 
	 * @param employee
	 *            specified employee.
	 * @return boolean value
	 */
	boolean updateEmployeeById(Employee employee);
}
