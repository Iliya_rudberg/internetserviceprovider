package com.epam.isp.core.tasks;

import java.io.Serializable;
import java.sql.Date;

import com.epam.isp.core.Entity;

/**
 * Task entity. Provides entity field's and get/set methods to hims.
 * 
 * @author Iliya Rudberg
 *
 */
public class Task extends Entity implements Serializable {

	private static final long serialVersionUID = 2613496056932204312L;
	
	private int id;
	private String name;
	private String email;
	private int phone;
	private String message;
	private Date date;
	private String status;
	public Task(String name, int phone, String email,String message) {
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.message = message;

		

	}
	
	public Task(int id,String name, int phone, String email,String message,Date date, String status) {
		this.id = id;
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.message = message;
		this.date = date;
		this.status = status;

	}
	
	public int getId(){
		return this.id;
	}
	public void setId(int id){
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public void setname(String name) {
		this.name = name;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}

