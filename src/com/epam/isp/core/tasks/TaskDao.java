package com.epam.isp.core.tasks;

import java.util.List;
/**
 * Task dao interface. Contains all methods for user entity.
 * 
 * @author Iliya Rudberg
 *
 */
public interface TaskDao {
	/**
	 * Show tasks from database.
	 * 
	 * @return tasks list
	 *            specified tasks list.
	 */
	List<Task> listTasks();
	/**
	 * add task to database.
	 * 
	 * @param task
	 *            specified task.
	 * @return boolean value 
	 *            specified value.
	 */
	boolean addTask(Task task);
	/**
	 * complete status of task.
	 * 
	 * @param task
	 *            specified task.
	 * @return boolean value 
	 *            specified value.
	 */
	boolean completeTask(int taskID);
}
