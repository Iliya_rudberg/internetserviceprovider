package com.epam.isp.core.tasks;

import java.util.List;
import com.epam.isp.dao.impl.TaskDaoImpl;
/**
 * Task manager implementation. Here the business logic call dao methods.
 * 
 * @author Iliya Rudberg
 *
 */
public class TaskManagerImpl implements TaskManager {
	private TaskDao taskDao = new TaskDaoImpl();

	@Override
	public List<Task> listTasks() {
		// TODO Auto-generated method stub
		return taskDao.listTasks();
	}

	@Override
	public boolean addTask(Task task) {
		// TODO Auto-generated method stub
		return taskDao.addTask(task);
	}

	@Override
	public boolean completeTask(int taskID) {
		// TODO Auto-generated method stub
		return taskDao.completeTask(taskID);
	}
	
}
