package com.epam.isp.core.tasks;

import java.util.List;
/**
* Task manager.
* 
* @author Iliya Rudberg
*
*/
public interface TaskManager {
	/**
	 * Show tasks from database.
	 * 
	 * @return tasks list
	 *            specified tasks list.
	 */
	List<Task> listTasks();
	/**
	 * add task to database.
	 * 
	 * @param task
	 *            specified task.
	 * @return boolean value 
	 *            specified value.
	 */
	boolean addTask(Task task);
	/**
	 * complete status of task.
	 * 
	 * @param task
	 *            specified task.
	 * @return boolean value 
	 *            specified value.
	 */
	boolean completeTask(int taskID);
}
