package com.epam.isp.core.client;

import java.util.List;
/**
 * Client manager.
 * 
 * @author Iliya Rudberg
 *
 */
public interface ClientManager {
	/**
	 * Get all clients.
	 * 
	 * @return list of clients.
	 */
	List<Client> getClients();
	/**
	 * Add client.
	 * 
	 * @param client
	 *            specified client.
	 * @return boolean value
	 */
	boolean addClient(Client client);


	/**
	 * Get clients by contract id.
	 * 
	 * @param contractId
	 *            specified contract id.
	 * @return client entity.
	 */
	Client getClientByContractId(int contractId);
	/**
	 * Update client by id.
	 * 
	 * @param Client
	 *            specified client.
	 * @return boolean value
	 */
	boolean updateClientById(Client client);
	/**
	 * Delete client by id.
	 * 
	 * @param clientId
	 *            specified clientId.
	 * @return boolean value
	 */
	boolean deleteClientByID(int clientID);
	/**
	 * Get client by user  id.
	 * 
	 * @param userId
	 *            specified userId.
	 * @return client entity.
	 */
	Client getClientByUserId(int userId);
	/**
	 * Change client password.
	 * 
	 * @param clientId
	 *            specified clientId.
	 * @param password
	 *            specified password.
	 * @return boolean value
	 */
	boolean changePassword(int clientID, String password);
}
