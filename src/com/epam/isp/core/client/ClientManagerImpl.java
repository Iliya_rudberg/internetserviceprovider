package com.epam.isp.core.client;

import java.util.List;
import com.epam.isp.dao.impl.ClientDaoImpl;

/**
 * Client manager implementation. Here the business logic call dao methods.
 * 
 * @author Iliya Rudberg
 *
 */
public class ClientManagerImpl implements ClientManager {
	private ClientDao clientDao = new ClientDaoImpl();
	
	@Override
	public List<Client> getClients() {
		return clientDao.getClients();
	}

	@Override
	public boolean addClient(Client client) {
		return clientDao.addClient(client);

	}

	

	@Override
	public Client getClientByContractId(int contractId) {
		// TODO Auto-generated method stub
		return clientDao.getClientByContractId(contractId);
	}

	@Override
	public boolean updateClientById(Client client) {
		// TODO Auto-generated method stub
		 return clientDao.updateClientById(client);
	}

	@Override
	public boolean deleteClientByID(int clientID) {
		// TODO Auto-generated method stub
		 return clientDao.deleteClientByID(clientID);
	}

	@Override
	public Client getClientByUserId(int userId) {
		// TODO Auto-generated method stub
		return clientDao.getClientByUserId(userId);
	}

	@Override
	public boolean  changePassword(int clientID, String password) {
		// TODO Auto-generated method stub
		 return clientDao.changePassword(clientID, password);
	}

	
}
