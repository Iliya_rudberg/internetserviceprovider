package com.epam.isp.core.client;
import java.io.Serializable;

import com.epam.isp.core.Entity;

/**
 * Client entity. Provides entity field's and get/set methods to hims.
 * 
 * @author Iliya Rudberg
 *
 */
public class Client extends Entity implements Serializable {

	private static final long serialVersionUID = 2613496056932204312L;
	private int id;
	private String name;
	private String surname;
	private String passportId;
	private String address;
	private int phone;
	private int userId;
	private String email;

	public Client(int id,String name, String surname, String passportId,String address, int phone, String email) {
		this.setId(id);
		this.name = name;
		this.surname = surname;
		this.passportId = passportId;
		this.address = address;
		this.phone = phone;
		this.email =  email;

	}
	
	public Client(String name, String surname, String passportId,String address, int phone, String email, int userId) {
		this.name = name;
		this.surname = surname;
		this.passportId = passportId;
		this.address = address;
		this.phone = phone;
		this.email =  email;
		this.userId =  userId;

	}
	
	public String getEmail (){
		return email;
	}
	public void setEmail (String email){
		this.email = email;
	}
	public String getName() {
		return name;
	}

	public void setname(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	

	public String getPassportId() {
		return passportId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id =  id;
	}

	public void setDoctorId(String passportId) {
		this.passportId = passportId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
}
