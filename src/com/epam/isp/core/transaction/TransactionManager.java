package com.epam.isp.core.transaction;

import java.util.List;
/**
 * Transaction manager.
 * 
 * @author Iliya Rudberg
 *
 */
public interface TransactionManager {
	/**
	 * Get all transactions from database.
	 * 
	 * @return list of transactions.
	 */
	List<Transaction> getTransactions();
	/**
	 * Get transactions by employee id.
	 * 
	 * @param employeeId
	 *            specified employeeId.
	 * @return list of transactions.
	 */
	 List<Transaction> getTransactionsByEmployeeId(int employeeId);
	 /**
		 * Add new transaction.
		 * 
		 * @param transaction
		 *            specified transaction.
		 * @return boolean value
	 */
	boolean addTransaction(Transaction transaction);
	/**
	 * Get transaction by id.
	 * 
	 * @param id
	 *            specified id.
	 * @return Transaction entity.
	 */
	Transaction getTransactionById(int id);
	/**
	 * Update transaction by id.
	 * 
	 * @param Transaction
	 *            specified transaction.
	 * @return boolean value
	 */
	boolean updateTransactionById(Transaction transaction);
	/**
	 * Delete transaction by id.
	 * 
	 * @param trsansactionID
	 *            specified transactionID.
	 * @return boolean value
	 */
	boolean deleteTransactionByID(int transactionID);
	/**
	 * Get transaction by contract id.
	 * 
	 * @param contractId
	 *            specified contractId.
	 * @return Transaction entity.
	 */
	 List<Transaction> getTransactionByContractId(int contractId);
	
}
