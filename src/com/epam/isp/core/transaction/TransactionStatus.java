package com.epam.isp.core.transaction;
/**
 * Transaction status enum class. Consist by possible transaction status.
 * 
 * @author Iliya Rudberg
 *
 */
public enum TransactionStatus {
COMPLETED,INCOMPLETED;
}
