package com.epam.isp.core.transaction;

import java.io.Serializable;
import java.sql.Date;

/**
 * Transaction entity. Provides entity field's and get/set methods to hims.
 * 
 * @author Iliya Rudberg
 *
 */
public class Transaction   implements Serializable {
	private static final long serialVersionUID = 1728310203306605413L;
	private int id;
	private int contractId;
	private TransactionType type;
	private int employeesId;
	private Date date;
	private double sum;
	
	public Transaction(int id,int contractId,TransactionType type, int employeesId, double sum,Date date) {
		this.setId(id);
		this.contractId = contractId;
		this.type =  type;
		this.employeesId = employeesId;
		this.date = date;
		this.sum = sum;
	}
	
	public Transaction(Double sum, TransactionType type,int contractId,int employeesId) {
		this.contractId = contractId;
		this.type =  type;
		this.employeesId = employeesId;
		this.sum = sum;
	}

	public int getContractId() {
		return contractId;
	}
	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public int getEmployeesId() {
		return employeesId;
	}

	public void setEmployeesId(int employeesId) {
		this.employeesId = employeesId;
	}

	public TransactionType getType() {
		return type;
	}

	public void setType(TransactionType type) {
		this.type = type;
	}

	public Date getDate() {
		return date;
	}

	public void setdate(Date date) {
		this.date = date;
	}
	
	public double getSum() {
		return sum;
	}

	public void setTariffId(double sum) {
		this.sum =  sum;
	}
}
