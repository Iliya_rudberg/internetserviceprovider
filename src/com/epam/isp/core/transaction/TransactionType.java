package com.epam.isp.core.transaction;
/**
 * Transaction type enum class. Consist by possible transaction types.
 * 
 * @author Iliya Rudberg
 *
 */
public enum TransactionType {
CASH,BANK,PLASTIC_CARD;
}
