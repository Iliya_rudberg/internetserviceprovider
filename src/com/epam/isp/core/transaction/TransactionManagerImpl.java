package com.epam.isp.core.transaction;
import java.util.List;

import com.epam.isp.dao.impl.TransactionDaoImpl;

/**
 * Transaction manager implementation. Here the business logic call dao methods.
 * 
 * @author Iliya Rudberg
 *
 */
public class TransactionManagerImpl implements TransactionManager {
	private TransactionDao transactionDao = new TransactionDaoImpl();
	
	@Override
	public List<Transaction> getTransactionsByEmployeeId(int employeeId) {
		// TODO Auto-generated method stub
		return transactionDao.getTransactionsByEmployeeId(employeeId);
	}
	@Override
	public boolean addTransaction(Transaction transaction) {
		// TODO Auto-generated method stub
		return transactionDao.addTransaction(transaction);
		
	}
	@Override
	public List<Transaction> getTransactions() {
		// TODO Auto-generated method stub
		return transactionDao.getTransactions();
	}
	@Override
	public Transaction getTransactionById(int id) {
		// TODO Auto-generated method stub
		return transactionDao.getTransactionById(id);
	}

	@Override
	public boolean updateTransactionById(Transaction transaction) {
		// TODO Auto-generated method stub
		 return transactionDao.updateTransactionById(transaction);
	}

	@Override
	public boolean deleteTransactionByID(int transactionID) {
		// TODO Auto-generated method stub
		return transactionDao.deleteTransactionByID(transactionID);
	}
	@Override
	public  List<Transaction> getTransactionByContractId(int contractId) {
		// TODO Auto-generated method stub
		return transactionDao.getTransactionByContractId(contractId);
	}
	
}
