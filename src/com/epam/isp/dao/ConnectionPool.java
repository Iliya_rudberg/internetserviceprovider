package com.epam.isp.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.annotation.Resource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

/**
 * The class provides connection pool.
 * 
 * @author Iliya Rudberg
 *
 */
public class ConnectionPool {
	@Resource(name="jdbc/internetdb")
	private static DataSource dataSource= null;
	private final static Logger logger = Logger.getLogger(ConnectionPool.class);
	private static Lock lock = new ReentrantLock();
	
	/**
     * Initialization of context from the resource file.
     * 
     */
	static {
		 try {
	         final Context initCtx = new InitialContext();
	         final Context envCtx = (Context) initCtx.lookup("java:comp/env");
	         dataSource = (DataSource) envCtx.lookup("jdbc/internetdb");
	      } catch (NamingException e) {
	       logger.error("Cannot find a dataSource");
	      }
	}
	/**
     * Custom dataSource without JNDI.
     * 
     * @param DataSource 
     */
	public ConnectionPool(DataSource dataSource){ConnectionPool.dataSource = dataSource;}
	/**
	 * Get free connection from pool.
	 * 
	 * @return connection.
	 */
	public static  Connection getConnection() {
		 if(dataSource == null) return null;
	      Connection conn = null;
	      lock.lock();
	      try {
				conn = dataSource.getConnection();
			} catch (SQLException e) {
				logger.error("Cannot get a free connection");
			}
	       finally {
	          lock.unlock();
	      }

	      return conn;
	   }
	}


