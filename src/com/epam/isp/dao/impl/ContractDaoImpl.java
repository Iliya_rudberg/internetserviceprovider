package com.epam.isp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.isp.core.contract.Contract;
import com.epam.isp.dao.ConnectionPool;
import com.epam.isp.dao.Query;
import com.epam.isp.core.contract.ContractDao;
import com.epam.isp.core.contract.Status;
import com.epam.isp.core.tariff.Tariff;

public class ContractDaoImpl implements ContractDao {
	private final static Logger logger = Logger.getLogger(ContractDaoImpl.class);
	private Connection connection;
	@Override
	public List<Contract> getContracts() {
		// TODO Auto-generated method stub
		 List<Contract> contract = new ArrayList<>();
		connection = ConnectionPool.getConnection();
		try (Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(Query.SELECT_ALL_CONTRACTS)) {
			while (rs.next()) {
				contract.add(new Contract(rs.getInt("id"), rs.getDouble("balance"), rs.getInt("employees_id"),
						rs.getInt("clients_id"),rs.getDate("date"), Status.valueOf(rs.getString("status")), rs.getInt("tariff_id")));
			}

		} catch (SQLException ex) {
			logger.error("Can not find a contract", ex);
		} finally {
			closeConnection();
		}
		return contract;
	}
	@Override
	public boolean deleteContractByID(int contractID) {
		// TODO Auto-generated method stub
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.DELETE_TRANSACTION_BY_CONTRACT_ID)) {
			pStatement.setInt(1, contractID);
			pStatement.execute();
			logger.trace("Transactions deleted successfull");
		} catch (SQLException ex) {
			logger.error("Can not find/delete a Transaction by id", ex);
			
		} 
		try (PreparedStatement pStatement = connection.prepareStatement(Query.DELETE_CONTRACT_BY_ID)) {
			pStatement.setInt(1, contractID);
			pStatement.execute();
			logger.trace("Contract deleted successfull");
			return true;
			
		} catch (SQLException ex) {
			logger.error("Can not find/delete a Contract by id", ex);
			return false;
		} finally {
			closeConnection();
		}
	}
	@Override
	public boolean updateContractById(Contract contract) {
		// TODO Auto-generated method stub
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.UPDATE_CONTRACT_BY_ID)) {
			pStatement.setDouble(1, contract.getBalance());
			pStatement.setInt(2, contract.getEmployeesId());
			pStatement.setInt(3, contract.getClientId());
			pStatement.setDate(4, contract.getDate());
			pStatement.setNString(5, contract.getStatus().toString());
			pStatement.setInt(6, contract.getTariffId());
			pStatement.setInt(7, contract.getId());
			pStatement.executeUpdate();
			return true;
		} catch (SQLException ex) {
			logger.error("Can not update contract", ex);
			return false;
		} finally {
			closeConnection();
		}
	}
	@Override
	public Contract getContractById(int id) {
		// TODO Auto-generated method stub
		Contract contract = null;
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.SELECT_CONTRACT_BY_ID)) {
			pStatement.setInt(1, id);
			pStatement.execute();

			ResultSet rs = pStatement.getResultSet();
			while (rs.next()) {
				contract =  new Contract(rs.getInt("id"), rs.getDouble("balance"), rs.getInt("employees_id"),
						rs.getInt("clients_id"), rs.getDate("date"),Status.valueOf(rs.getString("status")),rs.getInt("tariff_id"));
			}

		} catch (SQLException ex) {
			logger.error("Can not find a contract by id", ex);
		} finally {
			closeConnection();
		}
		return contract;
	}
	/**
	 * The method closes connection.
	 */
	private void closeConnection() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error("Can not close connection");
			}
		}
	}
	@Override
	public Contract getContractByClientId(int clientId) {
		// TODO Auto-generated method stub
		Contract contract = null;
		Tariff tariff= null;
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.SELECT_CONTRACT_BY_CLIENT_ID)) {
			pStatement.setInt(1, clientId);
			pStatement.execute();

			ResultSet rs = pStatement.getResultSet();
			while (rs.next()) {
				tariff = new Tariff (rs.getString("name"),rs.getDouble("cost"));
				contract = new Contract(rs.getInt("id"), Status.valueOf(rs.getString("status")), tariff,
						rs.getDate("date"),rs.getDouble("balance"));
			}
		} catch (SQLException ex) {
			logger.error("Can not find a contract by client id", ex);
		} finally {
			closeConnection();
		}
		return contract;
	}
	@Override
	public List<Contract> getContractsByEmployeeId(int employeeId) {
		// TODO Auto-generated method stub
		List <Contract> contracts = new ArrayList<>();
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.SELECT_CONTRACT_BY_EMPLOYEE_ID)) {
			pStatement.setInt(1, employeeId);
			pStatement.execute();

			ResultSet rs = pStatement.getResultSet();
			while (rs.next()) {
				contracts.add(new Contract(rs.getInt("id"), rs.getDouble("balance"), rs.getInt("employees_id"),
						rs.getInt("clients_id"), rs.getDate("date"),Status.valueOf(rs.getString("status")),rs.getInt("tariff_id")));
			}

		} catch (SQLException ex) {
			logger.error("Can not find a contract by employee id", ex);
		} finally {
			closeConnection();
		}
		return contracts;
	}
	@Override
	public boolean addContract(Contract contract) {
		// TODO Auto-generated method stub
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.INSERT_CONTRACT)) {
			pStatement.setDouble(1, contract.getBalance());
			pStatement.setInt(2, contract.getEmployeesId());
			pStatement.setInt(3, contract.getClientId());
			pStatement.setDate(4,  contract.getDate());
			pStatement.setNString(5, contract.getStatus().toString());
			pStatement.setInt(6, contract.getTariffId());
			pStatement.executeUpdate();
			return true;
		} catch (SQLException ex) {
			logger.error("Can not create a new contract", ex);
			return false;
		} finally {
			closeConnection();
		}
	}

	@Override
	public boolean changeStatusOfContract(Contract contract) {
		// TODO Auto-generated method stub
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.UPDATE_STATUS_OF_CONTRACT)) {
			pStatement.setNString(1, contract.getStatus().toString());
			pStatement.setInt(2, contract.getId());
			pStatement.executeUpdate();
			return true;
		} catch (SQLException ex) {
			logger.error("Can change status of contract", ex);
			return false;
		} finally {
			closeConnection();
		}
	}
	@Override
	public List<Contract> getContractsByStatus(Status status) {
		// TODO Auto-generated method stub
		List<Contract> contract = new ArrayList<>();
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.SELECT_CONTRACTS_BY_STATUS)) {
			pStatement.setNString(1, status.toString());
			pStatement.execute();

			ResultSet rs = pStatement.getResultSet();
			while (rs.next()) {
				contract.add(new Contract(rs.getInt("id"), rs.getDouble("balance"), rs.getInt("employees_id"),
						rs.getInt("clients_id"), rs.getDate("date"),Status.valueOf(rs.getString("status")),rs.getInt("tariff_id")));
			}

		} catch (SQLException ex) {
			logger.error("Can not find a contract by status", ex);
		} finally {
			closeConnection();
		}
		return contract;
	}
	@Override
	public boolean changeTariffOfContract(Contract contract) {
		// TODO Auto-generated method stub
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.UPDATE_TARIFF_OF_CONTRACT)) {
			pStatement.setInt(1, contract.getTariffId());
			pStatement.setInt(2, contract.getId());
			pStatement.executeUpdate();
			return true;
		} catch (SQLException ex) {
			logger.error("Can not change contracts tariff", ex);
			return false;
		} finally {
			closeConnection();
		}
	}

}
