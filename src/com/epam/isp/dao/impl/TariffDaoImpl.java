package com.epam.isp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import com.epam.isp.core.tariff.Tariff;
import com.epam.isp.core.tariff.TariffDao;
import com.epam.isp.core.tariff.TariffType;
import com.epam.isp.dao.ConnectionPool;
import com.epam.isp.dao.Query;

public class TariffDaoImpl implements TariffDao {
	private final static Logger logger = Logger.getLogger(TariffDaoImpl.class);
	private Connection connection;
	
	/**
	 * The method closes connection.
	 */
	private void closeConnection() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error("Can not close connection");
			}
		}
	}
	@Override
	public List<Tariff> getTariffs() {
		// TODO Auto-generated method stub
		List<Tariff> tariff = new ArrayList<>();
		connection = ConnectionPool.getConnection();
		try (Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(Query.SELECT_ALL_TARIFFS)) {
			while (rs.next()) {
				tariff.add(new Tariff(rs.getInt("id"), rs.getString("name"), rs.getDouble("cost"),
						TariffType.valueOf(rs.getString("type")),rs.getDouble("traffic"), rs.getString("description"), rs.getInt("download"),rs.getInt("upload")));
			}

		} catch (SQLException ex) {
			logger.error("Can not find a tariff", ex);
		} finally {
			closeConnection();
		}
		return tariff;
	}
	@Override
	public boolean deleteTariffByID(int tariffID) {
		// TODO Auto-generated method stub
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.DELETE_TARIFF_BY_ID)) {
			pStatement.setInt(1, tariffID);
			pStatement.execute();
			logger.trace("Tariff deleted successfull");
			return true;
		} catch (SQLException ex) {
			logger.error("Can not find/delete a tariff by id", ex);
			return false;
		} finally {
			closeConnection();
		}
	}
	@Override
	public boolean addTariff(Tariff tariff) {
		// TODO Auto-generated method stub
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.INSERT_TARIFF)) {
			pStatement.setNString(1, tariff.getName());
			pStatement.setDouble(2, tariff.getCost());
			pStatement.setNString(3, tariff.getType().toString());
			pStatement.setDouble(4, tariff.getTraffic());
			pStatement.setNString(5, tariff.getDescription());
			pStatement.setInt(6, tariff.getDownload());
			pStatement.setInt(7, tariff.getUpload());
			pStatement.executeUpdate();
			return true;
		} catch (SQLException ex) {
			logger.error("Can not create a new tariff", ex);
			return false;
		} finally {
			closeConnection();
		}
	}
	@Override
	public boolean updateTariffById(Tariff tariff) {
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.UPDATE_TARIFF_BY_ID)) {
			pStatement.setNString(1, tariff.getName());
			pStatement.setDouble(2, tariff.getCost());
			pStatement.setNString(3, tariff.getType().toString());
			pStatement.setDouble(4, tariff.getTraffic());
			pStatement.setNString(5, tariff.getDescription());
			pStatement.setInt(6, tariff.getDownload());
			pStatement.setInt(7, tariff.getUpload());
			pStatement.setInt(8, tariff.getId());
			pStatement.executeUpdate();
			return true;
		} catch (SQLException ex) {
			logger.error("Can not update tariff by id", ex);
			return false;
		} finally {
			closeConnection();
		}

	}
	@Override
	public Tariff getTariffById(int id) {
		// TODO Auto-generated method stub
		Tariff tariff = null;
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.SELECT_TARIFF_BY_ID)) {
			pStatement.setInt(1, id);
			pStatement.execute();

			ResultSet rs = pStatement.getResultSet();
			while (rs.next()) {
				tariff = new Tariff(rs.getString("name"), rs.getDouble("cost"));
			}

		} catch (SQLException ex) {
			logger.error("Can not find a tariff by id", ex);
		} finally {
			closeConnection();
		}
		return tariff;
	}

}
