package com.epam.isp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.isp.core.client.Client;
import com.epam.isp.core.client.ClientDao;
import com.epam.isp.dao.ConnectionPool;
import com.epam.isp.dao.Query;
/**
 * An Implementation of ClientDao interface.
 * 
 * @author Iliya Rudberg
 *
 */
public class ClientDaoImpl implements ClientDao {
	private final static Logger logger = Logger.getLogger(ClientDaoImpl.class);
	private Connection connection;
	@Override
	public List<Client> getClients() {
		// TODO Auto-generated method stub
		List<Client> clients = new ArrayList<>();
		connection = ConnectionPool.getConnection();
		try (Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(Query.SELECT_ALL_CLIENTS)) {
			while (rs.next()) {
				clients.add(new Client(rs.getInt("id"), rs.getString("name"), rs.getString("surname"),
						rs.getString("passport_id"), rs.getString("address"),rs.getInt("phone"),rs.getString("email")));
			}

		} catch (SQLException ex) {
			logger.error("Can not find any client", ex);
		} finally {
			closeConnection();
		}
		return clients;
	}

	@Override
	public boolean addClient(Client client) {
		// TODO Auto-generated method stub
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.INSERT_CLIENT)) {
			pStatement.setNString(1, client.getSurname());
			pStatement.setNString(2, client.getName());
			pStatement.setNString(3, client.getPassportId());
			pStatement.setNString(4, client.getAddress());
			pStatement.setInt(5, client.getPhone());
			pStatement.setInt(6, client.getUserId());
			pStatement.setNString(7, client.getEmail());
			pStatement.executeUpdate();
			return true;
		} catch (SQLException ex) {
			logger.error("Can not create a new client", ex);
			return false;
		} finally {
			closeConnection();
		}

	}

	
	@Override
	public Client getClientByContractId(int contractId) {
		// TODO Auto-generated method stub
		Client client = null;
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.SELECT_CLIENTS_BY_CONTRACTS_ID)) {
			pStatement.setInt(1, contractId);
			pStatement.execute();

			ResultSet rs = pStatement.getResultSet();
			while (rs.next()) {
				client =  new Client(rs.getInt("id"), rs.getString("name"), rs.getString("surname"),
						rs.getString("passport_id"), rs.getString("address"),rs.getInt("phone"),rs.getString("email"));
			}

		} catch (SQLException ex) {
			logger.error("Can not find a client by contract id", ex);
		} finally {
			closeConnection();
		}
		return client;
	}
	/**
	 * The method closes connection.
	 */
	private void closeConnection() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error("Can not close connection");
			}
		}
	}

	@Override
	public boolean updateClientById(Client client) {
		// TODO Auto-generated method stub
				connection = ConnectionPool.getConnection();
				try (PreparedStatement pStatement = connection.prepareStatement(Query.UPDATE_CLIENT_BY_ID)) {
					pStatement.setNString(1, client.getName());
					pStatement.setNString(2, client.getSurname());
					pStatement.setNString(3, client.getAddress());
					pStatement.setInt(4, client.getPhone());
					pStatement.setNString(5, client.getPassportId());
					pStatement.setNString(6, client.getEmail());
					pStatement.setInt(7, client.getId());
					pStatement.executeUpdate();
					return true;
				} catch (SQLException ex) {
					logger.error("Can not update client", ex);
					return false;
				} finally {
					closeConnection();
				}
			}

	@Override
	public boolean deleteClientByID(int clientID) {
		// TODO Auto-generated method stub
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.DELETE_CLIENT_BY_ID)) {
			pStatement.setInt(1, clientID);
			pStatement.execute();
			logger.trace("Client deleted successfull");
			return true;
		} catch (SQLException ex) {
			logger.error("Can not find/delete a Client by id", ex);
			return false;
		} finally {
			closeConnection();
		}
	}

	@Override
	public Client getClientByUserId(int userId) {
		// TODO Auto-generated method stub
		Client client = null;
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.SELECT_CLIENT_BY_USER_ID)) {
			pStatement.setInt(1, userId);
			pStatement.execute();

			ResultSet rs = pStatement.getResultSet();
			while (rs.next()) {
				client = new Client(rs.getInt("id"), rs.getString("name"), rs.getString("surname"),
						rs.getString("passport_id"),rs.getString("address"), rs.getInt("phone"),rs.getString("email"));
			}
		} catch (SQLException ex) {
			logger.error("Can not find a client by user id", ex);
		} finally {
			closeConnection();
		}
		return client;
	}

	@Override
	public boolean changePassword(int clientID, String password) {
		// TODO Auto-generated method stub
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.UPDATE_PASSWORD_OF_CLIENT)) {
			pStatement.setNString(1, password);
			pStatement.setInt(2, clientID);
			pStatement.executeUpdate();
			return true;
		} catch (SQLException ex) {
			logger.error("Can not change password", ex);
			return false;
		} finally {
			closeConnection();
		}
	}

	
	}

