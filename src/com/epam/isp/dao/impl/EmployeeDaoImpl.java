package com.epam.isp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import com.epam.isp.core.employee.Employee;
import com.epam.isp.core.employee.EmployeeDao;
import com.epam.isp.dao.ConnectionPool;
import com.epam.isp.dao.Query;
/**
 * An Implementation of EmployeeDao interface.
 * 
 * @author Iliya Rudberg
 *
 */
public class EmployeeDaoImpl implements EmployeeDao {
	private final static Logger logger = Logger.getLogger(EmployeeDaoImpl.class);
	private Connection connection;
	@Override
	public List<Employee> getEmployee() {
		// TODO Auto-generated method stub
		List<Employee> employee = new ArrayList<>();
		connection = ConnectionPool.getConnection();
		try (Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(Query.SELECT_ALL_EMPLOYEES)) {
			while (rs.next()) {
				employee.add(new Employee(rs.getInt("id"), rs.getString("name"), rs.getString("surname"),
						rs.getString("address"), rs.getInt("phone"),rs.getString("position"),rs.getString("email")));
			}

		} catch (SQLException ex) {
			logger.error("Can not find a employee", ex);
		} finally {
			closeConnection();
		}
		return employee;
	}
	@Override
	public boolean addEmployee(Employee employee) {
		// TODO Auto-generated method stub
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.INSERT_EMPLOYEE)) {
			pStatement.setNString(1, employee.getName());
			pStatement.setNString(2, employee.getSurname());
			pStatement.setNString(3, employee.getAddress());
			pStatement.setInt(4, employee.getPhone());
			pStatement.setNString(5, employee.getPosition());
			pStatement.setInt(6, employee.getUserId());
			pStatement.setNString(7, employee.getEmail());
			pStatement.executeUpdate();
			return true;
		} catch (SQLException ex) {
			logger.error("Can not create a new employee", ex);
			return false;
		} finally {
			closeConnection();
		}

	}

	@Override
	public Employee getEmployeeById(int id) {
		// TODO Auto-generated method stub
		Employee employee = null;
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.SELECT_EMPLOYEE_BY_ID)) {
			pStatement.setInt(1, id);
			pStatement.execute();

			ResultSet rs = pStatement.getResultSet();
			while (rs.next()) {
				employee = new Employee(rs.getInt("id"), rs.getString("name"), rs.getString("surname"),
						rs.getString("address"), rs.getInt("phone"),rs.getString("position"),rs.getString("email"));
			}

		} catch (SQLException ex) {
			logger.error("Can not find a employee by id", ex);
		} finally {
			closeConnection();
		}
		return employee;
	}
	/**
	 * The method closes connection.
	 */
	private void closeConnection() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error("Can not close connection");
			}
		}
	}
	@Override
	public Employee getEmployeeByUserId(int id) {
		// TODO Auto-generated method stub
		Employee employee = null;
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.SELECT_EMPLOYEE_BY_USER_ID)) {
			pStatement.setInt(1, id);
			pStatement.execute();

			ResultSet rs = pStatement.getResultSet();
			while (rs.next()) {
				employee = new Employee(rs.getInt("id"), rs.getString("name"), rs.getString("surname"),
						rs.getString("address"), rs.getInt("phone"),rs.getString("position"),rs.getString("email"));
			}

		} catch (SQLException ex) {
			logger.error("Can not find a employee by user id", ex);
		} finally {
			closeConnection();
		}
		return employee;
	}

	@Override
	public boolean deleteEmployeeByID(int employeeID) {
		// TODO Auto-generated method stub
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.DELETE_EMPLOYEE_BY_ID)) {
			pStatement.setInt(1, employeeID);
			pStatement.execute();
			logger.trace("Employee deleted successfull");
			return true;
		} catch (SQLException ex) {
			logger.error("Can not find/delete a Employee by id", ex);
			return false;
		} finally {
			closeConnection();
		}
	}
	
	@Override
	public boolean updateEmployeeById(Employee employee) {
		// TODO Auto-generated method stub
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.UPDATE_EMPLOYEE_BY_ID)) {
			pStatement.setNString(1, employee.getName());
			pStatement.setNString(2, employee.getSurname());
			pStatement.setNString(3, employee.getAddress());
			pStatement.setInt(4, employee.getPhone());
			pStatement.setNString(5, employee.getPosition());
			pStatement.setNString(6, employee.getEmail());
			pStatement.setInt(7, employee.getId());
			pStatement.executeUpdate();
			return true;
		} catch (SQLException ex) {
			logger.error("Can not update employee by id", ex);
			return false;
		} finally {
			closeConnection();
		}
	}
}
