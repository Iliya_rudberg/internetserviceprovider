package com.epam.isp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.isp.LoginDuplicateException;
import com.epam.isp.core.user.Role;
import com.epam.isp.core.user.User;
import com.epam.isp.core.user.UserDao;
import com.epam.isp.dao.ConnectionPool;
import com.epam.isp.dao.Query;
/**
 * An Implementation of UserDao interface.
 * 
 * @author Iliya Rudberg
 *
 */
public class UserDaoImpl implements UserDao {

	private final static Logger logger = Logger.getLogger(UserDaoImpl.class);

	private Connection connection;

	@Override
	public List<User> getUsers() {
		List<User> users = new ArrayList<>();
		connection = ConnectionPool.getConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(Query.SELECT_ALL_USERS)) {
			while (resultSet.next()) {
				users.add(new User(resultSet.getInt("id"), resultSet.getString("login"),
						resultSet.getString("password"), Role.valueOf(resultSet.getString("name").toUpperCase())));
			}

		} catch (SQLException ex) {
			logger.error("Can not find a users", ex);
		} finally {
			closeConnection();
		}

		return users;
	}

	@Override
	public boolean addUser(User user) throws LoginDuplicateException {
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.INSERT_USER)) {
			pStatement.setNString(1, user.getLogin());
			pStatement.setNString(2, user.getPassword());
			pStatement.setInt(3, user.getRoleId());
			pStatement.executeUpdate();
			return true;
		} catch (SQLException ex) {
			logger.error("Can not create a new user", ex);
			
			// check exception if true found duplicate
			if (ex instanceof SQLIntegrityConstraintViolationException) {
				logger.error("User with such login was created");
				throw new LoginDuplicateException();
			}
			return false;
		} finally {
			closeConnection();
		}
	}

	@Override
	public User getUserByLogin(String login) {
		
		User user = null;
		connection = ConnectionPool.getConnection();
		
		try (PreparedStatement ps = connection.prepareStatement(Query.SELECT_USER_BY_LOGIN)) {
			ps.setNString(1, login);
			ps.execute();

			ResultSet resultSet = ps.getResultSet();
			if (resultSet.next()) {
				user = new User(resultSet.getInt("id"), resultSet.getString("login"),
						resultSet.getString("password"), resultSet.getInt("roles_id"));
			}
			resultSet.close();
		} catch (SQLException ex) {
			logger.error("Can not find user by login", ex);
		} finally {
			closeConnection();
		}

		return user;
	}

	@Override
	public User getUserById(int id) {
		User user = null;
		connection = ConnectionPool.getConnection();
		try (PreparedStatement ps = connection.prepareStatement(Query.SELECT_USER_BY_ID)) {
			ps.setInt(1, id);
			ps.execute();

			ResultSet resultSet = ps.getResultSet();
			if (resultSet.next()) {
				user = new User(resultSet.getInt("id"), resultSet.getString("login"),
						resultSet.getString("password"), resultSet.getInt("roles_id"));
			}
			resultSet.close();
		} catch (SQLException ex) {
			logger.error("Can not find user by id", ex);
		} finally {
			closeConnection();
		}

		return user;
	}

	@Override
	public List<Role> getRoles() {
		List<Role> roles = new ArrayList<>();
		connection = ConnectionPool.getConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(Query.SELECT_ALL_ROLES)) {
			while (resultSet.next()) {

				// create role entity set id and add to list
				for (Role role : Role.values()) {
					if (resultSet.getString("name").toUpperCase().equals(role.toString())) {
						role.setId(resultSet.getInt("id"));
						roles.add(role);
						break;
					}
				}
			}

		} catch (SQLException ex) {
			logger.error("Can not find a roles", ex);
		} finally {
			closeConnection();
		}

		return roles;
	}
	@Override
	public boolean deleteUserByID(int userID) {
		// TODO Auto-generated method stub
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.DELETE_USER_BY_ID)) {
			pStatement.setInt(1, userID);
			pStatement.execute();
			logger.trace("user deleted successfull");
			return true;
		} catch (SQLException ex) {
			logger.error("Can not find/delete a user by id", ex);
			return false;
		} finally {
			closeConnection();
		}
	}
	/**
	 * The method closes connection.
	 */
	private void closeConnection() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error("Can not close connection");
			}
		}
	}
}

