package com.epam.isp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import com.epam.isp.core.tasks.Task;
import com.epam.isp.core.tasks.TaskDao;
import com.epam.isp.dao.ConnectionPool;
import com.epam.isp.dao.Query;
/**
 * An Implementation of ClientDao interface.
 * 
 * @author Iliya Rudberg
 *
 */
public class TaskDaoImpl implements TaskDao {
	private final static Logger logger = Logger.getLogger(TaskDaoImpl.class);
	private Connection connection;

	/**
	 * The method closes connection.
	 */
	private void closeConnection() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error("Can not close connection");
			}
		}
	}

	@Override
	public List<Task> listTasks() {
		// TODO Auto-generated method stub
		List<Task> tasks = new ArrayList<>();
		connection = ConnectionPool.getConnection();
		try (Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(Query.SELECT_ALL_TASKS)) {
			while (rs.next()) {
				tasks.add(new Task(rs.getInt("id"), rs.getString("name"), rs.getInt("phone"),rs.getString("email"),rs.getString("message"),rs.getDate("date"),rs.getString("status")));
			}

		} catch (SQLException ex) {
			logger.error("Can not find a task", ex);
		} finally {
			closeConnection();
		}
		return tasks;
	}

	@Override
	public boolean addTask(Task task) {
		// TODO Auto-generated method stub
		Connection connection = ConnectionPool.getConnection(); 
		try (PreparedStatement pStatement = connection.prepareStatement(Query.INSERT_CONTACT)) { 
		pStatement.setNString(1, task.getName()); 
		pStatement.setNString(2, task.getEmail()); 
		pStatement.setInt(3, task.getPhone()); 
		pStatement.setNString(4, task.getMessage()); 
		pStatement.executeUpdate(); 
		return true;
		} catch (SQLException ex) { 
		logger.error("Can not send a new message", ex); 
		return false;
		} finally { 
		if (connection != null) { 
		try { 
		connection.close(); 
		} catch (SQLException e) { 
		logger.error("Can not close connection"); 
		}  
	}
		}}

	@Override
	public boolean completeTask(int taskId) {
		// TODO Auto-generated method stub
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.UPDATE_TASK_BY_ID)) {
			pStatement.setInt(1, taskId);
			pStatement.executeUpdate();
			return true;
		} catch (SQLException ex) {
			logger.error("Can not update tariff by id", ex);
			return false;
		} finally {
			closeConnection();
		}
	}

	

	
	}

