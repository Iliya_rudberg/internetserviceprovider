package com.epam.isp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

import com.epam.isp.core.transaction.Transaction;
import com.epam.isp.core.transaction.TransactionDao;
import com.epam.isp.core.transaction.TransactionType;
import com.epam.isp.dao.ConnectionPool;
import com.epam.isp.dao.Query;

public class TransactionDaoImpl implements TransactionDao {
	private final static Logger logger = Logger.getLogger(TransactionDaoImpl.class);
	private Connection connection;
	
	/**
	 * The method closes connection.
	 */
	private void closeConnection() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error("Can not close connection");
			}
		}
	}
	@Override
	public List<Transaction> getTransactionsByEmployeeId(int employeeId) {
		// TODO Auto-generated method stub
		List<Transaction> transaction = new ArrayList<>();
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.SELECT_TRANSACTION_BY_EMPLOYEE_ID)) {
			pStatement.setInt(1, employeeId);
			pStatement.execute();

			ResultSet rs = pStatement.getResultSet();
			while (rs.next()) {
				transaction.add(new Transaction(rs.getInt("id"), rs.getInt("contracts_id"),TransactionType.valueOf(rs.getString("type")),
						rs.getInt("employees_id"), rs.getDouble("sum"),rs.getDate("date")));
			}

		} catch (SQLException ex) {
			logger.error("Can not find a transaction by employee id", ex);
		} finally {
			closeConnection();
		}
		return transaction;
	}
	@Override
	public List<Transaction> getTransactions() {
		// TODO Auto-generated method stub
		List<Transaction> transaction = new ArrayList<>();
		connection = ConnectionPool.getConnection();
		try (Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(Query.SELECT_ALL_TRANSACTIONS)) {
			while (rs.next()) {
				transaction.add(new Transaction(rs.getInt("id"), rs.getInt("contracts_id"), TransactionType.valueOf(rs.getString("type")),
						rs.getInt("employees_id"), rs.getDouble("sum"),rs.getDate("date")));
			}

		} catch (SQLException ex) {
			logger.error("Can not find a transaction", ex);
		} finally {
			closeConnection();
		}
		return transaction;
	}
	@Override
	public boolean addTransaction(Transaction transaction) {
		// TODO Auto-generated method stub
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.INSERT_TRANSACTION)) {
			pStatement.setInt(1, transaction.getContractId());
			pStatement.setNString(2, transaction.getType().toString());
			pStatement.setInt(3, transaction.getEmployeesId());
			pStatement.setDouble(4, transaction.getSum());
			pStatement.executeUpdate();
			return true;
		} catch (SQLException ex) {
			logger.error("Can not create a new transaction", ex);
			return false;
		} finally {
			closeConnection();
		}
	}
	@Override
	public boolean  deleteTransactionByID(int transactionID) {
		// TODO Auto-generated method stub
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.DELETE_TRANSACTION_BY_ID)) {
			pStatement.setInt(1, transactionID);
			pStatement.execute();
			logger.trace("Transaction deleted successfull");
			return true;
		} catch (SQLException ex) {
			logger.error("Can not find/delete a Transaction by id", ex);
			return false;
		} finally {
			closeConnection();
		}
	}
	@Override
	public boolean  updateTransactionById(Transaction transaction) {
		// TODO Auto-generated method stub
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.UPDATE_TRANSACTION_BY_ID)) {
			pStatement.setDouble(1, transaction.getSum());
			pStatement.setInt(2, transaction.getContractId());
			pStatement.setInt(3, transaction.getEmployeesId());
			pStatement.setDate(4, transaction.getDate());
			pStatement.setNString(5, transaction.getType().toString());
			pStatement.setInt(6, transaction.getId());
			pStatement.executeUpdate();
			return true;
		} catch (SQLException ex) {
			logger.error("Can not update transaction by id", ex);
			return false;
		} finally {
			closeConnection();
		}
	}
	@Override
	public Transaction getTransactionById(int id) {
		// TODO Auto-generated method stub
		Transaction transaction = null;
				connection = ConnectionPool.getConnection();
				try (PreparedStatement pStatement = connection.prepareStatement(Query.SELECT_TRANSACTION_BY_ID)) {
					pStatement.setInt(1, id);
					pStatement.execute();

					ResultSet rs = pStatement.getResultSet();
					while (rs.next()) {
						transaction = new Transaction(rs.getInt("id"), rs.getInt("contracts_id"),TransactionType.valueOf(rs.getString("type")),
								rs.getInt("employees_id"), rs.getDouble("sum"),rs.getDate("date"));
					}

				} catch (SQLException ex) {
					logger.error("Can not find a transaction by  id", ex);
				} finally {
					closeConnection();
				}
				return transaction;
	}
	@Override
	public List<Transaction> getTransactionByContractId(int contractId) {
		List<Transaction> transaction = new ArrayList<>();
				connection = ConnectionPool.getConnection();
				try (PreparedStatement pStatement = connection.prepareStatement(Query.SELECT_TRANSACTION_BY_CONTRACT_ID)) {
					pStatement.setInt(1, contractId);
					pStatement.execute();

					ResultSet rs = pStatement.getResultSet();
					while (rs.next()) {
						transaction.add(new Transaction(rs.getInt("id"), rs.getInt("contracts_id"),TransactionType.valueOf(rs.getString("type")),
								rs.getInt("employees_id"), rs.getDouble("sum"),rs.getDate("date")));
					}

				} catch (SQLException ex) {
					logger.error("Can not find a transactions by  contract id", ex);
				} finally {
					closeConnection();
				}
				return transaction;
	}


}
