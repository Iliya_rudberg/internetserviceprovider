package com.epam.isp.dao;

/**
 * Contains all queries to data base.
 * 
 * @author Iliya Rudberg
 *
 */
public class Query {

	// user
	public static final String SELECT_ALL_USERS = "SELECT * FROM users JOIN roles  ON roles.id = users.roles_id";
	public static final String INSERT_USER = "INSERT INTO users (login, password, roles_id) VALUES (?, ?, ?)";
	public static final String SELECT_USER_BY_LOGIN = "SELECT * FROM users WHERE login = ?";
	public static final String SELECT_USER_BY_ID = "SELECT * FROM users WHERE id = (?)";
	public static final String SELECT_ALL_ROLES = "SELECT * FROM roles";
	public static final String DELETE_USER_BY_ID = "DELETE FROM users WHERE id = ?";
	// employees
	public static final String SELECT_EMPLOYEE_BY_USER_ID = "SELECT * FROM employees WHERE users_id = ?";
	public static final String SELECT_ALL_EMPLOYEES = "SELECT * FROM employees";
	public static final String INSERT_EMPLOYEE = "INSERT INTO employees (name, surname, address, phone, position,  users_id, email) VALUES (?,?,?,?,?,?,?)";
	public static final String DELETE_TARIFF_BY_ID = "DELETE FROM tariff WHERE id = ?";
	public static final String DELETE_CONTRACT_BY_ID = "DELETE FROM contracts WHERE id = ?";
	public static final String DELETE_TRANSACTION_BY_CONTRACT_ID = "DELETE FROM  transactions WHERE contracts_id IN (SELECT id FROM contracts WHERE id = ?)";
	public static final String DELETE_EMPLOYEE_BY_ID = "DELETE emp, us FROM employees emp JOIN users us ON emp.users_id = us.id WHERE emp.id = ? AND emp.users_id = us.id";
	public static final String UPDATE_CONTRACT_BY_ID = "UPDATE contracts SET balance = ?, employees_id  = ?, clients_id  = ?, date  = ?, status  = ?, tariff_id = ? WHERE id = ?";
	public static final String UPDATE_EMPLOYEE_BY_ID = "UPDATE employees SET name = ?, surname  = ?, address  = ?, phone  = ?, position  = ?, email = ? WHERE id = ?";
	public static final String UPDATE_TRANSACTION_SUM_BY_ID = "UPDATE transactions SET sum = ? WHERE id = ?";
	// clients
	public static final String SELECT_CLIENT_BY_USER_ID = "SELECT * FROM clients WHERE users_id = ?";
	public static final String DELETE_CLIENT_BY_ID = "DELETE cl, us FROM clients cl JOIN users us ON cl.users_id = us.id WHERE cl.id = ? AND cl.users_id = us.id";
	public static final String SELECT_ALL_CLIENTS = "SELECT * FROM clients";
	public static final String INSERT_CLIENT = "INSERT INTO clients (surname, name, passport_id, address, phone, users_id, email) VALUES (?,?,?,?,?,?,?)";
	public static final String UPDATE_CLIENT_BY_ID = "UPDATE clients SET name = ?, surname  = ?, address  = ?, phone  = ?, passport_id  = ?, email = ? WHERE id = ?";
	//task
	public static final String INSERT_CONTACT = "INSERT INTO contact (name, email, phone, message, status) VALUES (?,?,?,?,'NEW')";
	public static final String SELECT_CONTRACT_BY_CLIENT_ID = "SELECT  c.id,status, date,balance, t.name, t.cost FROM contracts c JOIN tariff t ON c.tariff_id = t.id WHERE clients_id = ? ";
	public static final String SELECT_CLIENTS_BY_CONTRACTS_ID = "SELECT * FROM  clients WHERE id IN (SELECT clients_id FROM contracts WHERE id = ?)";
	public static final String SELECT_ALL_TRANSACTIONS = "SELECT * FROM transactions";
	public static final String SELECT_EMPLOYEE_BY_ID = "SELECT * FROM employees WHERE id = ?";
	public static final String SELECT_CONTRACT_BY_EMPLOYEE_ID = "SELECT * FROM contracts WHERE employees_id = ?";
	public static final String SELECT_TRANSACTION_BY_EMPLOYEE_ID = "SELECT * FROM transactions WHERE employees_id = ?";
	public static final String SELECT_CONTRACT_BY_ID = "SELECT * FROM contracts WHERE id = ?";
	public static final String SELECT_ALL_TARIFFS = "SELECT * FROM tariff;";
	public static final String SELECT_ALL_CONTRACTS = "SELECT * FROM contracts;";
	public static final String INSERT_TRANSACTION = "INSERT INTO transactions (contracts_id,type, employees_id, sum) VALUES (?,?,?,?)";
	public static final String INSERT_TARIFF = "INSERT INTO tariff (name, cost, type, traffic, description, download, upload ) VALUES (?, ?, ?, ?, ?, ?, ?)";
	public static final String INSERT_CONTRACT = "INSERT INTO contracts (balance, employees_id, clients_id, date, status, tariff_id) VALUES (?,?,?,?,?,?)";
	public static final String SELECT_CONTRACTS_BY_STATUS = "SELECT * FROM contracts WHERE status = (?)";
	public static final String UPDATE_TARIFF_BY_ID = "UPDATE tariff SET name = ?, cost  = ?, type  = ?, traffic  = ?, description  = ?, download  = ?, upload  = ?  WHERE id = ?";
	public static final String UPDATE_STATUS_OF_CONTRACT = "UPDATE contracts SET status = ? WHERE id = ?";
	public static final String UPDATE_PASSWORD_OF_CLIENT = "UPDATE users a LEFT JOIN clients b ON a.id = b.users_id SET a.password = ? WHERE b.id = ?";
	public static final String UPDATE_TRANSACTION_BY_ID = "UPDATE transactions SET sum = ?, contracts_id  = ?, employees_id  = ?, date  = ?, type = ? WHERE id = ?";;
	public static final String SELECT_TRANSACTION_BY_ID = "SELECT * FROM transactions WHERE id = ?";
	public static final String DELETE_TRANSACTION_BY_ID = "DELETE FROM transactions WHERE id = ?";
	public static final String UPDATE_TARIFF_OF_CONTRACT = "UPDATE contracts SET tariff_id = ? WHERE id = ?";
	public static final String SELECT_TARIFF_BY_ID = "SELECT * FROM tariff WHERE id = ?";
	public static final String SELECT_TRANSACTION_BY_CONTRACT_ID = "SELECT * FROM transactions WHERE contracts_id = ?";
	public static final String SELECT_ALL_TASKS = "SELECT * FROM contact WHERE status = 'NEW' ORDER by date ";
	public static final String UPDATE_TASK_BY_ID = "UPDATE contact SET status='Handled' where id = ?";
}
